


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="Description" content="ISIMA, ecole superieure publique d'informatique et de modelisation,
 enseignement des nouvelles technologies logicielles et materielles, technologies de l'information, reseaux et telecommunications">
<meta name="Keywords" content="informatique, metier, ingenieur, enseignement, superieur, ecole, superieure, publique, internet,
 modelisation, telecommunications, nouvelles, technologies, etudes, auvergne, diplome, ensi,
 polytechnique, polytechniques, admission, concours, international, formation, projets, projet">

<link rel="shortcut icon" href="icon.ico">
<link rel="stylesheet" href="style/defaut.css"   type="text/css">


<title>ISIMA - Institut Sup�rieur d'Informatique, de Mod�lisation et de leurs Applications - &eacute;cole d'ing&eacute;nieur publique en Informatique</title>

<style type="text/css">
<!--

#banniere1 {
   width : 12em;
   padding : 0.25em;
   position : relative;
   font-size : 1.1em;
   border : thin solid rgb(51,102,153);
   text-align:center;
}

#banniere2 {
   width : 12em;
   padding : 0.25em;
   position : relative;
   top:0.5em;
   font-size : 1.1em;
   border : thin solid rgb(51,102,153);
   text-align:center;
}

#banniere3 {
   width : 12em;
   padding : 0.25em;
   position : relative;
   margin-top : 1em;
   font-size : 1.1em;
   border : thin solid rgb(51,102,153);
   text-align:center;
}

#menu_circ {
   width:14em;
   top:0em;
   float:right;
}

#coindroit {
   margin-top:-5.5em;
   margin-right:0.095em;
   float:right;
   width:9em;
   height:5.5em;
   overflow:hidden;
}


#coindroit div {
   margin-top:-3em;
}

#coindroit div p {
   padding-right:0.3em;
   font-size: 0.95em;
   text-align:right;
   margin:0;
}

a, a:visited {
   color : rgb(51,102,153);
   text-decoration : none;
}

a:hover {
   color : #3366CC;
   background-color: #E1EBFB;
   /*text-decoration : underline overline;*/
}

-->
</style>
</head>

<body>

   <div id="boiteronde">
   <div class="haut"><div class="bas"><div class="gauche"><div class="droite">
   <div class="basgauche"><div class="basdroite"><div class="hautgauche"><div class="hautdroite">

   <div class="isima">
   <div style="width:14.5em;float:left;margin:-0.25em 1em 0 1em;"><a href="http://www.isima.fr" ><img src="images/logo1024.gif" alt="ISIMA" title="ISIMA" style="width:100%;height:100%" /></a></div>   Institut Sup�rieur d'Informatique<br /> de Mod�lisation et de leurs Applications</div>
   <div style="height:1.5em; background-color:rgb(51,102,153);width:100%"></div>

   <div style="padding:1em;height:26em;">
   <div id="menu_circ">
   <p style="position:relative;left:-0.25em;top:0.25em;font-size:0.95em;"><a href="presentation/presentation.php?sousmenu=01" title="Pr�sentation">Pr�sentation</a></p><p style="position:relative;left:0.588087370409em;top:2.25em;font-size:0.95em;"><a href="enseignement/enseignement.php?sousmenu=01" title="Enseignement">Enseignement</a></p><p style="position:relative;left:1.25969515136em;top:4.25em;font-size:0.95em;"><a href="admission/admission.html?sousmenu=01" title="Admission">Admission</a></p><p style="position:relative;left:1.72861679354em;top:6.25em;font-size:0.95em;"><a href="recherche/recherche.php?sousmenu=01" title="Recherche">Recherche</a></p><p style="position:relative;left:1.96957260984em;top:8.25em;font-size:0.95em;"><a href="entreprises/entreprises.php?sousmenu=01" title="Entreprises">Entreprises</a></p><p style="position:relative;left:1.96957260984em;top:10.25em;font-size:0.95em;"><a href="formationcontinue/formationcontinue.php?sousmenu=01" title="Formation continue">Formation continue</a></p><p style="position:relative;left:1.72861679354em;top:12.25em;font-size:0.95em;"><a href="international/international.php?sousmenu=01" title="International">International</a></p><p style="position:relative;left:1.25969515136em;top:14.25em;font-size:0.95em;"><a href="vie_etudiante/vie_etudiante.php?sousmenu=01" title="Vie �tudiante">Vie �tudiante</a></p><p style="position:relative;left:0.588087370409em;top:16.25em;font-size:0.95em;"><a href="annuaire/annuaire.php?sousmenu=01" title="Annuaire">Annuaire</a></p>   </div>

   <div style="width:22em;float:right;margin-top:1em;margin-left:-2em"><img src="photos/imagesdegrade1024.jpg" alt="Photos ISIMA et Auvergne" title="Photos ISIMA et Auvergne" style="width:100%;height:100%" /></div>
   <div id="banniere1">Grande �cole publique menant au m�tier<br />d'Ing�nieur en Informatique<br /> Recrutement sur <a href="http://ccp.scei-concours.org/">Concours Communs Polytechniques</a> et <a href="admission/admission.html?sousmenu=01#dossier">Dossier</a></div>
   <div id="banniere2">Anciens &Eacute;l�ves
     <div style="width:8.52em;height:2.16em;margin:auto;"><a href="http://anelis.isima.fr" ><img src="images/anelis.gif" alt="ANELIS : Association des anciens �l�ves" title="ANELIS : Association des anciens �l�ves" style="width:100%;height:100%" /></a></div>   </div>
   <div id="banniere3">Actualit�s
     <div><a href="news/forum.php" title="forum">Forum entreprises</a></div>
     <div><a href="news/forum.php" title="forum">8-9 novembre 2007</a></div>
   </div>

     <!--<div style="width:7.5em;height:4.5em;position:relative;margin-top:7em"><a href="http://www.univ-bpclermont.fr/" ><img src="images/ubp.jpg" alt="Universit� Blaise Pascal - Clermont-Ferrand II" title="Universit� Blaise Pascal - Clermont-Ferrand II" style="width:100%;height:100%" /></a></div>  -->

   </div>

   <div style="width:7.5em;height:4.5em;float:left;margin-top:-5em;margin-left:1em;"><a href="http://www.univ-bpclermont.fr/" ><img src="images/ubp.jpg" alt="Universit� Blaise Pascal - Clermont-Ferrand II" title="Universit� Blaise Pascal - Clermont-Ferrand II" style="width:100%;height:100%" /></a></div>
   <div id="coindroit">
    <div style="width:10em;height:6.25em;margin-top:0em;margin-right:1em;"><img src="images/plus.gif" alt="" title="" style="width:100%;height:100%" /></div>    <div>
      <p><a href="annuaire/annuaire.php?sousmenu=02">Contacts</a></p>
      <p><a href="plan_du_site/plan_du_site.php?sousmenu=01">Plan du site</a></p>
    </div>
   </div>


   <div class="notegauche">
   <div class="notedroite">ISIMA, Campus des C�zeaux, BP 10125, 63173 Aubi�re CEDEX<br />
   Pour toute question sur le site, contactez le <script type="text/javascript">document.write('<');document.write('a');document.write(' ');document.write('h');document.write('r');document.write('e');document.write('f');document.write('=');document.write('"');document.write('m');document.write('a');document.write('i');document.write('l');document.write('t');document.write('o');document.write(':');document.write('w');document.write('e');document.write('b');document.write('m');document.write('a');document.write('s');document.write('t');document.write('e');document.write('r');document.write('@');document.write('i');document.write('s');document.write('i');document.write('m');document.write('a');document.write('.');document.write('f');document.write('r');document.write('');document.write('"');document.write('>');document.write('webmestre');document.write('<');document.write('/');document.write('a');document.write('>');</script>   </div></div>
   </div></div></div></div></div></div></div></div></div>

</body>

</html>
