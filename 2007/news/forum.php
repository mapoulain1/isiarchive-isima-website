


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<link rel="shortcut icon" href="icon.ico">
<link rel="stylesheet" href="../style/defaut.css" type="text/css">

<style type="text/css">
<!--
   table {width : 100%;}
   td { padding-left : 1em;}
-->
</style>

<title>Forum Etudiants / Entreprises - 8 et 9 novembre 2007</title>
</head><body><div id="boiteronde"><div class="haut"><div class="bas"><div class="gauche"><div class="droite"><div class="basgauche"><div class="basdroite"><div class="hautgauche"><div class="hautdroite"><div class="isima"><div style="width:14.5em;float:left;margin:-0.25em 1em 0 1em;"><a href="http://www.isima.fr" ><img src="../images/logo1024.gif" alt="ISIMA" title="ISIMA" style="width:100%;height:100%" /></a></div>Institut Sup�rieur d'Informatique<br /> de Mod�lisation et de leurs Applications</div><div id="menu"><ul><li><a href="../presentation/presenta.html?sousmenu=01" title="Pr�sentation">Pr�sentation</a></li><li><a href="../enseignement/ense.htmlment?sousmenu=01" title="Enseignement">Enseignement</a></li><li><a href="../admission/a.htmlsion?sousmenu=01" title="Admission">Admission</a></li><li><a href="../recherche/r.htmlrche?sousmenu=01" title="Recherche">Recherche</a></li><li><a href="../entreprises/ent.htmlises?sousmenu=01" title="Entreprises">Entreprises</a></li><li><a href="../formationcontinue/formation.htmlinue?sousmenu=01" title="Formation continue">Formation continue</a></li><li><a href="../international/inter.htmlonal?sousmenu=01" title="International">International</a></li><li><a href="../vie_etudiante/vie_e.htmlante?sousmenu=01" title="Vie �tudiante">Vie �tudiante</a></li><li><a href="../annua.htmlannuaire?sousmenu=01" title="Annuaire">Annuaire</a></li></ul></div><div style="55.3em;margin:0 0.1em 0 0.1em;;height: auto !important;height:26em;min-height:26em;"><div id="menucontextuel" style="width:15em;float:left;margin:0;padding:0;overflow:hidden;height:100%"><div style="width:11.75em;height:25.06em;"><img src="../images/gauche.gif" alt="ISIMA" title="ISIMA" style="width:100%;height:100%" /></div><div id="sousmenu" style="position:relative;margin-top:-25em;background-color:transparent;min-height:25.06em;"></div></div><div id="contenu">
   <h2><a name="debut"></a>Forum Etudiants / Entreprises - 8 et 9 novembre 2007</h2>
   <p>L'ISIMA organise les 8 et 9 novembre prochains un forum
de rencontre Etudiants-Entreprises dans ses locaux.</p>

<p>Les entreprises participantes y disposeront de stands
et pourront compl&eacute;ter ce dispositif par une
conf&eacute;rence au cours de laquelle elles pourront
pr&eacute;ciser leur pr&eacute;sentation.</p>

<p>
Quelques entreprises participantes&nbsp;:
</p>
<table>
<TR>
<TD><a href="http://www.ajilon.fr/">AJILON IT CONSULTING</a></TD>
<TD><IMG HEIGHT="60" width="300px" SRC="logos/ajilon.jpg"></TD>
</TR>
<TR>
<TD><a href="http://www.altran.com">ALTRAN</a></TD>
<TD><IMG HEIGHT="40" SRC="logos/altran.gif"></TD>
</TR>
<TR>
<TD><a href="http://www.apec.fr">APEC</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/apec.png"></TD>
</TR>
<TR>
<TD>ARSAUDI Group SAS</TD>
<TD></TD>
</TR>
<TR>
<TD><a href="http://www.athalia.fr/">ATHALIA Conseil</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/athalia.jpg"</TD>
</TR>
<TR>
<TD><a href="www.atosoriginrecrute.fr">ATOS Origin Systems Integration</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/atos.jpg"></TD>
</TR>
<TR>
<TD>BV associates</TD>
<TD></TD>
</TR>
<TR>
<TD><a href="http://www.fr.capgemini.com/">CAP GEMINI Outsourcing</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/capgemini.gif"></TD>
</TR>
<TR>
<TD><a href="http://www.coframi.fr/">groupe COFRAMI</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/coframi.jpg"></TD>
</TR>
<TR>
<TD><a href="http://www.ca-centrefrance.fr/">CREDIT AGRICOLE Centre France</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/creditagricole.gif"></TD>
</TR>

<TR>
<TD><a href="http://www.edf.fr">EDF</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/EDF.gif"></TD>
</TR>
<TR>
<TD><a href="http://www.genigraph.fr">GENIGRAPH</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/genigraph.jpg"></TD>
</TR>
<TR>
<TD><a href="http://www.harvest.fr">HARVEST</a></TD>
<TD><IMG HEIGHT="40" SRC="logos/harvest.gif"></TD>
</TR>
<TR>
<TD><a href="http://www.homsys.com/homsys/index.htm">HOMSYS GROUP</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/homsys.jpg"></TD>
</TR>
<TR>
<TD><a href="http://www.ibm.com/fr">IBM</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/ibm.jpg"></TD>
</TR>
<TR>
<TD><a href="http://http://cc.in2p3.fr">Centre de calcul in2p3</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/ccin2p3.jpg"></TD>
</TR>
<TR>
<TD><a href="http://http://www.irisa.fr">IRISA</a></TD>
<TD><IMG HEIGHT="40" SRC="logos/irisa.gif"></TD>
</TR>
<TR>
<TD><a href="http://www.medialexie.com">Logolexie/Medialexie</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/medialexie.png"></TD>
</TR>

<TR>
<TD><a href="http://www.orange-business.com">ORANGE Business</a>
/<a href="http://www.silicomp.fr">SILICOMP</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/O.bmp"></TD>
</TR>
<TR>
<TD><a href="http://www.pascalis.net">Pascalis</a></TD>
<TD></TD>
</TR>
<!-- <TR>
<TD><a href="http://www.sii.fr">S I I</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/sii.jpg"></TD>
</TR> -->
<TR>
<TD><A HREF="http://www.letram-clermontferrand.com/"> SMTC</A>
</TD>
<TD></TD>
</TR>
<TR>
<TD><a href="http://www.fr.sogeti.com/">SOGETI R&eacute;gion Rh&ocirc;ne Alpes</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/sogeti.png"></TD>
</TR>
<TR>
<TD><a href="http://www.recrut.sopragroup.com">SOPRA group</a></TD>
<TD><IMG HEIGHT="60" height="120" SRC="logos/sopragroup.jpg"></TD>
</TR>
<TR>
<TD><a href="http://www.groupeumanis.com/">Groupe UMANIS</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/umanis.png"></TD>
</TR>
<TR>
<TD><a href="http://www.unilog.fr">UNILOG (LogicaCMG)</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/unilog.jpg"></a></TD>
</TR>
<TR>
<TD><a href="http://www.cea.fr">CEA Commissariat &agrave; l'&eacute;nergie atomique</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/cea.gif"></TD>
</TR>
<!-- <TR>
<TD><a href="http://www.symeos.fr/">SYMEOS</a></TD>
<TD></TD>
</TR>-->
<TR>
<TD>E-FRONTECH</TD>
<TD>&nbsp;</TD></tr>
<TR>
<TD>CISCO</TD>
<TD>&nbsp;</TD></tr>
<tr>
<TD>GDF</TD>
<TD>&nbsp;</TD></tr>
<tr>
<TD>AUBERT & DUVAL</TD>
<TD>&nbsp;</TD></tr>
<TR>
<TD><a href="http://www.michelin.com">Michelin</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/michelin.jpg"></TD>
</TR>
<TD>C-S</TD>
<TD>&nbsp;</TD></tr>
</table>

<p>Entreprises en attente : </p>

<table>

<TR>
<TD><a href="http://www.3ds.com/fr/">DASSAULT Syst�mes</a></TD>
<TD><IMG HEIGHT="60" SRC="logos/dassaultsystemes.gif"></TD>
</TR>
<TR>
<TD><a href="http://www.mbda-systems.com/mbda/site/FO/scripts/siteFO_accueil.html">MBDA France-Bourges Subdray</a></TD>
<TD></TD>
</TR>
</table>
</div></div><div class="notegauche"><div class="notedroite">ISIMA, Campus des C�zeaux, BP 10125, 63173 Aubi�re CEDEX<br />Pour toute question sur le site, contactez le <script type="text/javascript">document.write('<');document.write('a');document.write(' ');document.write('h');document.write('r');document.write('e');document.write('f');document.write('=');document.write('"');document.write('m');document.write('a');document.write('i');document.write('l');document.write('t');document.write('o');document.write(':');document.write('w');document.write('e');document.write('b');document.write('m');document.write('a');document.write('s');document.write('t');document.write('e');document.write('r');document.write('@');document.write('i');document.write('s');document.write('i');document.write('m');document.write('a');document.write('.');document.write('f');document.write('r');document.write('');document.write('"');document.write('>');document.write('webmestre');document.write('<');document.write('/');document.write('a');document.write('>');</script></div></div></div></div></div></div></div></div></div></div></div></body></html>

