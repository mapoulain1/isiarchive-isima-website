import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;

public class proc extends atomic{
 protected entity job;
 protected int processing_time,state;

 public proc(String  name,int  Processing_time){
  super(name);
  inports.add("none");
  inports.add("in");
  inports.add("setProcTime");
  inports.add("inName");
  outports.add("out");
  outports.add("outName");
  phases.add("busy");
  processing_time = Processing_time;
  job = new entity("job");
 }

 public proc(){
  super("proc");
  inports.add("none");
  inports.add("in");
  inports.add("inName");
  inports.add("setProcTime");
  outports.add("out");
  outports.add("outName");
  phases.add("busy");
  processing_time = 1000;
  addTestPortValue("in",new entity("job"));
  addTestPortValue("inName",
  new pair(this,new entity("job")));
  addTestPortValue("inName",
   new pair(new proc("p",500),new entity("jobIn")));
  addTestPortValue("none",new entity("job"));
  addTestPortValue("setProcTime",new intEnt(500));
  addTestPortValue("setProcTime",new intEnt(1000));
  addTestPortValue("setProcTime",new intEnt(2000));
  initialize();
 }

 public void initialize(){
  phase = "passive";
  state = 0;
  sigma = INFINITY;
  job = new entity("job");
  super.initialize();

 }

 public void showState(){
  super.showState();
  addString("job: "+job.get_name());
  addString("ProcTIme: " + processing_time);
 }

 public void  deltext(int e,message  x){
  Continue(e);
  for(int i=0; i< x.get_length();i++)
     if(message_on_port(x,"setProcTime",i)){
      entity en = x.get_val_on_port("setProcTime",i);
      intEnt in = (intEnt)en;
      processing_time = in.getv();
     }
  if(phase_is("passive")){
   for(int i=0; i< x.get_length();i++)
    if(message_on_port(x,"in",i)){
      job = x.get_val_on_port("in",i);
      hold_in("busy",processing_time);
      state = 1;
    }
   for(int i=0; i< x.get_length();i++)
    if(message_on_port(x,"inName",i)){
      entity ent = x.get_val_on_port("inName",i);
      pair pr = (pair  )ent;
      entity en = pr.get_key();
      proc pn = (proc  )en;
      if(this.equal(pn)){
       job = pr.get_value();
       hold_in("busy",processing_time);
       state = 1;
      }
    }
  }
  //show_state();
 }

 public void  deltint( ){
  passivate();
  state = 0;
  job = new entity("job");
  //show_state();
 }

 public message    out( ){
  System.out.println("Output Function " + name);
  message   m = new message();
  if(phase_is("busy")){
   content   con = make_content("out",job);
   m.add(con);
   con = make_content("outName",new pair(this,job));
   m.add(con);
  }
  return m;
 }

 public void show_state(){
  System.out.println(   "state of  " +  name +  ": " );
  System.out.println(  "phase, sigma,job_id : "
         + phase +  " " +  sigma +  " ");
   //job.print();
  System.out.println( );
 }
}

