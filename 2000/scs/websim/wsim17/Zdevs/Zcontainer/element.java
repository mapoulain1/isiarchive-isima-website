package Zdevs.Zcontainer;
/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
/*                     Filename     : element.java                     */
/*                     Version      : 1.0                            */ 
/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

/**********************************************************************
CLASS ELEMENT: 
	This class is used to construct a singly linked-list of 
	objects.
**********************************************************************/

// element.java - methods definition of class element 

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

// CLASS ELEMENT

public class element extends entity {

/* private */

static final String classname = "element";

protected entity ent;
/* public */

public element() 
{
   ent       = null; // not containing any object initially
} 
 

public element(entity ENT1) 
{
   ent       = ENT1;  // please note:  we just hold the given object
                     // but not make a copy of it.
}


public String get_classname() // use this since classname is private 
{ 
   return classname;  // classification of object  
}

public boolean empty()
{
   if ( ent == null )
      return true;
   return false;
}

public entity get_ent()  // use this since ent is protected 
{ 
   return ent;    // returns the object that element holds. 
} 

public void set_ent(entity e) 
{ 
   ent = e; 
}

/*
public boolean equal(void ENT)
{
   if (ENT == null && get_ent() == null)   
      return true;
   return false;
}
*/

public boolean equal(entity ENT) 
{
   return (get_ent().equal(ENT));
}

public void print()
{
   if ( get_ent() == null )
      System.out.println("(no entity)");
   else
      get_ent().print();
}

public void print_name()
{
   if ( get_ent() == null )
      System.out.println("(no entity)");
   else
      get_ent().print();
}

public String get_name()
{
   if ( get_ent() == null) {
//      return make_name("(no entity)");
return "(no entity)";
   } else
      return (get_ent().get_name());  // name search (recursively)
}

}