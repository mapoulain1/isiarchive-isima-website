/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  nameGen.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs.Zcontainer;

public class nameGen extends bag{

 public nameGen(){
  super();
 }

 public String getName(String s){
  String r = s+number_of(s);
  add(s);
  return(r);
 }
}

