/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  OutDisplay.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;
import java.awt.*;

public class OutDisplay extends Dialog{

 protected devs mySource;
 protected content myContent;
 protected java.awt.List ol;

 public OutDisplay(Frame parent,String title,boolean modal,devs d,content c){
  super(parent, title, modal);
  mySource = d;
  myContent = c;
  setLayout(new FlowLayout(FlowLayout.LEFT));
  addNotify();
  resize(insets().left + insets().right + 161, insets().top + insets().bottom + 188);
  label1=new Label("Src->Dest");
  add(label1);
  label1.reshape(insets().left + 21,insets().top + 20,70,13);
  topText=new TextField(mySource.get_name()+"->"+myContent.devs.get_name());
  add(topText);
  topText.reshape(insets().left + 42,insets().top + 39,70,17);
  labelO=new Label("Src Out");
  add(labelO);
  labelO.reshape(insets().left + 21,insets().top + 20,70,13);
  message mess = mySource.get_output();
  //test for atomic runalone case
  boolean alone = (mess == null);
  if(alone) mess = ((atomic)mySource).getLastOutput();
  java.awt.List li =  new java.awt.List(3, false);
  for(entity p = mess.get_head();p != null;p = p.get_right()) {
     entity ent = p.get_ent(); //since it is element
     content cc =  (content)ent;
     li.addItem(cc.devs.get_name() + " "+ cc.p+" "+cc.val.get_name());
  }
  add(li);
  if(!alone){
    label2=new Label("InPort");
    add(label2);
    label2.reshape(insets().left + 21,insets().top + 68,70,13);
    midText=new TextField(myContent.p);
    midText.reshape(insets().left + 42,insets().top + 88,70,17);
    add(midText);
    label3=new Label("Value");
    add(label3);
    label3.reshape(insets().left + 21,insets().top + 117,70,13);
    botText=new TextField(myContent.val.get_name());
    add(botText);
    botText.reshape(insets().left + 42,insets().top + 135,70,17);
  }
 }

 public boolean handleEvent(Event event){
  if(event.id == Event.GOT_FOCUS && event.target == topText){
    gotFocusTopText(event);
  return true;
  }
  else if(event.id == Event.WINDOW_DESTROY) {
    hide();
    return true;
  }
  return super.handleEvent(event);
 }

 //{{DECLARE_CONTROLS
 TextField topText;
 TextField botText;
 TextField midText;
 Label label1;
 Label label2;
 Label label3;
 Label labelO;
 //}}

 public void gotFocusTopText(Event ev){
  // to do: put event handler code here.
 }
}


