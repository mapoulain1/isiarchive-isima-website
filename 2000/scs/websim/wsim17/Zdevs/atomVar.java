/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atomVar.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import Zdevs.Zcontainer.*;

public class atomVar extends atomic{

  protected digraph d;


  public atomVar(){
    super(name);
    d = new digraph();
    d.add(this);
    new devsVarDrawPanel(d);
  }


}

