/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : devs.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;

import Zdevs.Zcontainer.*;


public class devs extends entity implements Runnable{

 public void addInports(){}

 public void addOutports(){}

 public devs with_name(String nm){
  if(parent == null) return null;
  for(entity p = ((digraph)parent).get_components().get_head();p != null;p = p.get_right()){
      entity pp = p.get_ent();
      if(pp.equal_name(nm) != null) return (devs)pp;
  }
  return null;
 }

 public int getTL(){
   return tL;
 }

 // protected set inports,outports;
 protected ports inports,outports;
 protected message input,output,Input;
 protected int tL,tN;
 public final int INFINITY = 10000000;// Vinc added final
 protected entity parent;


 String classname = "devs";

 public String get_classname(){
  return classname;
 }


public devs(String name){
 super(name);
 /*
  inports = new set();
  outports = new set();
   */
   inports = new ports();
   outports = new ports();
}
//bpz added

public set get_atomic_components()
{
     set r = new set();
     r.add(this);
     return r;
}
/*//add by Vinc
public void simulate_atomic()
{
    atomic a = new atomic();
    a.start();
}
//added by Vinc
public void simulate_digraph()
{
}
*/
public set reverse_get_atomic_components()
{
    return get_atomic_components();
}

//bpz added
public void wrap_wrap(int t,message m)
{
    setInput(m);
}

//bpz added
public void set_tL(int TL){
    tL = TL;
}

public void setInput(message m)
{//BPZ added
  // System.out.println(get_name() + " got input ");
    Input = m;
}

public void initialize()
{
  tL = 0;
  tN = tL + ta();
  output = null;
}

public  int next_tN()
{
  return tN;
}


public void inject(String p, entity val)
{
  inject(p,val,0);
}

public void inject(String p, entity val, int e)
{
  message in = new message();
  content co = make_content(p,val);
  in.add(co);

  wrap_deltfunc(e,in);
}

public void inject_address (String p, addrclass address, entity val, int e)
{
  message inpu = new message();
  content co =  make_content_address(p,val,address);
  inpu .add(co);
  wrap_deltfunc(e,inpu);
}

public void inject_all(String p, entity val, int li, int ui, int lj, int uj)
{
  message in = new message();

  for (int i=li; i<=ui; i++) {
    for (int j=lj; j<=uj; j++) {
      addrclass ad = new addrclass(i,j);
      content co = new content(p,this,ad,val,this);
      in.add(co);
    }
  }
  wrap_deltfunc(0,in);

}

public void inject_address (String p, addrclass address, entity val)
{
  inject_address(p,address,val,0);
}

public void deltfunc(int e,message x){
x.print_all();

    }
public void deltcon(int e,message x){}
public void deltint(){}
public void deltext(int e,message x){}

public void  show_tN(){System.out.println("tN: " + tN ); }
public void  show_tL(){System.out.println(  "tL: " + tL ); }


public message out(){ return null; }
public int ta(){return INFINITY;}
public int elapsed(int t) {return (t-tL);}

public void wrap_deltfunc(int t,message x)
{
//System.out.println( "DEVS WRAP_DELFUNC");
  if (t == tN ||(! x.empty())) {
    deltfunc(t,x);
    tL = t;
    tN = tL + ta();
  }
}

public void wrap_deltext(int t,message x)
{}

public void wrap_deltint(int t,message x)
{}

public content make_content(String p,entity value)
{
    content co;
  if (value != null)
   co = new content(p,this, new addrclass(-1,-1),
       value,this);
  else co = new content(p,this, new addrclass(-1,-1),
        new entity("null value"),this);
  return co;
}


public content make_content_address(String p,entity value, addrclass address)
{
  content co = new content(p,this,address,value,this);
  return co;
}
/*
  try{
    !outports.is_in_name(p);
     }
   catch (Exception e)
   {
   return phase.equals(Phase);
   }
   return false;
    */

public boolean   message_on_port(message x,String p, int i)
{
   if (!inports.is_in_name(p))
    System.out.println( "Warning: model :" + name +  " inport: " + p +
      " has not been declared" );
 return x.on_port(p,i);
}
 /*
  try{
    !inports.is_in_name(p);
     }
   catch (Exception e)
   {
   return phase.equals(Phase);
   }
   return false;
    */

public entity get_parent()
{
  return parent;
}

public void set_parent(entity p)
{
  parent = p;
}


public  void  show_state(){}
public  void  show_output(){}
public void show_input(){}
public void simulate(int num_iter){}
public boolean  equal_tN(int t){ return t==tN;}
public void  compute_input_output(int t){}
public message  get_output(){return output;}
public message  get_input(){return input;}
                 //added by bpz
public void add_coupling(devs c1, String p1, devs c2, String p2) {}
public void add_coupling(String p1, String p2) {}

public void Add_coupling(devs c1, String p1, devs c2, String p2) {}
public void Add_coupling(String p1, String p2) {}

public boolean  address_is_in(container c)
{return false;}
public boolean  address_is_in(addrclass addr)
{return false;}
/*
public set get_inports(){return inports;}
public set  get_outports(){return outports;}
*/

public ports get_inports(){return inports;}
public ports  get_outports(){return outports;}

}
