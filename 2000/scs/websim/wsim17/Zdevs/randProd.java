/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  randProd.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;
import java.lang.*;

public class randProd extends rand{

 public randProd(int stream){
  super(stream);
 }

 public int round100(double d){
     return (int)Math.rint(d);
 }

 public int uniform(int lo, int hi){
  double save = duniform(lo,hi);
  return round100(save);
 }

 public int uniform(int hi){
  return uniform(0,hi);
 }

 public int expon(int mean){
  return round100(dexpon(mean));
 }

 public int normal(int mean,int sig){
  int save = round100(mean + sig*dnormal1());
  if(save < 0)System.out.println("Warning; negaive normal variate");
  return save;
 }
}
