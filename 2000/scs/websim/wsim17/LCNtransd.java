/***************************************************************
	ECE 575
	Homework #5
	Daryl Hild

File:  LCNtransd.java

            +---LCNtransd--------+
     ariv   |     sigma          |  out
     ------>|     phase          |---->
            |     arrived_list   |
     solved |     solved_list    | stop
     ------>|     observ_clock   |---->
            |     total_ta       |
            |     last_pkt       |
            |  ZERO_TIME         |
            |  job_count         |
            +--------------------+

Assumptions:
     1. The transducer is initially in the passive phase.
     2. When the transducer has received "job_count" packets on the
           ariv input port, the stop_gen phase is entered.
     3. When in the stop_gen phase, the transducer continues to
        collect results until the last arrived packet is seen again
        as a solved packet.
     4. Only input and output ports declared below are legitimate.
     5. Values for phase are limited to those specified below;
           all others are disallowed.


DEVS ATOMIC-MODEL:  LCNtransd
     state variables:
           sigma         INFINITY
           phase         passive     (passive, active, stop_gen, report_results)
           arrived_list  nil
           solved_list   nil
           observ_clock  0
           total_ta      0
           last_pkt      nil
     parameters:   ZERO_TIME    100
                   job_count    5
     input-ports:  ariv
        		   solved
     output-ports: out
     external transition function:
       observ_clock = observ_clock + e
       Continue(e)
       for each message on input-port solved
          set job_id to content-value
          case phase
               passive   error: invalid phase for a solved message
               active | stop_gen
                         if (job_id in arrived_list)
                            total_ta = total_ta + observ_clock - arrival_time
                            add(job_id, observ_clock) to solved_list
                         else error: job_id not in arrived_list
                         if phase_is stop_gen && job_id==last_pkt
                            hold_in report_results ZERO_TIME
               else      error: invalid phase
       for each message on input-port ariv
          set job_id to content-value
          case phase
               passive || active
                         add(job_id, observ_clock) to arrived_list
                         if (length_of(arrived_list)=>job_count)
                            hold_in stop_gen ZERO_TIME
                            last_pkt = job_id
                         else
                            hold_in active INFINITY
               stop_gen  error: invalid arrival during stopping phase
               else      error: invalid phase

     internal transition function:
        e_time = tN - tL;
        case phase
             report_results  passivate
             stop_gen        observ_clock = observ_clock + e_time
                             hold_in stop_gen INFINITY
             else            error: invalid phase

     output function:
          case phase
               report_results   send 'AVG TA & THRUPUT' to output-port out
               stop_gen         send 'stop' to output-port out

***************************************************************/

import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;


public class LCNtransd extends atomic {


	protected function  arrived_list, solved_list;
	protected int observ_clock, total_ta, job_count, ZERO_TIME;
	protected entity last_pkt;


	public LCNtransd(String name, int setJob_count, int ZeroTime) {
		super(name);
		inports.add("ariv");
		inports.add("solved");
		outports.add("stop");
		phases.add("active");
		phases.add("stop_gen");
		phases.add("report_results");
		job_count = setJob_count;
		ZERO_TIME = ZeroTime;
		initialize();
	}


	public LCNtransd() {
		super("LCNtransd");
		inports.add("ariv");
		inports.add("solved");
		outports.add("stop");
		phases.add("active");
		phases.add("stop_gen");
		phases.add("report_results");
		job_count = 5;
		ZERO_TIME = 100;  // 100 required by digraph model
		addTestPortValue("ariv",new entity("job1"));
		addTestPortValue("ariv",new entity("job2"));
		addTestPortValue("solved",new entity("job1"));
		addTestPortValue("solved",new entity("job2"));
		initialize();
	}


	public void initialize() {
		phase = "passive";
		sigma = INFINITY;
		arrived_list = new function();
		solved_list = new function();
		observ_clock = 0;
		total_ta = 0;
		last_pkt = new entity("nil");
		super.initialize();
	}


	public void deltext(int et, message x) {
	  observ_clock = observ_clock + e;
	  Continue(e);
	  for (int i=0; i< x.get_length();i++) {
	     if (message_on_port(x,"solved",i)) {
	        entity job_id = x.get_val_on_port("solved",i);
	        if (phase_is("passive")) {
	           System.out.println(name + " EXTERNAL --> ERROR: "
	             + "Invalid phase for a solved message arrival.");
	        }
	        else if (phase_is("active") || phase_is("stop_gen")) {
	           if (arrived_list.key_name_is_in(job_id.get_name())) {
	              entity  ent = arrived_list.assoc(job_id.get_name());
	              intEnt  num = (intEnt)ent;
	              int arrival_time = num.getv();
	              total_ta = total_ta + observ_clock - arrival_time;
	              solved_list.add(job_id, new intEnt(observ_clock));
	              System.out.println(name + " EXTERNAL --> packets received: "
	                 + solved_list.get_length());
	           }
	           else {
	              System.out.println(name + " EXTERNAL --> ERROR: "
	                + "Invalid solved job received.");
	           }
	           if (phase_is("stop_gen") && job_id.eq(last_pkt.get_name())) {
	              hold_in("report_results", ZERO_TIME);
	              System.out.println(name + ": observations complete.");
	           }
	        }
	        else   System.out.println(name + " EXTERNAL --> ERROR: Invalid phase.");
	     }
	  } // endcase for each message on input-port solved
	  for (int i=0; i< x.get_length();i++) {
	     if (message_on_port(x,"ariv",i)) {
	        entity job_id = x.get_val_on_port("ariv",i);
	        if (phase_is("passive") || phase_is("active")) {
	           arrived_list.add(job_id,new intEnt(observ_clock));
	           if (arrived_list.get_length() >= job_count) {
	              hold_in("stop_gen", ZERO_TIME);
	              last_pkt = job_id;
	              System.out.println(name + " EXTERNAL --> observation period ending.");
	           }
	           else {
	              hold_in("active", INFINITY);
	           }
	        }
	        else if (phase_is("stop_gen")) {
	           System.out.println(name + " EXTERNAL --> "
	             + "ERROR: Invalid arrival during stopping phase.");
	        }
	        else {
	           System.out.println(name + " EXTERNAL --> ERROR: Invalid phase.");
	        }
	     }
	  } // endcase for each message on input-port ariv
//	  show_state(" EXTERNAL --> ");
	}


	public void deltint() {
	   int et = tN - tL;
	   if (phase_is("report_results")) {
	      passivate();
	      System.out.println(name + " INTERNAL --> simulation done.");
	      System.out.println("PACKETS SENT:  " );
	      arrived_list.print_all();
	      System.out.println("     total packets sent:  " + arrived_list.get_length());
	      System.out.println("PACKETS RECEIVED:  ");
	      solved_list.print_all();
	      System.out.println("     total packets received:  " + solved_list.get_length());
	      System.out.println("     AVG TA = " + compute_TA());
	      System.out.println("     THRUPUT = " + compute_Thru());
	   }
	   else if (phase_is("stop_gen")) {
	      observ_clock = observ_clock + et;
	      hold_in("stop_gen", INFINITY);
//	      System.out.println(name + " INTERNAL --> observation period ending.");
	   }
	   else System.out.println(name + " INTERNAL --> ERROR: Invalid phase.");
//	   show_state(" INTERNAL --> ");
	}


	public message out() {
	   message m = new message();
	   if (phase_is("report_results")) {
	      content  con = make_content("out", new entity(
	         "     AVG TA: " + compute_TA() + "     THRUPUT: " + compute_Thru()));
	      m.add(con);
	   }
	   else if (phase_is("stop_gen")) {
	      content con = make_content("stop", new entity("stop"));
	      m.add(con);
	   }
	   return m;
	}


	public float compute_TA() {
		float avg_ta_time = 0;
		if (!solved_list.empty())
			avg_ta_time = total_ta/solved_list.get_length();
		return avg_ta_time;
	}


	public float compute_Thru() {
		float thruput = 0;
		if (observ_clock > 0)
			thruput = solved_list.get_length()/(float)observ_clock;
		return thruput;
	}


	public void  show_state(String header) {
		System.out.println(name + header);
		System.out.println("     state of " + name
		    + ":    phase: " + phase
		    + ",    sigma: " + sigma
		    + ",    observ_clock: " + observ_clock
		    + ",    total_ta: " + total_ta
		    + ",    tL: "    + tL
		    + ",    tN: "    + tN
		    + ",    total packets sent: " + arrived_list.get_length()
		    + ",    total packets received: " + solved_list.get_length());
	}
}
