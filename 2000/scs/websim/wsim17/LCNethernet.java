/***************************************************************
	ECE 575
	Project:  Distributed Object Computing
	Daryl Hild

File:  LCNethernet.java

          +-----LCNethernet-------------------------------------------+
          |                                                           |
          |                stop         out                           |LCNresults
          |     +--------------LCNtransd-----------------+----------->|--------->
          |     |              ^ariv  ^solved            |            |
          |     V              |      |------------+     |stop        |
 startStop|startStop       outX|     / \           |     V         out|ETHERresults
 -------->|------->PKT_GENR-+--+-----+-----------+ |    ETHERtransd-->|--------->
          |                 |     /  |  |   ...  | |          ^in     |
          |                 |   /    |  |        | |          |       |
          |                 | /      |  |        | |          |       |
          |               in||out  in|  |out   in| |          |       |
          |                 V|       V  |        V |          |       |
          |                NODE_1   NODE_2  ... NODE_X        |       |
          |                 | ^      | ^         | ^          |       |
          |                 | |      | |         | |          |       |
          |        media_out+=|======+=|=========+ |          |       |
          |                 | |        |           |          |       |
          |                 | +========+===========+media_in  |       |
          |                 |                      |          |       |
 in       |                 V     in         out   |          |       |  out
 -------->|---------------->+------->ETHERNET----->+----------+------>|---->
          |                                                           |
          +-----------------------------------------------------------+

Assumptions:
     1. Only input and output ports declared below are legitimate.


DEVS DIGRAPH-MODEL:  LCNethernet
     composition tree:  root:  LCNethernet
                      leaves:  ETHERNET, LCNtransd, ETHERtransd, PKT_GENR,
                                 NODE_1, NODE_2, ..., NODE_X
     external input coupling:  LCNethernet.in        -> ETHERNET.in
                               LCNethernet.startStop -> PKT_GENR.startStop
     external output coupling: ETHERNET.out    -> LCNethernet.out
                               LCNtransd.out   -> LCNethernet.LCNresults
                               ETHERtransd.out -> LCNethernet.ETHERresults
     internal input coupling:  NODE_X.media_out -> ETHERNET.in
                               ETHERNET.out -> NODE_X.media_in
                               ETHERNET.out -> ETHERtransd.in
                               PKT_GENR.outX -> NODE_X.in
                               PKT_GENR.outX -> LCNtransd.ariv
                               LCNtransd.stop -> PKT_GENR.startStop
                               LCNtransd.out  -> ETHERtransd.stop
                               NODE_X.out -> LCNtransd.solved
     influencee digraph:       NODE_X -> ETHERNET
                               ETHERNET -> NODE_X
                               ETHERNET -> ETHERtrands
                               PKT_GENR -> NODE_X
                               PKT_GENR -> LCNtransd
                               LCNtransd -> PKT_GENR
                               LCNtransd -> ETHERtransd
                               NODE_X -> LCNtransd
     priority list:            ETHERNET, PKT_GENR, LCNtransd, ETHERtransd,
                               NODE_1, NODE_2, ..., NODE_X

***************************************************************/


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;


public class LCNethernet extends digraph {


	public LCNethernet() {
	    super("LCNethernet");
	    String LANname="ETHERNET", NODEnames="NODE_";
	    int NumNodes=3, Load=40, PropBytes=8, ZeroTime=100, RcvSvcTime=0, pktCount=5;
	    make(LANname, NODEnames, NumNodes, Load, PropBytes, ZeroTime,
	      RcvSvcTime, pktCount);
	}


	public LCNethernet(String Name, String LANname, String NODEnames, int NumNodes,
	  int Load, int PropBytes, int ZeroTime, int RcvSvcTime, int pktCount) {
	    super(Name);
	    make(LANname, NODEnames, NumNodes, Load, PropBytes, ZeroTime,
	      RcvSvcTime, pktCount);
	}


	private void make(String LANname, String NODEnames, int NumNodes,
	  int Load, int PropBytes, int ZeroTime, int RcvSvcTime, int pktCount) {

	   inports.add("in");
	   inports.add("startStop");
	   outports.add("out");
	   outports.add("LCNresults");
	   outports.add("ETHERresults");

	   atomic ETHERNET = new Ethernet(LANname, PropBytes, ZeroTime);
	   add(ETHERNET);
	   Add_coupling(this,"in",ETHERNET,"in");
	   Add_coupling(ETHERNET,"out",this,"out");

	   atomic PKT_GENR = new PKTgenr("PKT_GENR", NODEnames, NumNodes, Load, ZeroTime);
	   add(PKT_GENR);
	   Add_coupling(this,"startStop",PKT_GENR,"startStop");

//	   atomic transd = new transd("transd", 10000);     //AW
//	   add(transd);                                     //AW
//	   Add_coupling(ETHERNET, "out", transd, "solved"); //AW
//	   Add_coupling(transd, "out", this, "out");        //AW

	   atomic LCNtransd = new LCNtransd("LCNtransd", pktCount, ZeroTime);
	   add(LCNtransd);
	   Add_coupling(LCNtransd,"out",this,"LCNresults");
	   Add_coupling(LCNtransd,"stop",PKT_GENR,"startStop");

	   atomic ETHERtransd = new ETHERtransd("ETHERtransd", ZeroTime);
	   add(ETHERtransd);
	   Add_coupling(ETHERtransd,"out",this,"ETHERresults");
	   Add_coupling(ETHERNET,"out",ETHERtransd,"in");
	   Add_coupling(LCNtransd,"out",ETHERtransd,"stop");

	   addTestPortValue("startStop", new intEnt(10));
	   addTestPortValue("startStop", new intEnt(20));
	   addTestPortValue("startStop", new intEnt(30));
	   addTestPortValue("in", new pair(
	      new pair(new entity("dst1"),new entity("src2")),
	      new pair(new intEnt(1500),new entity("test_payload_1"))));
	   addTestPortValue("in", new pair(
	      new pair(new entity("dst1"),new entity("src2")),
	      new pair(new intEnt(500),new entity("test_payload_2"))));
	   addTestPortValue("in", new pair(
	      new pair(new entity("dst1"),new entity("src2")),
	      new pair(new intEnt(10),new entity("test_payload_3"))));

	   for (int i=1; i<=NumNodes; i++) {
	      MAU node = new MAU(NODEnames + i, PropBytes, ZeroTime, RcvSvcTime);
	      add(node);
	      Add_coupling(PKT_GENR,"out"+i,node,"in");
	      Add_coupling(PKT_GENR,"out"+i,LCNtransd,"ariv");
	      Add_coupling(node,"out",LCNtransd,"solved");
	      Add_coupling(node,"media_out",ETHERNET,"in");
	      Add_coupling(ETHERNET,"out",node,"media_in");

//	      //Add_coupling(node,"media_out",transd, "ariv");// - AW
//	      Add_coupling(node, "out", transd, "solved");  //AW
//	      Add_coupling(ETHERNET, "out", transd, "ariv");
	   }

	   initialize();

	// show_coupling();
	}
}
