/***************************************************************
	ECE 575
	Homework #5
	Daryl Hild

File:  ETHERtransd.java

               +---ETHERtransd-----+
         in    |     sigma         |  out
         ----->|     phase         |---->
               |     Carriers      |
         stop  |     Successes     |
         ----->|     Collisions    |
               |     observ_clock  |
               |     total_ta      |
               |     media_state   |
               +-------------------+

Assumptions:
     1. The transducer is initially in the passive phase.
     2. When the transducer receives a message on the stop port,
           the report_results phase is entered.
     4. Only input and output ports declared below are legitimate.
     5. Values for phase are limited to those specified below;
           all others are disallowed.


DEVS ATOMIC-MODEL:  ETHERtransd
     state variables:
                   sigma         INFINITY
                   phase         passive     (passive, report_results)
                   Carriers      nil
                   Successes     nil
                   Collisions    nil
                   observ_clock  0
                   total_ta      0
                   media_state   idle
     parameters:   ZERO_TIME     100
     input-ports:  in
        		   stop
     output-ports: out

     external transition function:
       observ_clock = observ_clock + e
       Continue(e)
       for each message on input-port in
         set frame to content-value
         case media_state
              idle            if frame==Preamble
                                 media_state = single_carrier
                                 add(frame, observ_clock) to Carriers
                              else
                                 media_state = idle
              single_carrier  if frame==Preamble
                                 media_state = multi_carriers
                                 add(frame, observ_clock) to Carriers
                                 add(frame, observ_clock) to Collisions
              multi_carriers  if frame==NoiseBurst
                                 media_state = idle
                              else if frame==Preamble
                                 add(frame, observ_clock) to Carriers
       if frame!=Preamble && frame!=NoiseBurst && media_state==single_carrier
           //transmission done; ether idle
           media_state = idle
           add(frame, observ_clock) to Successes
       for each message on input-port stop
           hold_in report_results ZERO_TIME

     internal transition function:
       passivate

     output function:
       send 'results' to output-port out

***************************************************************/

import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;


public class ETHERtransd extends atomic {


	protected function Carriers, Collisions, Successes;
	protected int observ_clock, total_ta, ZERO_TIME;
	protected String media_state;


	public ETHERtransd(String name, int ZeroTime) {
		super(name);
		inports.add("stop");
		phases.add("report_results");
		ZERO_TIME = ZeroTime;
		initialize();
	}


	public ETHERtransd() {
		super("ETHERtransd");
		inports.add("stop");
		phases.add("report_results");
		ZERO_TIME = 100;  // 100 required by digraph model
		addTestPortValue("in",new entity("Preamble"));
		addTestPortValue("in",new entity("frame"));
		addTestPortValue("in",new entity("NoiseBurst"));
		addTestPortValue("stop",new entity("stop2"));
		initialize();
	}


	public void initialize() {
		phase = "passive";
		sigma = INFINITY;
		Carriers = new function();
		Collisions = new function();
		Successes = new function();
		observ_clock = 0;
		total_ta = 0;
		media_state = "idle";
		super.initialize();
	}


	public void deltext(int et, message x) {
	    observ_clock = observ_clock + e;
	    Continue(e);
	    entity frame = new entity("nil");
	    for (int i=0; i< x.get_length();i++) {
	        if (message_on_port(x,"in",i)) {
	            frame = x.get_val_on_port("in",i);
	            if (media_state=="idle") {
	                if (frame.eq("Preamble")) {
	                    media_state="single_carrier";
	                    Carriers.add(frame, new intEnt(observ_clock));
	                }
	                else {
	                    media_state="idle";
	                }
	            }
	            else if (media_state=="single_carrier") {
	                if (frame.eq("Preamble")) {
	                    media_state="multi_carriers";
	                    Carriers.add(frame, new intEnt(observ_clock));
	                    Collisions.add(frame, new intEnt(observ_clock));
	                }
	            }
	            else { //media_state="multi_carriers"
	                if (frame.eq("NoiseBurst")) {
	                    media_state="idle";
	                }
	                else if (frame.eq("Preamble")) {
	                    Carriers.add(frame, new intEnt(observ_clock));
	                }
	            }
	        }
	    }
	    if (!frame.eq("Preamble") && !frame.eq("NoiseBurst")
	      && media_state=="single_carrier") {
	        // transmission done; ether idle
	        media_state = "idle";
	        Successes.add(frame, new intEnt(observ_clock));
	    }
	    for (int i=0; i< x.get_length();i++) {
	       if (message_on_port(x,"stop",i)) {
	          hold_in("report_results", ZERO_TIME);
	          System.out.println(name + ": observations complete.");
	       }
	    }
//	    show_state(" EXTERNAL --> ");
	}


	public void deltint() {
	   System.out.println(name + " INTERNAL --> Results:"
	     + "  Number carriers: " + Carriers.get_length()
	     + "  Number collisions: " + Collisions.get_length()
	     + "  Number successful transmissions: " + Successes.get_length());
	   passivate();
//	   show_state(" INTERNAL --> ");
	}


	public message out() {
	   message m = new message();
	   content  con = make_content("out", new entity(
	         "Number carriers: " + Carriers.get_length()
	         + "  Number of collisions: " + Collisions.get_length()
	         + "  Number of successful transmissions: " + Successes.get_length()));
	   m.add(con);
	   return m;
	}


	public void  show_state(String header) {
		System.out.println(name + header);
		System.out.println(
		      "  phase: " + phase
		    + ",  sigma: " + sigma
		    + ",  observ_clock: " + observ_clock
		    + ",  total_ta: " + total_ta
		    + ",  tL: "    + tL
		    + ",  tN: "    + tN
		    + ",  total carriers detected: " + Carriers.get_length()
		    + ",  total collisions detected: " + Collisions.get_length()
		    + ",  total successful transmissions: " + Successes.get_length());
	}
}
