
>From Pierre.Wodey@isima.fr  Tue Jan  7 14:31:01 2003



Toujours Infineon Munich, propositions de stages qui peuvent
convenir � des F1 et F2 (meme si cela concerne SystemC).

Pour contact et autres, passer par moi.

ces sujets sont tres interessant et se situent en Central R&D de Infineon.

Bien cordialement,

Pierre.


-- 
-,,,-|  Pierre Wodey                  -|-  tel : +33(0)473405012      
--
-,,,-|  ISIMA/LIMOS                   -|-  fax : +33(0)473405001      
--
-,,,-|  BP 1025                       -|- mail : Pierre.Wodey@isima.fr
--
-,,,-|  63173 AUBIERE CEDEX - FRANCE  -|-                             
-->
	
Subject: internships
Date: Tue, 7 Jan 2003 12:19:01 +0100 

 I have other interesting SystemC topics as well, where I
would very appreciate to cooperate with you as a SystemC expert. These are
"extension of systemc with slave API" and "dynamic module overloading". I am
sure that, if we will have success in work on this topics we can make a
publication (at least in SystemC forum, but I think about some good
conference). If you have interested and motivated student with good C++
knowledge (he will have to improve systemc kernel and write dll's), or at
leased willing to deeply hack into C++ please encourage him to contact me.
We are open to discuss other theoretic SystemC topics as well. I inculded
the Infineon application form, which he should fill and send with his CV.

With best wishes from Munich,

Ilia


