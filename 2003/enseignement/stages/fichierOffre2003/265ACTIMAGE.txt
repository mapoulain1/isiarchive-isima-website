
>From clementine@actimage.net  Fri Feb  7 12:09:17 2003




Nous avons le plaisir de vous soumettre, 5 offres de stages pour vos
�tudiants.

ACTIMAGE, soci�t� de conseil en hautes technologies, intervient en mode
organisationnel et fonctionnel aupr�s d'entreprises et d'institutions
structur�es autour de projets strat�giques � forte valeur ajout�e.

Notre m�tier nous permet d'apporter une r�ponse globale dans les secteurs de
l'agroalimentaire, de l'automobile, de la banque, de la distribution, de
l'�lectronique, de l'�nergie, de l'a�ronautique, des institutions, de la
m�decine, du multim�dia, des transports,...

La nature de notre intervention s'adapte aux besoins sp�cifiques de nos
clients dans les domaines suivants :
o INGENIERIE LOGICIELLE
o INGENIERIE MULTIMEDIA
o INGENIERIE RESEAUX et TELECOMS
o FORMATION

Nos prestations d'Ing�nierie ont pour objet de fournir � nos clients des
services techniques int�grant une d�marche qualit� et une m�thodologie
adapt�e.

Les comp�tences graphiques et technologiques d'ACTIMAGE dans le secteur du
multim�dia nous permettent de d�marquer nos clients de leurs concurrents en
leur offrant un outil de promotion visuelle et audiovisuelle externe et/ ou
interne sur tout support multim�dia (Web, Cd-rom, borne interactive).

Notre force vive se compose d'Ing�nieurs et de Consultants dont l'ambition
est la diffusion des nouvelles technologies de communication dans tous les
secteurs d'activit�s � fort potentiel de d�veloppement.

Nous nous tenons � votre disposition pour tout compl�ment d'information.
Je vous prie d�agr�er, Monsieur, l�expression de nos sentiments les
meilleurs.


Cl�mentine GASS
info@actimage.net

      		A C T I M A G E
Software, Network, Telecom & Multimedia Engineering
                 www.actimage.net

Architecture Logicielle
-----------------------
Au-del� des aspects m�thodes, outils et normes de d�veloppement, vous
souhaitez disposer d'une bonne culture des Syst�mes d'Information et avez
d�j� de bonnes connaissances des nouvelles technologies.
Vous souhaitez apprendre � ma�triser les phases de conception, de
mod�lisation, d'architecture applicative et d'int�gration des NTIC dans les
SI. Vous aborderez les domaines suivants : UML, C, C++, Java, technologies
internet.
Rejoignez une �quipe performante et dynamique, adressez directement vos
candidatures � l'attention de la Direction des Ressources Humaines �
ACTIMAGE, 1 rue Saint L�on 67000 STRASBOURG . http://www.actimage.net- Email
:info@actimage.net
_______________________________________________________________________
D�veloppements Temps r�el/embarqu�
----------------------------------
Int�gr� � une �quipe de d�veloppements logiciels, vous intervenez de la
d�finition des sp�cifications jusqu'� la validation finale et plus
particuli�rement sur la conception, le d�veloppement et l'int�gration de
logiciels embarqu�s.
Vous poss�dez des comp�tences en d�veloppement de syst�mes en temps r�el, en
langage C, C++ et en assembleurs.
Rejoignez une �quipe performante et dynamique, adressez directement vos
candidatures � l'attention de la Direction des Ressources Humaines �
ACTIMAGE, 1 rue Saint L�on 67000 STRASBOURG . http://www.actimage.net -
Email
:info@actimage.net
______________________________________________________________________
D�veloppements Logiciels
------------------------
Au sein de notre �quipe D�veloppements, vous participerez � la d�finition
des solutions applicatives les plus appropri�es pour r�pondre aux besoins
exprim�s par nos clients.
Vous avez des connaissances en C, C++, Java, technologies Internet.
La ma�trise de l'Anglais est indispensable et celle de l'Allemand serait un
atout suppl�mentaire.
Rejoignez-nous !
Merci d'adresser vos candidatures � l'attention de la Direction des
Ressources Humaines �  ACTIMAGE, 1 rue Saint L�on 67000 STRASBOURG .
http://www.actimage.net- Email :info@actimage.net
______________________________________________________________________
Conceptions Infographiques
--------------------------
Au sein de nos �quipes, vous participez � la r�alisation de sites internet,
intranet et extranet innovants et cr�atifs.
En collaboration avec nos consultants, vous intervenez sur l'�laboration des
solutions graphiques adapt�es aux besoins de nos clients.
Vous participez � la conception visuelle de nos sites Web et de la
d�finition de la ligne graphique et vous suivez la d�clinaison visuelle par
projet avec les �quipes de webdesigners et de d�veloppeurs
Vous �tes tr�s cr�atif, motiv� et sensible aux innovations graphiques et
technologiques du web.
Vous avez une bonne culture Web, vous ma�trisez illustrator ou photoshop,
vous connaissez flash 4 et avez des notions de programmation html.
Votre esprit d'�quipe et votre dynamisme sera un atout indispensable pour
r�ussir dans ce stage.
Merci d'adresser vos candidatures � l'attention de la Direction des
Ressources Humaines � ACTIMAGE, 1 rue Saint L�on 67000 STRASBOURG.
http://www.actimage.net- Email:info@actimage.net
_______________________________________________________________________
D�veloppements sp�cifiques / Serveurs d'application
---------------------------------------------------
Vous avez des connaissances en JAVA, JSP ou  XML et vous souhaitez approcher
le d�veloppement sp�cifique sur des serveurs d'applcations tels que
Websphere, JBoss et/ou WebLogic.
Vous souhaitez approfondir vos connaissances de l'environnement Windows NT
avec des notions des r�seaux IP.
La  pratique de l'Anglais et /ou de l'Allemand serait fortement appr�ci�e.
Rejoignez-nous !
Merci d'adresser vos candidatures � l'attention de la Direction des
Ressources Humaines � ACTIMAGE, 1 rue Saint L�on 67000 STRASBOURG.
http://www.actimage.net- Email:info@actimage.net

Cordialement,

Cl�mentine GASS

ACTIMAGE
1 rue St L�on F-67000 STRASBOURG
Tel: + 33 (0)3 90 23 63 63 Fax: + 33 (0)3 90 23 63 64
Gsm: + 33 (0)6 08 36 92 31 Email: info@actimage.net
http://www.actimage.net

