

>From contact@directrecrutement.com  Thu Jan 23 16:49:21 2003

Subject: Offres de stages www.directetudiant.com


Bonjour, 

Nous vous prions de trouver ci-dessous les= offres de stage d�pos�es par les entreprises sur= notre site DirectEtudiant. Celles-ci souhaitent proposer= sp�cifiquement ces offres aux �tudiants de votre= �cole
Nous vous remercions de leur transmettre ces offres, et restons � votre disposition pour tout renseignement compl�mentaire
Cordialement,

L'�quipe de Direct= Performance
contact@direct-p= erformance=2Ecom
01=2E42=2E93=2E17=2E50= 

= 

Intitul� du poste: DEfinition dEtaillEe de modEles optoElectroniques pour la simulation d'un systEme multi-constituants (VBVT/01)

R�f�rence : MSY/02-03/URD09/VBVT/01= 

Entreprise : SAGEM SA

Descriptif du poste= : DESCRIPTIF DU STAGE :

CONTEXTE : 
Ce projet vise  la rEalisation d'un systEme optoE9lectronique complexe La premiEre phase consiste  modEliser entiErement le systEme de maniEre � spEcifier les constituants du syst�me, et en d�finir l'architecture et l'organisation Une description pr�liminaire des mod�les= (capteurs, ecartom�trie, imagerie, syst�mes optiques,= environnement atmosph�rique....) n�cessaires � la simulation Et � la r�alisation Cette description doit maintenant etre v�rifi�e, d�finie et compl�t�e

DESCRIPTION DE LA TACHE :
Le stagiaire se basera sur la definition de l'architecture du= systeme et la description preliminaire des mod�les pour :
- �tablir une sp�cification d�taill�e du contenu des mod�les= (caract�ristiques techniques, param�trages, entre les-sorties)= dans l'objectif d'en r�aliser le codage
- d�finir pr�cis�ment la logique de l'agencement des mod�les= entre eux (interfaces, logique d'appel, horloge)

Le stagiaire s'appuiera sur les comp�tences de sp�cialistes en= asservissement et en optique

DUREE :
3 mois de mars � septembre 2003

SPECIFICITES DU STAGE :
- contacts inter-services en interne et avec des sous-traitants= ext�rieurs pour la r�alisation de certains mod�les
- devoir assurer la synth�se et v�rifier la coh�rence des= informations re�ues

PROFIL:
Connaissances requises : imagerie, capteurs
Connaissance souhait�es : optique, asservissements, lasers
Aptitudes particuli�res : synth�se technique multiple, r�daction= technique
Niveau 3eme ann�e =Ecole d'ing�nieur

CONTACT : 
adresse e-mail : claire.cerveau@sagem.com

adresse postale : 
SAGEM SA
A l'attention de Claire CERVEAU
178 RUE DE PARIS
91344 MASSY CEDEX




P�riode : mars  septembre= 2003

Fonction/Secteur := Electronique-Optique-Electricit�

Zone= g�ographique : MASSY

Contact : claire.cerveau@sagem.com<

Lien= vers le site 

Intitul� du poste: Optimisation d'un= logiciel embarqu� de conduite de tir

R�f�rence : MSY URD09/VBVT/05= 

Entreprise : SAGEM SA

Descriptif du poste= : Ref stage : MSY URD09/VBVT/05

2eme ann�e =Ecole d'ing�nieurs option informatique

DUREE : 5 mois mini d�s que possible

DESCRIPTIF DU SUJET :

Le but est d'apporter diverses am�liorations logicielles au= systeme SAS de SAGEM (Conduite de Tir Anti-a�rienne)
Le stage comprend ainsi trois parties completement autonomes= :

- Automatisation de la baie de mise en oeuvre (BMO) : il s'agit= d'un logiciel tournant sur PC permettant d'espionner le= fonctionnement temps reel du logiciel embarqu� SAS � travers une= liaison RS422 Il est possible, via des fichiers de= configuration, de surveiller les �v�nements du syst�me et les= valeurs de variables temps r�elles (cadence 25 Hz, 125 Hz ou 626= Hz), d'afficher des valeurs instantann�es ou des courbes et= d'enregistrer les r�sultats Il est �galement possible= d'espionner des variables non pr�vues en fournissant leur= adresse m�moire

Le but de cette partie du stage consiste � automatiser la saisie= des variables par adresse en proposant une liste de ces= variables issue du fichier "map" obtenu � l'=�dition des liens du= logiciel embarqu� L'ergonomie actuelle ne permet qu'une saisie= manuelle variable par variable

- R��criture du filtre trajectographie : pour des raisons de= performance, le filtre actuel est cod� en assembleur Il= effectue des calculs de type matriciel, trigonom�trique et= changement de rep�re

Le but de cette partie du stage consiste � r��crire le filtre en= langage C en s'attachant � optimiser et valider les performances= (cas des cibles en �loignement et �tude des d�bordements)

- Analyse de l'algorithme de traitement d'image et r��criture du= logiciel : pour des raisons de performance, le traitement actuel= est cod� en assembleur Il effectue des calculs de d�tection de= contraste, filtrage et recherche de cible sur une image= num�ris�e � la cadence du temps r�el (25 Hz)

Le but de cette partie du stage consiste � r��crire les= traitements en langage C de mani�re =� pouvoir les r�utiliser sur= d'autres cibles mat�rielles en s'attachant � optimiser et= valider les performances (temps r�el)

CONNAISSANCES SOUHAITABLES :

- INDISPENSABLES : Langage C, technique de d�veloppement en= Assembleur

- SOUHAITE : LabWindows / CVI (National Instruments), Assembleurs= TMS34020 / TMS320C26 (Texas Instrument)

CONTACT :
Adresse e-mail : claire.cerveau@sagem.com

Adresse postale :
SAGEM SA
A l'attention de Claire CERVEAU
178 Rue de Paris
91344 MASSY CEDEX





P�riode : d�s que= possible

Fonction/Secteur := Informatique-Math�matique-Telecom

Zone= g�ographique : MASSY

Contact : claire.cerveau@sagem.com<

Lien= vers le site 

