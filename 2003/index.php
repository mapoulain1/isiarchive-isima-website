<html>

<head>

<title>ISIMA : Institut Sup&eacute;rieur d'Informatique, de Mod&eacute;lisation et des Applications</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<SCRIPT LANGUAGE="javascript">

<!--
	entrezrub = new Image();

	entrezrub.src = "images/rub/entrez.jpg";

	bderub = new Image();

	bderub.src = "images/rub/bde.jpg";

	admissionrub = new Image();

	admissionrub.src = "images/rub/admission.jpg";

	contactsrub = new Image();

	contactsrub.src = "images/rub/contacts.jpg";

	enseignementrub = new Image();

	enseignementrub.src = "images/rub/enseignement.jpg";

	partenariatsrub = new Image();

	partenariatsrub.src = "images/rub/partenariats.jpg";

	rechercherub = new Image();

	rechercherub.src = "images/rub/recherche.jpg";

	planrub = new Image();

	planrub.src = "images/rub/plan.jpg";

       	anelisrub = new Image();

	anelisrub.src = "images/rub/anelis.jpg";


// Change les rubriques

  	function echangerub(rub)

  	{ 

	   browser=navigator.appName;

	   if ( browser == "Netscape" )

	       {

    	        objet = eval("document.crubrique.document.images.rubrique");

	       }

	   else 		

    	       objet = eval("crubrique.document.images.rubrique");

	   objetsource=eval(rub);

    	   objet.src = objetsource.src;

  	}

//-->

</SCRIPT>




</head>

<script>function ouvrir_page(url,name){mywindow = window.open(url,name, ' menubar=no,resizable=no,dependent,status=no,width=315,height=255,left=20,top=30')}</script><body background="images/fondbleu.jpg" text="#FFFFFF" link="#9999CC" vlink="#9999CC" alink="#9999CC" bgcolor="010066">


<table width="758" border="0" cellspacing="0" cellpadding="0" background="images/fondbleu.jpg" vspace="0" hspace="0" height="406">
  <tr> 
    <td height="112" colspan="3"> 
      <div id="Layer1" style="position:absolute; width:558px; height:98px; z-index:1; left: 0px; top: 1"><img src="images/isima.jpg" width="558" height="98"></div>
      <div id="Layer3" style="position:absolute; width:300px; height:101px; z-index:3; left: 489px; top: 5px">
        <p><b><font face="Courier New, Courier, mono" color="#CCFFCC"><b><font color="#CCCCFF">[</font></b></font> 
          <a href="./isima/admiss.html" target="_blank">Comment int&eacute;grer 
          ISIMA ?</a> <font face="Courier New, Courier, mono" color="#CCFFCC"><b><font color="#CCCCFF">]</font><font color="#66FF66"><br>
          <font color="#CCCCFF">[</font></font></b></font><font color="#CCFFCC"><b><font color="#66FF66"> 
          </font></b></font><a href="./isima/stages.html" target="_blank">Comment 
          proposer un stage &agrave; ISIMA ?</a> <font face="Courier New, Courier, mono" color="#CCFFCC"><b><font color="#CCCCFF">]</font><font color="#66FF66"><br>
          </font></b></font></b><font face="Courier New, Courier, mono" color="#CCFFCC"><b><font color="#CCCCFF">[</font></b></font><b> 
          <a href="isima/admission.html" target="_self">T&eacute;l&eacute;chargez 
          le dossier de candidature</a></b> <font face="Courier New, Courier, mono"><b><font color="#CCCCFF">]</font></b></font></p>
      </div>
    </td>
  </tr>
  <tr bgcolor="010066"> 
    <td colspan="3" height="406"> 
      <div align="left"></div>
      <div id="Layer4" style="position:absolute; width:125px; height:96px; z-index:4; left: 297px; top: 485px"> 
        <div align="left"><a href="http://www.univ-bpclermont.fr/" target="_blank"><img src="images/univbp.gif" width="150" height="89" border="0"></a></div>
      </div>
      <img src="images/accueil.jpg" border="0" usemap="#accueil"> 
      <div id="crubrique" style="position:absolute; width:206px; height:28px; z-index:5; left: 266px; top: 402px"><img src="images/rub/entrez.jpg" width="206" height="28" name="rubrique"></div>
      <div id="Layer2" style="position:absolute; width:90px; height:45px; z-index:2; left: 675px; top: 467px"> 
        <p align="center"><font color="#FFFFFF" size="2" face="Times New Roman, Times, serif"><a href="isima/anglais/accueil.html"><img src="images/anglais.gif" width="68" height="50" align="absmiddle" border="0"><br>
          </a><a href="isima/anglais/accueil.html"><i><b>English Version</b></i></a></font></p>
      </div>
      <map name="accueil">
        <area shape="rect" coords="661,164,697,198" href="./isima/plan.html" target="_self" alt="PLAN DU SITE"
  ONMOUSEOVER="echangerub('planrub')"
  ONMOUSEOUT="echangerub('entrezrub')">
        <area shape="rect" coords="659,121,694,154" href="./isima/contacts.html" target="_self" alt="CONTACTS"
  ONMOUSEOVER="echangerub('contactsrub')"
  ONMOUSEOUT="echangerub('entrezrub')">
        <area shape="rect" coords="48,254,92,287" href="./isima/partenariat.html" target="_self" alt="PARTENARIATS"
  ONMOUSEOVER="echangerub('partenariatsrub')"
  ONMOUSEOUT="echangerub('entrezrub')">
        <area shape="rect" coords="632,260,681,290" href="http://www.isima.fr/anelis" target="_blank" alt="ANELIS"
  ONMOUSEOVER="echangerub('anelisrub')"
  ONMOUSEOUT="echangerub('entrezrub')">
        <area shape="rect" coords="652,206,693,245" href="http://www.isima.fr/bde/index.htm" target="_blank" alt="Le BDE"
  ONMOUSEOVER="echangerub('bderub')"
  ONMOUSEOUT="echangerub('entrezrub')">
        <area shape="rect" coords="30,207,71,244" href="./isima/recherche.html" target="_self" alt="RECHERCHE"
  ONMOUSEOVER="echangerub('rechercherub')"
  ONMOUSEOUT="echangerub('entrezrub')">
        <area shape="rect" coords="26,165,62,197" href="./isima/admission.html" target="_self" alt="ADMISSION"
  ONMOUSEOVER="echangerub('admissionrub')"
  ONMOUSEOUT="echangerub('entrezrub')">
        <area shape="rect" coords="176,43,548,300" href="./isima/accueil.html" target="_self" alt="ACCUEIL">
        <area shape="rect" coords="25,125,66,154" href="./isima/ens.html" target="_self" alt="ENSEIGNEMENT"
  ONMOUSEOVER="echangerub('enseignementrub')"
  ONMOUSEOUT="echangerub('entrezrub')">
      </map></td>
  </tr>
  <tr> 
    <td colspan="3" height="18"> 
      <div align="right"></div>
      </td>
  </tr>
  <tr> 
    <td align="left" colspan="2" width="70%">
      <div align="left"><font face="Courier New, Courier, mono" color="#CCFFCC"><b><font color="#CCCCFF">[</font></b></font><font color="#FFFFFF" size="3">&nbsp;</font><font color="#FFFFFF" size="3" face="Times New Roman, Times, serif"><b><a href="mailto:mrue@isima.fr">Webmaster</a></b></font><font color="#FFFFFF" size="3"><b> 
        </b></font><font face="Courier New, Courier, mono" color="#CCFFCC"><b><font color="#CCCCFF">]</font></b></font><font color="#FFFFFF" size="3" face="Times New Roman, Times, serif"><b> 
        </b></font></div>
    </td>
    <td align="right" width="30%">
      <div align="right"><font color="#FFFFFF" size="2" face="Times New Roman, Times, serif"><i>Derni&egrave;re 
        mise &agrave; jour le : 2 juin 2003.</i></font></div>
    </td>
  </tr>
  <tr> </tr>
</table>

</body>

</html>

