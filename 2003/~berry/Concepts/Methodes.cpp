// M�thodes.cpp
// D�claration d'autres m�thodes

#include <vector>
#include <list>
#include <fstream>
#include "Matrice.h"

using namespace std;

typedef vector<int> VecInt;

// Surcharge de << pour l'affichage des VecInt :
ostream& operator<<(ostream& o,const VecInt& vect){
  if (vect.size()!=0)
    for (int i=0;i<(int)vect.size();i++) o<<vect[i]<<" ";
  else o<<" ";
  return o;
}

// M�thode indiquant si l'entier i appartient au vecteur V :
bool appartient(const VecInt& V,const int i){
  for (int j=0;j<(int)V.size();j++)
    if (V[j]==i) return 1;
  return 0;
}

// M�thode disant si le vecteur V1 est inclus dans le vecteur V2 :
bool inclus(const VecInt& V1,const VecInt& V2){
  int k=1;
  for (int j=0;j<(int)V1.size();j++)
   if (V1[j]!=0 && appartient(V2,V1[j])==0) k=0;
  if (k==1) return 1;
  return 0;
}

// M�thode ajoutant deux vecteurs :
VecInt sommeVecInt(const VecInt& _V1,const VecInt& _V2){
  VecInt temp;
  if (_V1.size()!=0 && _V2.size()!=0){
    for (int i=0;i<(int)_V1.size();i++) temp.push_back(_V1[i]);
    for (int i=0;i<(int)_V2.size();i++)
      if (appartient(temp,_V2[i])==0) temp.push_back(_V2[i]);
  }
  else{
    if (_V1.size()==0) temp=_V2;
    else temp=_V1;
  }
  return temp;
}

// M�thode triant un vecteur d"entiers :
void triVecInt(VecInt& V){
  list<int> temp(V.size());
  list<int>::iterator it;
  int i=0;
  for (it=temp.begin();it!=temp.end();it++){
    *it=V[i];
    i++;
  }
  temp.sort();
  i=0;
  for (it=temp.begin();it!=temp.end();it++){
    V[i]=*it;
    i++;
  }
}

// M�thode lisant le nombre d'objets, le nombre de propri�t�s
// et la matrice R0 dans le fichier d'entr�e :
void entree_fichier(char fichier_entree[20],int& nbo,int& nbp,Matrice<int>& R0){
  ifstream entree(fichier_entree);
  if (!entree){
    cout<<"Impossible de lire le fichier !"<<endl;
    cout<<endl;
    system("pause");
    system("cls");
    exit(1);
  }
  cout<<endl;
  entree>>nbo;
  entree>>nbp;
  nbo+=1;nbp+=1;
  R0.setTaille(nbo,nbp);
  int k;
  for (int i=0;i<nbp;i++) R0.setElement(0,i,i);
  for (int i=1;i<nbo;i++){
    R0.setElement(i,0,i);
    for (int j=1;j<nbp;j++){
      entree>>k;
      R0.setElement(i,j,k);
    }
  }
  entree.close();
}


// M�thode affichant les concepts et les r�gles :
void affichage(Matrice<VecInt>& CONCEPTS,Matrice<VecInt>& REGLES){
  if (CONCEPTS.getNbLigne()==0) cout<<"Aucun concept genere"<<endl;
  else{
	cout<<"Voici les "<<CONCEPTS.getNbLigne()<<" concepts :"<<endl;
    for(int i=0;i<CONCEPTS.getNbLigne();i++){
	  VecInt V1,V2;
	  V1=CONCEPTS.getElement(i,0);triVecInt(V1);
	  V2=CONCEPTS.getElement(i,1);triVecInt(V2);
	  for (int j=0;j<(int)V1.size();j++)
	    cout<<char(96+V1[j]);
	  cout<<" * ";
	  for (int j=0;j<(int)V2.size();j++)
	    cout<<V2[j];
	  cout<<endl;
    }
  cout<<endl;
  }
  if (REGLES.getNbLigne()==0) cout<<"Aucune regle generee"<<endl;
  else{
	cout<<"Voici les "<<REGLES.getNbLigne()<<" regles :"<<endl;
    for(int i=0;i<REGLES.getNbLigne();i++){
	  VecInt V1,V2;
	  V1=REGLES.getElement(i,0);triVecInt(V1);
	  V2=REGLES.getElement(i,1);triVecInt(V2);
	  for (int j=0;j<(int)V1.size();j++)
	    cout<<char(96+V1[j]);
	  cout<<" -----> ";
	  for (int j=0;j<(int)V2.size();j++)
	    cout<<char(96+V2[j]);
	  cout<<endl;
    }
  cout<<endl;
  }
}

// M�thode �crivant les concepts et les r�gles dans le fichier de sortie :
void sortie_fichier(char fichier_sortie[20],Matrice<VecInt>& CONCEPTS,Matrice<VecInt>& REGLES){
  ofstream sortie(fichier_sortie);
  if (!sortie){
    cout<<"Impossible de cr�er le fichier !"<<endl;
    cout<<endl;
    system("pause");
    system("cls");
    exit(1);
  }
  if (CONCEPTS.getNbLigne()==0) sortie<<"Aucun concept genere"<<endl;
  else{
	sortie<<"Voici les "<<CONCEPTS.getNbLigne()<<" concepts :"<<endl;
    for(int i=0;i<CONCEPTS.getNbLigne();i++){
	  VecInt V1,V2;
	  V1=CONCEPTS.getElement(i,0);triVecInt(V1);
	  V2=CONCEPTS.getElement(i,1);triVecInt(V2);
	  for (int j=0;j<(int)V1.size();j++)
	    sortie<<char(96+V1[j]);
	  sortie<<" * ";
	  for (int j=0;j<(int)V2.size();j++)
	    sortie<<V2[j];
	  sortie<<endl;
    }
  sortie<<endl;
  }
  if (REGLES.getNbLigne()==0) sortie<<"Aucune regle generee"<<endl;
  else{
	sortie<<"Voici les "<<REGLES.getNbLigne()<<" regles :"<<endl;
    for(int i=0;i<REGLES.getNbLigne();i++){
	  VecInt V1,V2;
	  V1=REGLES.getElement(i,0);triVecInt(V1);
	  V2=REGLES.getElement(i,1);triVecInt(V2);
	  for (int j=0;j<(int)V1.size();j++)
	    sortie<<char(96+V1[j]);
	  sortie<<" -----> ";
	  for (int j=0;j<(int)V2.size();j++)
	    sortie<<char(96+V2[j]);
	  sortie<<endl;
    }
  sortie<<endl;
  }
  sortie.close();
  cout<<endl;
}

