// Main.cpp
// Programme principal

#include "Concept.h"

using namespace std;

int main(){
  char fichier_entree[20];
  char fichier_sortie[20];
  int nbo,nbp;
  MatInt R0;

  cout<<" -------------------------------------------------"<<endl;
  cout<<"Generation des concepts et de la base de regles associee"<<endl;
  cout<<" -------------------------------------------------"<<endl;
  cout<<endl;

  cout<<"Entrez le nom du fichier d'entree : ";
  cin>>fichier_entree;
  entree_fichier(fichier_entree,nbo,nbp,R0);

// Construction de T
  MatInt T(nbp,nbp);
  for (int i=0;i<nbp;i++){
    T.setElement(0,i,R0.getElement(0,i));
    T.setElement(i,0,R0.getElement(0,i));
  }
  for (int i=1;i<nbp;i++)
    for (int j=1;j<nbp;j++){
  	   T.setElement(j,i,0);
	   if (j!=i)
         for (int k=1;k<nbo;k++)
           if (R0.getElement(k,i)>R0.getElement(k,j)) T.setElement(j,i,T.getElement(j,i)+1);
    }

// Construction de D
  MatInt D(2,nbp-1);
  for (int i=0;i<nbp;i++){
    D.setElement(0,i,T.getElement(0,i+1));
  }
  for (int i=0;i<nbp-1;i++){
    int k=0;
    for (int j=0;j<nbp-1;j++)
      if (T.getElement(j+1,i+1)==0) k++ ;
    D.setElement(1,i,k);
  }

// Construction de l'�l�ment bottom
  VecInt V1,V2;
  for (int i=1;i<nbo;i++) V2.push_back(i);
  Concept bottom(V1,V2);

// Cr�ation de la matrice CONCEPTS contenant l'ensemble des concepts
  MatVecInt CONCEPTS(0,2);

// Cr�ation de la matrice REGLES contenant l'ensemble des r�gles
  MatVecInt REGLES(0,2);

// Lancement de l'algo r�cursif CONCEPTS pour l'�l�ment bottom
  bottom.CONCEPTS(R0,T,D,nbp,nbo,CONCEPTS,REGLES);

// Affichage des r�sultats
  char rep1;
  do{
    cout<<"Voulez-vous afficher les resultats ? (o/n) : ";
    cin>>rep1;
    cout<<endl;
  }while (rep1!='o' && rep1!='O' && rep1!='n' && rep1!='N');
  if(rep1=='o' || rep1=='O'){
    system("cls");
   	affichage(CONCEPTS,REGLES);
    system("pause");
    system("cls");
  }

// Sauvegarde des r�sultats
  char rep2;
  do{
    cout<<"Voulez-vous sauver les resultats ? (o/n) : ";
    cin>>rep2;
    cout<<endl;
  }while (rep2!='o' && rep2!='O' && rep2!='n' && rep2!='N');
  if(rep2=='o' || rep2=='O'){
    cout<<"Entrez le nom du fichier de sortie : ";
    cin>>fichier_sortie;
    sortie_fichier(fichier_sortie,CONCEPTS,REGLES);
  }

  system("pause");
  system("cls");

  return 0;
}
