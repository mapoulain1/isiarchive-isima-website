<HTML>

  <HEAD>

    <META http-equiv="Content-Type" content="text/html">

    <META name="description" content="">

    <META name="keywords" content="">
    <LINK REL="stylesheet" HREF="style.css" TYPE="text/css"> 


    <TITLE>ISIMA Curriculum</TITLE>

  </HEAD>



  <BODY text="#000080" bgcolor="#FFFFFF">

    <A name="top"></A>

    <TABLE width="100%">

      <TR>

        <TD valign="CENTER">

          <H2>ISIMA Curriculum</H2></TD>

        <TD valign="CENTER" align="RIGHT">

          <IMG src="images/clermont.jpg"></TD></TR>

    </TABLE>



    <HR><CENTER>



    <TABLE border="0" cellspacing="0" cellpadding="5" bgcolor="#FFFFFF" width="98%">

    <TR><TD>



    <FONT size="2">



    <H3>Engineering Degree</H3>

    

    <P align="JUSTIFY">Three years of higher education spent at ISIMA lead to a nationally recognized Engineering 

       Degree in Computer Science and Modelling (French "Dipl�me d'Ing�nieur"). The curriculum 

       leading to this degree includes a balance of scientific and technical studies, as well as 

       humanities such as foreign languages, law, economics and management.</P>



    <P align="JUSTIFY">ISIMA students are not only taught the different subjects of their engineering curriculum, 

       but also  encouraged to learn on their own, so that they have the learning abilities 

       necessary to succeed in a rapidly changing world.</P><BR>





    <H3>First year&nbsp;: the fundamentals of Computer Science Engineering</H3>



    <P align="JUSTIFY">Dr. Christine Force, <A href="mailto:christine.force@isima.fr">

       christine.force@isima.fr</A></P>



    <P align="JUSTIFY">

       <UL>

         <LI>Applied Mathematics and Numerical Algorithm&nbsp;: 180 h</LI>

         <LI>Operations Research and Statistics&nbsp;: 120 h</LI>

         <LI>Software Engineering,  Programming Languages&nbsp;: 220 h</LI>

         <LI>Computer and Circuit Architectures; Systems&nbsp;: 110 h</LI>

         <LI>Solid Physics, Automation, Signal Processing&nbsp;: 120 h</LI>

         <LI>Communication Skills, Foreign Languages&nbsp;: 130 h</LI>

         <LI>Economic and Industrial Systems&nbsp;: 50 h</LI>

         <LI>Individual student project (or assignment)</LI>

       </UL><p></P><BR>





    <H3>Second and third years</H3>



    <P align="JUSTIFY">From the second year onwards, ISIMA students follow a double curriculum.</P>



    <P align="JUSTIFY">On the one hand, their course of study includes a Computer Science common core, as well 

       as subjects in the humanities and social sciences&nbsp;: students are thus able to acquire sound 

       Engineering Fundamentals, but are also encouraged to include the needs and requirements of 

       society as part of their engineering projects.</P><BR>





    <H3>Common subjects for 2nd and 3rd year students</H3>

    

    <P align="JUSTIFY">

       <UL>

         <LI>Mathematics and Algorithms&nbsp;: 80h</LI>

         <LI>Modelling&nbsp;: 50 h</LI>

         <LI>Software Engineering&nbsp;: 50 h</LI>

         <LI>Automated Systems&nbsp;: 50 h</LI>

         <LI>Communication Skills, Foreign Languages&nbsp;: 150 h</LI>

         <LI>Law, Management, Project Management&nbsp;: 110 h</LI>

         <LI>Individual second year project (or assignment)&nbsp;: 100 h</LI>

         <LI>Individual third year project (or assignment)&nbsp;: 200 h</LI>

       </UL><p></P>



    <P align="JUSTIFY">On the other hand, the ISIMA curriculum requires students to specialize in any one of four 

       fields, for which they enroll in different courses of study as follows.</P><BR>





    <H3>Industrial placement periods or internships</H3>



        <P align="JUSTIFY">Anne Jobert, <A href="mailto:anne.jobert@isima.fr">anne.jobert@isima.fr</A></P>



    <P align="JUSTIFY">In addition, two industrial placement periods or internships are required, one at the end 

       of the second year (5 to 6 months, starting in April) and the other in the third year (3 

       to 5 months from April through August).They are equivalent to a placement in industry in 

       Great Britain, or to an American internship or co-optation program. They take place&nbsp;:

       <UL>

         <LI>Between the second and the third year (from April through September).</LI>

         <LI>At the end of the third year (from April to June, with a possibility of extending 

             the placement on request).</LI>

       </UL><p></P>



    <P align="JUSTIFY">Students work on a specific project defined by the host company or research laboratory. 

       This work is supervised by one of the ISIMA faculty members. Students are assessed on the 

       basis of successful achievement of the initial objectives, and on the quality of the 

       written report and oral defence.</P>



    <P align="JUSTIFY">Our policy is to encourage ISIMA students to do their industrial placement period or 

       internship abroad; international exchange programs have been set up worldwide.</P>



    <P align="RIGHT">

       <A href="#top"><IMG src="../images/top.gif" border="0"></A></P>



    </FONT>

    </TABLE>



    </CENTER>

  </BODY>

</HTML>

