#!/usr/local/bin/perl
# Takes a list of files as standard in (piped from ls maybe)
# outputs HTML list of IMG SRC tags for the list...

$bool = 0;

open (Liste,"ls |") || die "Error reading directory\n";

$date = `date`;
chop($date);

printf("Content-type: text/html\n\n\r");
printf("<HTML>\n");
printf("<HEAD>\n");
printf("<TITLE>\n");
printf("ISIMA : Banque d'icones (dots)\n");
printf("</TITLE>\n");
printf("</HEAD>\n");
printf("<BODY>\n");
printf("<EM>Last Updated : $date</EM>\n");
printf("<CENTER>\n");
printf("<IMG SRC=dots.gif>\n");
printf("<HR SIZE=5>\n");
printf("<TABLE CELLSPACING=10>\n");

while (<Liste>) {
   unless (/dots|Welcome/)  {
        chop($_);
        if (!$bool) { print "<TR ALIGN=CENTER>"; }
        print "<TD><IMG SRC=\"$_\">";
        print "</TD><TD>";
        print "<A HREF=$_>$_</A>";
        print "</TD>";
        if ($bool) { print "</TR>\n"; }
        $bool = !$bool;
   }
}
printf("</TABLE>\n");
printf("<HR SIZE=5>\n");
printf("<H2>Index construit &agrave; la demande par un script en Perl</H2>\n");
printf("</BODY>\n");
printf("</HTML>\n");
close(Liste);
