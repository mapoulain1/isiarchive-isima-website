#!/usr/local/bin/perl
# Takes a list of files as standard in (piped from ls maybe)
# outputs HTML list of IMG SRC tags for the list...

open (Liste,"ls |") || die "Error reading directory\n";

$date = `date`;
chop($date);

printf("Content-type: text/html\n\n\r");
printf("<HTML>\n");
printf("<HEAD>\n");
printf("<TITLE>\n");
printf("ISIMA : Logos\n");
printf("</TITLE>\n");
printf("</HEAD>\n");
printf("<BODY>\n");
printf("<EM>Last Updated : $date</EM>\n");
printf("<CENTER>\n");
printf("<HR SIZE=5>\n");

while (<Liste>) {
   unless (/logos|Welcome/)  {
        chop($_);
        print "<IMG SRC=\"$_\"><BR>";
        print "<A HREF=$_>$_</A><P>";
   }
}
printf("</TABLE>\n");
printf("<HR SIZE=5>\n");
printf("<H2>Index construit &agrave; la demande par un script en Perl</H2>\n");
printf("</BODY>\n");
printf("</HTML>\n");
close(Liste);
