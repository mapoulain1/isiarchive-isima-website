/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  Graph.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import java.awt.*;
import java.applet.*;

public class Graph extends Applet{

 public atomic1 GetAtomic(){
  return atomic_;
 }

 public static Point AbsLocation(Component c){
  /* Call this to get the absolute (screen) location
   * of the component c. This function has no relation
   * to this class other than that it is called by another member function.
  */
  int x = 0, y = 0;
  do{
	Point p = c.location();
    x += p.x;
	y += p.y;
  }
  while((c = c.getParent()) != null);
   return new Point(x, y);
 }

 atomicDrawPanel atomicDrawPanel_;
 atomic1 atomic_;


 public Graph(atomic1 atomic__, atomicDrawPanel atomicDrawPanel__){
  /*
  I am a panel that should be added to a parent atomicDrawPanel.  I contain a GraphPanel
  (graphPanel) and a field for the name of the model attached to my parent (nameField).
  Call this to create me and let me know who my parent panel is (atomicDrawPanel__) and
  who my attached model is (atomic__).
  */

  atomic_ = atomic__;
  atomicDrawPanel_ = atomicDrawPanel__;
 }

 public void setMyAtomic(atomic1 a){ //bpz added
  atomic_ = a;
 }

 public void repaint(){
  // Call this to get my child GraphPanel repainted.
  nameField.setText(atomic_.get_name()); //bpz added
  graphPanel.repaint();
 }

 public void init(){
  // Call this to set me up onscreen. This layout was created using the resource editor.
  //{{INIT_CONTROLS
   setLayout(new BorderLayout());
  //addNotify();
  resize(insets().left + insets().right + 253, insets().top + insets().bottom + 245);
  graphPanel=new GraphPanel(this);
  add(graphPanel);
  graphPanel.reshape(insets().left + 7,insets().top + 20,238,143);
  label1=new Label("Name");
  add(label1);
  label1.reshape(insets().left + 0,insets().top + 7,49,13);
  nameField=new TextField(10);
  add(nameField);
  nameField.reshape(insets().left + 56,insets().top + 0,84,20);
  //}}
  //nameField.setText(atomic_.get_name());//bpz
  show();
 }

 private devs mySource;
 private content myContent;

 public synchronized void SignalOutput(Graph source,devs d,content c){
  // Call this to make me create an OutputBox that will travel from me
  // to the GraphPanel of the source model (source).
  mySource = d;
  myContent = c;
  Point end = AbsLocation(atomicDrawPanel_);
  end.x += graphPanel.size().width / 2;
  end.y += graphPanel.size().height / 4;
  Point start = new Point(end.x, end.y);
  if(source != null){
	start = AbsLocation(source.atomicDrawPanel_);
	start.x += source.graphPanel.size().width / 2;
	start.y += source.graphPanel.size().height / 4;
  }
  OutputBox o = new OutputBox(start, end,mySource,myContent);
  o.start();
 }

 public void SetPhaseElapsed(int P,int Q, String phaseName){
  // Call this to set the values to be plotted on my child GraphPanel.
  if(atomic_ != null && phaseName != null)
	graphPanel.SetPhaseElapsed(P, Q, phaseName);
  else graphPanel.SetPhaseElapsed(P, Q, "");
 }
 //bpz for atomDigraph

 public void SetPhaseElapsed(int P,int Q, String phaseName,int Control){
  // Call this to set the values to be plotted on my child GraphPanel.
  if(atomic_ != null && phaseName != null)
	graphPanel.SetPhaseElapsed(P, Q, phaseName,Control);
  else graphPanel.SetPhaseElapsed(P, Q, "",Control);
 }

 //{{DECLARE_CONTROLS
 GraphPanel graphPanel;
 Label label1;
 TextField nameField;
 //}}
}

