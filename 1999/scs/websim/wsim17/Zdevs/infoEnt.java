/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : infoEnt.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.io.*;
import Zdevs.Zcontainer.*;


public class infoEnt extends  entity{
  protected Panel myPanel;
  protected Button bOK;




public infoEnt(){
  super();
  init();
}

public infoEnt(String name){
  super(name);
  init();
}

protected void init(){
  myPanel = new Panel();
  myPanel.setLayout(new FlowLayout());
  bOK= new Button("OK");
  myPanel.add(bOK);
  myPanel.validate();
}

public Panel getPanel(){
  return myPanel;
}

public void getInfo(){
//String s = tfe.getText();
//System.out.println(s);
}

public void refresh(){
  myPanel.repaint();
}

}


