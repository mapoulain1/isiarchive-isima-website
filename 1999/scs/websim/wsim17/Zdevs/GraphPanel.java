/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  GraphPanel.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import java.awt.*;
import java.applet.*;

class GraphPanel extends Panel{
 long time;
 long oldTime;
 Graph graph;

 public void update(Graphics g){
  // This displays my graphs. Call my repaint() to get this function to fire.
  // if we haven't yet setup our double buffering image
  if(image == null){
    // create the image
	image = createImage(this.size().width, this.size().height);
	imageG = image.getGraphics();
    // draw the box around where the graph will go
	imageG.setColor(Color.black);
	imageG.drawRect(0, imageG.getFontMetrics().getMaxAscent() + 1, xSize - 1,
	                ySize - imageG.getFontMetrics().getMaxAscent() - 2);
    // draw the graph axes
	phaseGraphY = elapsedGraphY - maxElapsed - 20;
	imageG.setColor(Color.black);
	imageG.drawLine(graphX, phaseGraphY, graphX, phaseGraphY - maxPhase);
	imageG.drawLine(graphX, phaseGraphY, graphX + timeRange, phaseGraphY);
	imageG.drawLine(graphX, elapsedGraphY, graphX, elapsedGraphY - maxElapsed);
	imageG.drawLine(graphX, elapsedGraphY, graphX + timeRange, elapsedGraphY);
    // label the axes
	imageG.setColor(Color.black);
	imageG.drawString("t", graphX + timeRange + 5, phaseGraphY + 5);
	imageG.drawString("t", graphX + timeRange + 5, elapsedGraphY + 5);
	imageG.drawString("phase", graphX - 15, phaseGraphY - maxPhase - 5);
	imageG.drawString("elapsed", graphX - 15, elapsedGraphY - maxElapsed - 5);
  }

  //clear the graph area if the graph has reached the far right edge
  time++;
  if(oldTime % timeRange > time % timeRange) Clear(imageG);
	oldTime = time;
  // draw graph line for quantity
  //x = graphX + (int)(time % timeRange) + 1;
  int xx = (int)(time % timeRange);
  x = xx*control + graphX + 1;
  if(elapsed > 0){
	imageG.setColor(Color.green);
	imageG.drawLine(x, elapsedGraphY - 1, x, elapsedGraphY - Math.min(elapsed, maxElapsed) - 1);
  }
  // draw graph line for phase
  imageG.setColor(Color.green);
  y = phaseGraphY - (Math.min(phase * phaseScale, maxPhase)) - 1;
  imageG.drawLine(x, y, x, y);
  // transfer the buffer image to the window
  g.drawImage(image, 0, 0, this);
  // draw the name of the phase
  g.setColor(Color.green);
  g.drawString(phaseName, x - 20, y - 5);
  g.setPaintMode();
 }

 Graphics imageG;
 Image image;

 public void paint(Graphics g){
  //This is called by the system.
  update(g);
 }

 int y;
 int x;
 String phaseName;

 public GraphPanel(Graph graph_){
  /*
  I am a panel that should be added to a parent Graph panel (graph_).
  I display graphs of the current values of my phase and quantity
  variables (both int's).
  */
  image = null;
  phaseName = "";
  graph = graph_;
 }

 static int maxPhase = 15;
 static int phaseScale = 1;
 static int maxElapsed = 30;
 int phase;
 int elapsed;
 static int control = 1;

 void SetPhaseElapsed(int P,int E, String phaseName_){
  phase = P;
  elapsed = E;
  phaseName = phaseName_;
 }

 void SetPhaseElapsed(int P,int E, String phaseName_,int Control){
  phase = P;
  elapsed = E;
  phaseName = phaseName_;
  control = 0;
 }

 static int ySize = 120;
 static int xSize = 230;
 static int elapsedGraphY = 100;
 int phaseGraphY;
 static int graphX = 25;
 static int timeRange = 180;

 private void Clear(Graphics g){
  g.setColor(getBackground());
  g.fillRect(graphX + 1, phaseGraphY - maxPhase - 1, timeRange + 1, maxPhase + 1);
  g.fillRect(graphX + 1, elapsedGraphY - maxElapsed - 1, timeRange + 1, maxElapsed + 1);
 }
}

