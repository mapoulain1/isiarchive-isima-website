/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : addrclass.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;

public class addrclass extends entity {
 int i,j = 0;
 final static String classname = "addrclass";

 public String get_classname(){
  return classname;
 }

 public addrclass(){
  super();
  // classname = entity::make_name("addrclass");
 }

 public addrclass(int I,int J){
  //  classname = entity::make_name("addrclass");
  i=I;
  j=J;
 }

 public void set_ent(int I,int J){
  i=I;
  j=J;
 }

 public boolean equal(entity ent){
  return (get_classname().equals( ent.get_classname())
            &&((addrclass)ent).i == i
            && ((addrclass)ent).j == j);
 }

 public void print(){
  System.out.println("(addr "+ i + "," + j + ")" );
 }

 public int get_i(){
  return i;
 }

 public int get_j(){
  return j;
 }
}