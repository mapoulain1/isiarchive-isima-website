/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  atomDiagraph.java
 *  Version    :  1.0
 *  Date       :  12-4-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;

public class atomDigraph extends atomic{
 protected digraph d;

 public atomDigraph(digraph D){
 super(D.get_name());
 d = D;
 //initialize();
 }

 public int ta(){
  return d.next_tN()- tL;
 }

 public void initialize(){
  d.initialize();
  super.initialize();
 // tL = 0;
 // tN = ta();
 }

 public void inject(String p,entity val){
  inject(p,val,0);
 }

 public void inject(String p,entity val,int e){
  message m = new message();
  m.add(make_content(p,val));
  d.wrap_deltext(e,m);
 }

 public void  deltext(int e,message x){
  d.wrap_deltfunc(tL+e,x); //notethe difference in time
 }

 public void deltint(){
  int dtN = d.next_tN();
  d.compute_input_output(dtN);
  d.tellall_wrap_deltfunc(dtN,d.get_input());
  d.set_tL (dtN);
 }

 public message out(){
  d.compute_input_output(d.next_tN());
  message m = new message();
  for(entity p = d.get_output().get_head();p != null;p = p.get_right()){
     entity ent = p.get_ent();
     content c = (content)ent;
     c.source = this;
     m.add(c);
  }
  return m;
 }

 public void myRepaint(){
  newList();
  addString("tL: " + tL);
  addString("tN: " + tN);
  String totalPhase = "";
  for(entity p = d.get_atomic_components().get_head();
             p != null;p = p.get_right()){
     entity ent = p.get_ent();
     atomic a = (atomic)ent;
     addString( a.get_name() + ":" + a.getPhase());
     totalPhase  = totalPhase + " " + a.get_name() + ":" + a.getPhase();
  }
  if ( myAtomicDrawPanel != null){
   myAtomicDrawPanel.graph.SetPhaseElapsed(5,e/100,totalPhase,0);
   myAtomicDrawPanel.repaint();
   tf.setText( " tL = " + tL + " tN = " + tN +" e =  " + e);
  }
  else tf.setText(totalPhase  + " tL = " + tL + " tN = " + tN +" e =  " + e );
 }

}

