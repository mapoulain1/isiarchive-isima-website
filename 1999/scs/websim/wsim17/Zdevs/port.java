/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  port.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;

public class port extends entity{

 public port(String n){
  super(n);
 }
}