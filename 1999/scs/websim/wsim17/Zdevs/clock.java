/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  clock.java
 *  Version    :  1.0
 *  Date       :  12-5-96
 */
package Zdevs;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.io.*;
import Zdevs.Zcontainer.*;

public class clock extends entity{
 protected    int time, stoptime;
 protected TextField myTextField;


 int get_stoptime(){
   return stoptime;
 }

 TextField get_tf(){
  return myTextField;
 }

 public clock(int Start){
  super();
  time = Start;
  myTextField = new TextField(20);
  myTextField.setText("clock " + time);
 }

 public void reset(){
  time = 0;
  myTextField.setText("clock " + time);
 }

 public void clockRun(int Stop){
  stoptime = Stop;
  resume();
 }

 public void run(){
  while(true){;
       while(time < stoptime){
            sleep(100);
            time = time + 100;
            myTextField.setText("clock " + time);
       }
       stop();
  }
 }
}
