/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : coord1.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.lang.Integer;
import Zdevs.Zcontainer.*;
import java.io.*;
import java.awt.*;



public class coord1 extends entity{
 protected int count; //,clock;
 protected clock cl;
 protected int i,min,tL,tN, numIter;
 protected set c;
 protected digraph d;
 protected boolean first = true;
 protected boolean restart = false;
 protected static Param param;
 protected TextField myTextField;

 public synchronized void decrement(){//why not inherited
   count = count - 1;
   //System.out.println("decrement count " + count);
 }

 public coord1(){ // to fool atomic1
  super();
 }

 public  coord1 (digraph D,clock CL,int n,Param Parm, boolean f){
  super();
  d = D;
  c = d.get_atomic_components();
  cl = CL;
  //System.out.println("ordinary");
  count =  c.get_length();
  numIter = n;
  first = f;
  restart = false;
  param = Parm;
  //if (param == null) param = new Param(d.getButtons());
 }

 public  coord1 (digraph D,clock CL,int n,boolean f, Param Parm,boolean r){
  super();
  d = D;
  c = d.get_atomic_components();
  cl = CL;
  //System.out.println("restart");
  count =  c.get_length();
  numIter = n;
  first = true;
  restart = true;
  //param = Parm;
  //if (param == null) param = new Param(d.getButtons());
 }

 public void stopAtNextIter(){
  //tellAllt(-1);
  numIter = i+1;
 }

 public void waitAllDone(){
  reset_count();
  //System.out.println("Start Wait" + count);
  while (count!= 0){  //needed to delay results until ensemble completed
  sleep(100);
  }
 }

 public void reset_count(){
  count = c.get_length();
 }

 public void tellAllme(){
  //System.out.print(" tellAllme");
  for(entity p = c.get_head();p != null;p = p.get_right()) {
     entity ent = p.get_ent();
     atomic1 a = (atomic1)ent;
     a.setCoord(this);
  }
 }

 public void tellAllStep(){
  //System.out.println("all step ");
  for(entity p = c.get_head();p != null;p = p.get_right()) {
     entity ent = p.get_ent();
     atomic1 a = (atomic1)ent;
     a.step();
  }
 }

 public void tellAllStartup(){
  //System.out.println("all start ");
  for(entity p = c.get_head();p != null;p = p.get_right()) {
     entity ent = p.get_ent();
     atomic1 a = (atomic1)ent;
     a.start(); // start them up
  }
 }

 public void tellAllResume(){
   //System.out.println("all start ");
  for(entity p = c.get_head();p != null;p = p.get_right()) {
     entity ent = p.get_ent();
     atomic1 a = (atomic1)ent;
     a.resume();
  }
 }

 public void askAlltN(){
  //System.out.print("askAlltN ");
  min =100000000; //INFINITY;
  //System.out.print("askAlltN ");

  for(entity p = c.get_head();p != null;p = p.get_right()) {
      entity ent = p.get_ent();
      atomic1 a = (atomic1)ent;
      if (a.next_tN() < min) min = a.next_tN();
  }

   for(entity p = c.get_head();p != null;p = p.get_right()) {
       entity ent = p.get_ent();
       atomic1 a = (atomic1)ent;
       a.setMin(min);
   }

   tN = min;
 }

 void tellAllt(int t){
  for(entity p = c.get_head();p != null;p = p.get_right()) {
     entity ent = p.get_ent();
     atomic1 a = (atomic1)ent;
     a.setMin(t );
  }
 }

 public void tellAllSetP(String s){
   //System.out.println("all start ");
   for(entity p = c.get_head();p != null;p = p.get_right()) {
       entity ent = p.get_ent();
       atomic1 a = (atomic1)ent;
       a.setP(s);
  }
 }

 private void setParm(){
  // if (param == null) param = new Param(d.getButtons());
  if(param != null){
    String s = param.getS();
    tellAllSetP(s);
  }
 }

 public void run(){
  devs dummy = new devs("");
  int INF = dummy.INFINITY;
  //System.out.println("NEW COORD STARTED ");
  setParm();
  for(i = 0;i < numIter;i++){
     System.out.println("ITERATION " + i);
     askAlltN();
     if (tN >= INF){
    cl.clockRun(INF);
    break;
  }
  // System.out.println("min " +min);
  tellAllme();
  d.compute_input_output(min);
  d.threadTellall_wrap_deltfunc(min,d.get_input());
  tL = tN;
  // d.show_state();
  //  show_tL();
  //System.out.println("first1 = " + first);

  if(first){
    first = false;
    tL = 0;
    if(!restart){
      cl.start();
      tellAllStartup();
    }
    else{
      restart = false;
      cl.reset();
      tellAllStep();
    }
   //System.out.println("first2 = " + first);
  }
  else  tellAllStep();
  cl.clockRun(min);
  waitAllDone();
  }
 }
}
