package Zdevs;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.io.*;
import Zdevs.Zcontainer.*;

public class devsProd extends devsDrawPanel{


    public devsProd(){
        super();
    }


    public devsProd(devs d){
        super();
    }


protected  void addCompPanels(){

    myCoupled = dig.get_atomic_components();
   System.out.println("Components: ");
    myCoupled.print_all();


 for ( entity p = myCoupled.get_head();p != null;p = p.get_right()) {

       entity pp = p.get_ent();
        atomic1 A = (atomic1)pp;

       atomicDrawPanel ap = new atomicDrawPanel(A,false,true,this);

        add(ap);
 }
dig.initialize(); //need to initialize again to get rand instance right
}

 public boolean action(Event evt, Object obj) {
	if(evt.target instanceof Button)
		{
		 if("Enter Digraph Class".equals(obj))
	      {
          getClassInstance();
          resize(750, 500);
		setLayout(new GridLayout(2, 3));//better than 3,2
        addButtons();
        add(globalPanel);
        addCompPanels();
        validate();
          show();

          }
  if("Help".equals(obj))
       {
           getHelp();
        }
        if("Quit".equals(obj))
        {
            tellAllKill();
        }
 if("Inject External".equals(obj))
                {
           queryExternal();
                }

	     if("Set Params".equals(obj))
             {
    if (param == null) param = new Param(dig.getButtons());

  /*
     System.out.println("Stop All ");
     tellAllStop();
     cl.stop();


    for (int i = 0; i< countComponents();i++){
              Component c = getComponent(i);
              atomicPanel a = (atomicPanel)c;
              a.stop();
    } //doesn't work
    */
 return true;
                }
         if("Toggle Step Mode".equals(obj))
                {
                 stepMode = !stepMode;
       System.out.println("STEPMODE " + stepMode);
         if  (!stepMode){
       Coord = new coord1(dig,cl,1000,param,first);
        Coord.start();
        }
         else Coord.stopAtNextIter();

                 return true;
                }
  if("Step ".equals(obj))               {

       if  (stepMode){

       Coord = new coord1(dig,cl,1,param,first);

       Coord.start();
       if (first) first = false;

       }
 return true;
                }

    if("Restart".equals(obj))
                {
   int inf = (new devs("a")).INFINITY;
  // System.out.println("inf = " + inf);
     if  (!stepMode ||min >= inf )
            Coord.stopAtNextIter();
       tellAllInitialize();
           tellAllNow();
           cl.stop();
   if (!stepMode) Coord = new coord1(dig,cl,1000,true,param,true);
   else Coord = new coord1(dig,cl,1,true,param,true);
       Coord.start();
   first = false;
 return true;
                   }

}

return false;
}


}

