/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  ports.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import Zdevs.Zcontainer.*;

/* ports replaces set as the class for inports and outports
It is downward compatible. It is able to associate the class
of the value on a port with the port.
*/

public class ports extends function{

 public void add(String s,String t){
   super.add(new entity(s),new entity(t));
 }

 public void add(String s){
  add(s,"entity");
 }

 public boolean is_in_name(String s){
  return key_name_is_in(s);
 }

 public String getClassnm(String p){
  return assoc(p).get_name();
 }
}