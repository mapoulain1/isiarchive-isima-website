/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  queue.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */
package Zdevs.Zcontainer;

public class queue extends list{

 public void  add(entity e){
  insert(e,get_length());
 }

 public void remove(){
  super.remove(0);
 }

 public entity  front(){
  return list_ref(0);
 }
}