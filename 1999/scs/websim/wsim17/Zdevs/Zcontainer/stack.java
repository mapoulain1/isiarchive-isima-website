/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  stack.java
 *  Version    :  1.0
 *  Date       :  12-13-96
 */

package Zdevs.Zcontainer;

public class stack extends list{

 public void  push(entity e){
  insert(e,0);
 }

 void pop(){
  super.remove(0);
 }

 entity top(){
  return list_ref(0);
 }
}