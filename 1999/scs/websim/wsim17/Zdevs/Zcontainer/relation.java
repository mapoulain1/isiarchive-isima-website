package Zdevs.Zcontainer;

/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */
/*                     Filename     : relation.C                     */
/*                     Version      : 1.0                            */
/* = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = */

/**********************************************************************
CLASS RELATION
	A Container Class to Perform Relation Operations.
**********************************************************************/

// relation.java - methods difinition of class relation


// CLASS RELATION
public class relation extends set {
 // remove objects with name NAME from relation
   public void REMOVE(String NAME, entity value)
{
   if (key_name_is_in(NAME)){
    entity e = get_head();
    pair pr = (pair)e;
   super.remove( next_key_name(NAME,pr));
}
}
    public boolean is_in_name(String nm)
    {
        /*
     for (element pr = start; pr != null; pr = ((element)(pr.get_right()))) {
        if (ent.equal( ((pair)pr.get_ent()).get_key()))
 //   if (p.get_ent().eq(NAME))
   if (p.eq(NAME))
         return true;
         */
   return false;

    }

/* private */

static final String classname = "relation";

/* protected */

// find the next key object with the given ent in relation
// from the position pointed by start
protected pair next_key( entity ent, pair start )
{
   if (start != null) {
      for (pair pr = start; pr != null; pr = ((pair)(pr.get_right()))) {
	if (pr.get_key().equal(ent))
	   return (pr);
      }
      return null;
   }
   return null;
}

protected element next_key( entity ent, element start )
{

   if (start != null) {
	//System.out.println("start " +start);
      for (element pr = start; pr != null; pr = ((element)(pr.get_right()))) {
        if (ent.equal( ((pair)pr.get_ent()).get_key()))
		       return (pr);
      }
      return null;
   }
   return null;
}

public boolean key_is_in(entity ent) // is the key in relation?
{
    /*
    entity e = get_head();
    element el = (element)e;
    pair pr = (pair)el;
    element ell = next_key(ent,pr);
    return ell != null;
    */
  return (next_key(ent,(pair )((element)get_head())) != null);
}

protected void addRel(entity el) // this ADD method is copied from container.java
{
   element el1 = new element(el);
   add_at_head(el1);
}

protected void ADDRel(entity el) // this ADD method is copied from container.java
{
   add_at_head(el);
}

/* public: */

public relation()
{
}

public String get_classname()   // get the classification name
{
   return classname;
}

public void add( entity ent, entity value ) // add objects into
{                                                // relation
   pair pr = new pair(ent, value);
   //container::add
   ADDRel(pr);  //addRel(pr);             // for user compatablility
}

public void ADD( entity ent, entity value ) // add objects into
{                                                // relation
   pair pr = new pair(ent, value);
   ADDRel(pr);
}

public void remove( entity ent, entity value ) // remove objects
{                                                   // from relation
   pair pr = new pair(ent, value);

   super.remove(pr);			/* may cause an error */
}

public void remove_all( entity ent ) // remove all objects from
{
    /*
   pair temp;                        // relation with the given ent
   for (pair posn =temp= (pair)get_head();
		temp != null && posn != null;
            temp = posn = next_key(ent, posn) ) {
      posn = (pair)posn.get_right();
      remove(temp.get_key(), temp.get_value());
   }
   */
   //bpz to correct above
   set s = assoc_all(ent);
   for (entity  p = s.get_head(); p!=null;p=p.get_right())
    remove(ent,p.get_ent());
}

public entity assoc( entity ent )  // return entity associate with
{                                       // ent in relation
   pair pr = next_key(ent,(pair )get_head());

   if ( pr != null )
      return (pr.get_value());
   else {
/*
      entity  e = new entity();
      return (e);   // with nothing
*/
	return null;
   }
}

// put all "value" object in the list in a set with given "ent"
public set assoc_all( entity  ent )
{
   pair  posn = (pair )get_head();		/* Should it be a element or entity*/
   //element  posn = (element)get_head();
   set s = new  set();

   while ( true ) {
      posn = next_key(ent, posn);
 // int i = 0; 	/* added for testing */
      if (posn != null) {
         s.add(posn.get_value()); // add every "value" into set
         //s.add(((pair)posn.get_ent()).get_value());
//	System.out.println(posn.get_value().get_name()+" assoc_all count " + i);
//	i++;
	   posn = ((pair)(posn.get_right()));
	//System.out.println("Right " +posn.get_value().get_name());
      } else
         return (s);
   }
}

public set ASSOC_ALL( entity  ent )
{
   pair posn = (pair )get_head();
   set s = new  set();

   while ( true ) {
      posn = next_key(ent, posn);
      if (posn != null) {
         s.ADD(posn.get_value()); // add every "value" into set
         posn = (pair )posn.get_right();
      } else
         return (s);
   }
}


// put all "value" object in the list in a set
public set range_objects()
{
   set s = new  set();

   for ( pair temp = (pair )get_head(); temp != null;
                temp = (pair )temp.get_right() )
      s.add(temp.get_value());

   return (s);
}
// put all "value" object in the list in a set
public set RANGE_OBJECTS()
{
   set s = new  set();

   for ( pair temp = (pair )get_head(); temp != null;
                temp = (pair )temp.get_right() )
      s.ADD(temp.get_value());

   return (s);
}



// put all "value" object's name in the list in a container
public container range_names()
{
   return (range_objects().names());
}


// put all "ent" object in the list in a set
public set domain_objects()
{
   set s = new  set();

   for (pair temp = (pair )get_head(); temp != null;
		 temp = (pair )temp.get_right())
      s.add(temp.get_key());
   return (s);
}

// put all "ent" object in the list in a set
public set DOMAIN_OBJECTS()
{
   set s = new  set();

   for (pair temp = (pair )get_head(); temp != null;
		temp = (pair )temp.get_right())
      s.ADD(temp.get_key());
   return (s);
}


// put all "ent" object's name in the list in a container
public container domain_names()
{
   return (domain_objects().names());
}

// add objects into relation
public void ADD(String NAME, entity value )
{
   entity ent = new entity(NAME);

   pair pr = new pair(ent, value);

  if ( !(is_in_name(pr.get_name())) )  // add if it is not already in
      super.ADD(pr);       // relation

}


// add objects into relation
public void add(String NAME, entity value )
{
   entity ent = new entity(NAME);

   pair pr = new pair(ent, value);

  if ( !(is_in_name(pr.get_name())) )  // add if it is not already in
      ADDRel(pr);       // relation

}

// same as next_key() but now look for the given name NAME
protected pair next_key_name( String NAME, pair start )
{
   if (start != null) {
	{	// a block
	pair pr;
      for (pr = start; pr.get_right() != null &&
               (!(pr.get_key().eq(NAME))); pr = (pair )pr.get_right()) ;
      if (pr.get_key().eq(NAME))
         return (pr);
	}
   }
   return null;
}

// same as name_is_in() but now look for the given name NAME
public boolean key_name_is_in(String NAME)
{
        entity e = get_head();
    element el = (element)e;
    pair pr = (pair)el;
    element ell = next_key_name(NAME,pr);
    return ell != null;

 //  return (next_key_name(NAME,(pair )((element)get_head())) != null);
}

// remove objects with name NAME from relation
public void remove(String NAME, entity value )
{
    entity e = get_head();
    element el = (element)e;
    pair pr = (pair)el;
   super.remove( next_key_name(NAME,pr));
}

// same as assoc_all() with the given name NAME
public set assoc_all(String NAME)
{
   pair posn = (pair )get_head();
   set s = new  set();

   while ( true ) {
      posn = next_key_name(NAME, posn);
      if (posn != null)
      {
         s.add(posn.get_value());
         posn = (pair )posn.get_right();
      } else
         return (s);
   }
}
// same as assoc_all() with the given name NAME
public set ASSOC_ALL(String NAME)
{
   pair posn = (pair )get_head();
   set s = new  set();

   while ( true ) {
      posn = next_key_name(NAME, posn);
      if (posn != null)
      {
         s.ADD(posn.get_value());
         posn = (pair )posn.get_right();
      } else
         return (s);
   }
}

// same as assoc() with the given name NAME
public entity assoc(String NAME)
{
   pair pr = next_key_name(NAME,(pair )get_head());
   if ( pr != null )
      return (pr.get_value());
   else {
//      entity e = new entity();
//      return (e);
      return null;
   }
}
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
