/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  pair.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 *
 *  CLASS PAIR:
 * This class is to perform Pair operations.
 *
 *  - methods definition of class pair
 */

package Zdevs.Zcontainer;

public class pair extends element{  // ver 4.0 inherit from entity

 /* private */

 static final String classname = "pair";
 protected entity key;		//static is not correct
 protected entity value;

/* protected: */

 protected String make_name(String NAME1, String NAME2){
  /*String temp_name = new char [strlen(NAME1) + strlen(NAME2) + 1];
   strcpy(temp_name,NAME1);
   strcat(temp_name,NAME2);
  */
  String temp_name = NAME1 + NAME2;
  return temp_name;
 }

 protected void set_value(entity VALUE){
  value = VALUE;
 }

 /* public: */

 public pair(){
  //super();
  //name = super.make_name("(null)");
  name = make_name("");
 }

 public pair(entity KEY, entity VALUE){
   key = KEY;
   //System.out.println(key.get_name());
   value = VALUE;
  // System.out.println(value.get_name());
  name = make_name(KEY.get_name(), VALUE.get_name());  // bug
  //original is KEY.get_ent().get_name()
 }

 public  String get_classname(){
  return classname;   // returns the classification name of object
 }

 public entity get_value(){
  return value;   // returns the "value" object in container
 }

 public entity get_key(){
  return key;   // returns the "value" object in container
 }

 public boolean equal(entity ENT){  // is ent and ENT equal?
  return(get_classname().compareTo(ENT.get_classname()) == 0 &&
         ((pair )ENT).get_key().equal(get_key())
         && ((pair )ENT).get_value().equal(get_value()) );
 }

 public void print(){
  System.out.println(get_key().get_name() + "," + get_value().get_name());
 }

 public void print_name(){  // print all entity names contain in the list
  System.out.print("(" + this.get_key().get_name() + "," + this.get_value().get_name() + ")  ");
 }

 public String  get_name(){  // get name of object that currently hold
   return name;
 }

 public entity copy(){
  pair p = new pair(get_key(), get_value());
  return (entity)p;
 }
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
