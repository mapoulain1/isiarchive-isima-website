
/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  container.java
 *  Version    :  1.0
 *  Date       :  12-5-96
 *
 * container.java mathods difinition of class container
 * CLASS CONTAINER:
 * 	This class provides simple container methods for other
 *  container classes such as Set, Relation, and Function.
*/

package Zdevs.Zcontainer;

//extern freelist *fl;

public class container extends entity{
 static final String classname = "container";
 /* protected: */
 protected int length;
 //protected element head;  //ver 4.0 is using entity instead
 protected entity head;
 //protected freelist fl;

 protected void set_head(entity el){
  head = el; // set the head entity of the list
 }

 protected void set_length(int len){
  length = len;  // set the length of the list
 }

 protected void increment_length(int inc){
  length = length + inc;   // increment length of list
 }

 protected void decrement_length(int dec){
   length = length - dec;  // decrement length of list
 }

 public void add_at_head(entity el){ // insert entity at the                                  // head of the list
  increment_length(1);
  el.set_right(head);
  set_head(el);
 }

 public void ADD(entity el){ // add entity into the list
  add_at_head(el);
 }


 public synchronized void add(entity el){ // add entity into the list
  // element el1 = fl.newelement(el);
  element el1 = new element(el); // fl.newelement(el);
  add_at_head(el1);
  //add_at_head(el);
 }

 public entity get_head(){         //ver 4.0 is returning an entity
  //protected element get_head()  // get the pointer of the first entity
  return head;                    // in the list
 }

 /* public */

 public container(){
  set_length(0);  // no entity in the container initially
  set_head(null);
 }

 public container(String NAME){
  super(NAME);
  set_length(0);   // no entity in the container initially
  set_head(null);
 }

/*
~container(){
  entity e = get_head();
  if(e != null && strcmp(e.get_classname(),"element")==0) {
    for(entity p=get_head();p!=null;p=p.get_right()){
       delete p;
    }
  }
}
*/

 public String get_classname(){
  return classname;  // returns the classification name
 }

 public boolean empty(){
  return (get_length() == 0);  // is the list of container empty?
 }

 public int get_length(){
   return length;
 }

 public void ADD(String NAME){
  entity e = new entity(NAME);
  ADD(e);
 }

 public void add(String NAME){
  entity  e = new entity(NAME);
  ADD(e);
 }

 public void multiple_add(entity ENT, int quantity){
  for( int q = 0; q < quantity; q++)
     ADD(ENT);
 }

 public void multiple_add(String NAME, int quantity){
  for( int q = 0; q < quantity; q++ )
     ADD(NAME);
 }

 public void APPEND(container c){
  entity e1;
  for(entity el=c.get_head(); el!=null; el=e1){
     e1 = el.get_right();
     ADD(el);
  }
 }

 public void append(container  c){
  for(entity  el=c.get_head();el!=null;el=el.get_right())
     ADD(el.get_ent());
 }

/*
 public void print(){
  System.out.print( "Container (" );
  for(entity  p = get_head(); p!=null; p=p.get_right()){
     p.print();
     System.out.print( " ");
  }
  System.out.println( ")" );
 }
*/

 // for tellallPrint Testing

 public void print(){
  //tellallPrint tc = new tellallPrint(this);
  //tc.start();
  print_all();
 }

 public void print_all(){
  for(entity  p = get_head();p != null;p = p.get_right())
     p.print();
  System.out.println();
 }

 public container names(){        // create another container object
  container c = new container();  // with all objects' names in the current
  entity e;                       // list of this container
  for(entity p = get_head();p != null;p = p.get_right()){
     e = new entity(p.get_name());
     c.ADD(e);
  }
  return(c);
 }

 public boolean is_in_name(String NAME){  // is entity with NAME in set?
  for(entity  p = get_head();p!= null;p = p.get_right())
     if(p.eq(NAME))
       return true;
  return false;
 }

 public entity find_with_name(String NAME){ // is entity with NAME in set?
  for(entity  p = get_head();p != null;p = p.get_right())
     if(p.eq(NAME))
       return p;
  return null;
}

 public boolean is_in(entity  ENT){ // is ent in this container?
  for(entity p = get_head();p!= null;p = p.get_right())
     if(p.equal(ENT))
       return true;
  return false;
 }

/*
 public void printall(){
  for(entity p = get_head(); p != null; p=p.get_right()){
     printThread pt = new printThread(p.get_ent());
     try{
      pt.start();
      pt.join();
     }
     catch(InterruptedException ignored){
		 System.out.println("Interrupted");
     }
     catch(ThreadDeath aTD){
	     System.out.println("thread is dead");
	     throw aTD;
     }

    //System.out.println(p.get_name());
  }
 }
*/
}

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
