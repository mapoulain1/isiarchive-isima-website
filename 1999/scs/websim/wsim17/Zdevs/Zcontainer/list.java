/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  list.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs.Zcontainer;

public class list extends function{
 public entity list_ref(int i){
  if ( 0 <= i && i < get_length())
  return assoc(new intEnt(i));
  else return null;
 }

 public void remove(int i){
  if( 0 <= i && i < get_length()){
    //super.remove_all(new intEnt(i));
	remove(new intEnt(i),list_ref(i));
	tellallGreater_dec(i);
  }
 }

 public void insert(entity e,int i){
  if( 0 <= i && i <= get_length()){
	tellallGreater_inc(i);
	add(new intEnt(i),e);
  }
 }

 private void tellallGreater_dec(int i){
  for(pair pr = (pair)get_head(); pr != null; pr = ((pair)(pr.get_right()))){
     intEnt ie = (intEnt)(pr.get_key());
     if(ie.getv() >= i)
       ie.setv(ie.getv() - 1);
  }
 }

 private void tellallGreater_inc(int i){
  for(pair pr = (pair)get_head(); pr != null; pr = ((pair)(pr.get_right()))){
     intEnt ie = (intEnt)(pr.get_key());
     if(ie.getv() >= i)
       ie.setv(ie.getv() + 1);
  }
 }
}