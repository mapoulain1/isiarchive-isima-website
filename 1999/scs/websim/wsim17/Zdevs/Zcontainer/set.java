/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  set.java
 *  Version    :  1.0
 *  Date       :  12-13-96
 *
 *  CLASS SET:
 *  A container class to perform Set operations.
 *  set.java  - methods definition of class set
 */

package Zdevs.Zcontainer;

public class set extends bag{

 /* private */

 static final String classname = "set";

 /* public: */

 public set(){}

 public String get_classname(){
  return classname;   // return the classification name of object
 }

 public void add(entity ent){  // add ent into set
  if (!is_in(ent))        // if it is not already in set,
	 super.add(ent); // for user compatability
 }

 public void ADD(entity ent){  // add ent into set
  if(!is_in(ent))        // if it is not already in set,
    super.ADD(ent); // add it
 }

 public void add_without_checking(entity ent){  // add ent into set
  super.add(ent); // add it
 }

 public void ADD_WITHOUT_CHECKING(entity ent){  // add ent into set
  super.ADD(ent); // add it
 }

 public void add(String NAME){  // add entity with name NAME into set
  if(!(is_in_name(NAME))) // add if it is not already in set
    super.add(NAME);  // for User compatability
 }

 public void ADD(String NAME){  // add entity with name NAME into set
  if(!(is_in_name(NAME))) // add if it is not already in set
    super.ADD(NAME);
 }

 public set union_objects(container c){
  set s = new set();
  entity el11;
  for(entity el1=get_head(); el1!=null; el1=el11){
     el11 = el1.get_right();
     s.add(el1.get_ent());
  }
  entity el22;
  for(entity el2=c.get_head(); el2!=null; el2=el22){
     el22 = el2.get_right();
     s.add(el2.get_ent());
  }
  return s;
 }

 public set UNION_OBJECTS(container c){
  set s = new set();
  entity el11;
  for(entity el1=get_head(); el1!=null; el1=el11){
     el11 = el1.get_right();
     s.ADD(el1);
  }
  entity el22;
  for(entity el2=c.get_head(); el2!=null; el2=el22){
     el22 = el2.get_right();
     s.ADD(el2);
  }
  return s;
 }

 public void union_self(container c){
  entity el1;
  for(entity el=c.get_head(); el!=null; el=el1){
     el1 = el.get_right();
     add(el.get_ent());
  }
 }

 public void UNION_SELF( container c ){
  entity el1;
  for(entity el=c.get_head(); el!=null; el=el1){
     el1 = el.get_right();
     ADD(el);
  }
 }

 public set intersect_objects(container c){
  set s = new set();
  entity el1;
  for(entity el =c.get_head(); el!=null; el=el1){
     el1 = el.get_right();
     if(is_in(el))
       s.add(el.get_ent());
  }
  return s;
 }

 public void intersect_self( container c ){
  entity el1;
  for(entity el=get_head(); el!=null; el=el1){
     el1 = el.get_right();
     if(!(c.is_in(el)))
       remove(el.get_ent());
  }
 }

 public set INTERSECT_OBJECTS(container c){
  set s = new set();
  entity el1;
  for(entity el =c.get_head(); el!=null; el=el1){
     el1 = el.get_right();
     if(is_in(el))
       s.ADD(el);
  }
  return s;
 }

 public void INTERSECT_SELF(container c){
  entity el1;
  for(entity el=get_head(); el!=null; el=el1){
     el1 = el.get_right();
     if(!(c.is_in(el)))
       remove(el);
  }
 }

 public set container_to_set( container c ){ // copy everything to set
  set s = new set();
  entity el1;
  for(entity el=c.get_head(); el!=null; el=el.get_right()){
     el1 = el.get_right();
     s.add(el);
  }
  return s;
 }

 public set bag_to_set( bag b ){
  return container_to_set(b);
 }
}
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
