 /*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  order.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */
package Zdevs.Zcontainer;

public class order extends bag{
 protected entity  maximum;

 public void copy_to(container r){
  //r.append(this);
  for(entity p = get_head();p != null; p = p.get_right())
     r.add(p.get_ent());
 }

 public void copy_back(container r){
  maximum = null;
  head = null;
  length = 0;;
  //append(r); doesn't work for some reason
  for(entity p = r.get_head();p != null; p = p.get_right())
     add(p.get_ent());
 }

 public order(){
  super();
  maximum = null;
 }

 public boolean greater_than(entity  n, entity  m){
  return n.greater_than(m);
 }

 public void add(entity   n){
  if(maximum == null || greater_than(n,maximum))
    maximum = n;
  super.add(n);
 }

 public entity  get_max(){
  return maximum;
 }


 public void remove(){
  super.remove(maximum);
  compute_max();
 }

 private void compute_max(){
  container   r = new container();
  copy_to(r);
  copy_back(r);
 }

 private void quicsort(entity  []a , int lo0, int hi0) {
  int lo = lo0;
  int hi = hi0;
  if(lo >= hi){
    return;
  }
  entity   mid = a[(lo + hi) / 2];
  while(lo < hi){
	  while(lo<hi && greater_than(a[lo],mid) ){
            lo++;
	  }
	  while(lo<hi && (!greater_than(a[hi],mid)) ){
		   hi--;
      }
      if(lo < hi){
        entity T = a[lo];
        a[lo] = a[hi];
        a[hi] = T;
      }
  }
  if(hi < lo){
    int T = hi;
    hi = lo;
    lo = T;
  }
  quicsort(a, lo0, lo);
  quicsort(a, lo == lo0 ? lo+1 : lo, hi0);
  }


 public void quicsort(entity []a,int len){
  quicsort(a, 0, len-1);
 }

 public list sort(){
  int len = get_length();
  entity []   array = new entity[len];
  int i = 0;
  for(entity   p = get_head();p != null;p = p.get_right())
     array[i++] = p.get_ent();
  quicsort(array, len);
  list   l = new list();
  for(i=0;i<len;i++){
     l.insert(array[i],0);
  }
  return l;
 }
}





