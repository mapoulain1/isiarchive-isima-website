/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  Param.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import java.awt.*;
import java.applet.*;
import Zdevs.Zcontainer.*;

public class Param extends Dialog{
 String s;

 public String getS(){
  return s;
 }

 public Param(container buttons){
  super(new Frame(),"Set Parameters", false);
  resize(400, 100);
  setLayout(new FlowLayout());
  show();
  init(buttons);
 }

 public void makeButton(String s){
  add(new Button(s));
 }

 public void init(container buttons){
  for(entity p = buttons.get_head();p != null;p = p.get_right()){
     entity ent = p.get_ent();
     makeButton(ent.get_name());
  }
  //makeButton("No Parameter Buttons Made");
 }

 public boolean action(Event evt, Object obj){
  if(evt.target instanceof Button){
    Button b = (Button)evt.target;
    s = b.getLabel();
  }
  return false;
 }
}