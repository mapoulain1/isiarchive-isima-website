/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : MovingFrame.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
/*
    This class is a basic extension of the Frame class.  It can be used
    by an applet or application.  To use it, create a reference to the class,
    then instantiate an object of the class, and call the show() method.


    example:

    MovingFrame theMovingFrame;
    theMovingFrame = new MovingFrame();
    theMovingFrame.show();

    You can add controls or menus to MovingFrame with Cafe Studio.
 */

import java.awt.*;
import Zdevs.Zcontainer.*;

public class MovingFrame extends Frame {

    int myx,myy;
    static int num_of_instances =0;
    private devs mySource;
    private content myContent;

    private Image my_image;

private int mx, my;

    //public MovingFrame(Frame parent,String title, boolean modal
    //              ,devs d,content c, Image im) {
    public MovingFrame(devs d,content c, Image im, int x , int y) {
        //super(parent,"MovingFrame window",modal);
        super();
	    mySource = d;
	    myContent = c;
        my_image = im;
        //{{INIT_CONTROLS
        setLayout(null);
        mx = x;
        my = y;
        addNotify();
        resize(insets().left + insets().right + 83, insets().top + insets().bottom + 83);
        //}}

        //{{INIT_MENUS
        //}}
        num_of_instances++;
    	setResizable(false);

    }

    public void paint(Graphics g)
    {
        String temp = " ";
        if(my_image != null)
            g.drawImage(my_image,0,0,100,50,this);
        else
            g.drawString(myContent.val.get_name(),20,20);
        g.drawString(temp.valueOf(mx), 5,60);
        g.drawString(temp.valueOf(my), 50,60);
    }

    public synchronized void show() {
    	move(50, 50);
    	super.show();
    }

    public boolean handleEvent(Event event) {
    	if (event.id == Event.MOUSE_DOWN && event.target == this) {
    	    	mouseDownThis(event);
    	    	return true;
    	}
    	else
    	if (event.id == Event.MOUSE_UP && event.target == this) {
    	    	mouseUpThis(event);
    	    	return true;
    	}
    	else

    	if (event.id == Event.WINDOW_DESTROY) {
    	    hide();
    	    return true;
    	}
    	return super.handleEvent(event);
    }

    //{{DECLARE_MENUS
    //}}

    //{{DECLARE_CONTROLS
    //}}
    public void mouseUpThis(Event ev) {
        // to do: put event handler code here.
    }
    public void mouseDownThis(Event ev) {
        // to do: put event handler code here.
                Frame f = new Frame();
        OutDisplay od = new OutDisplay(f,
             "Output Info",false
              ,mySource,myContent);

        od.move(
                 (myx + num_of_instances*5)%600,
                 (500+num_of_instances*5)%800
                 );
        od.show();

    }

    public void set_info_location(int x, int y)
    {
        myx = x;
        myy = y;
    }

}


