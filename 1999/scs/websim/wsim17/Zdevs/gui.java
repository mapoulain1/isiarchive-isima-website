/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : gui.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


package Zdevs;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.io.*;
import Zdevs.Zcontainer.*;

public class gui extends Panel{

    protected  Choice menu;
    protected  Button bch,bpar;
    protected  ParaDialog firstPanel;
    protected atomicDrawPanel myUserFace;
    protected infoEnt ie;

public gui(atomicDrawPanel myUser){
   super();
   myUserFace = myUser;
   init();
}



public void makeMenu(){
    menu = new Choice();
    list infoEnts =  myUserFace.getInfoEnts();
    for(int j = 0;j < infoEnts.get_length();j++){
      entity ent = infoEnts.list_ref(j);
       menu.addItem(ent.get_name());
    }
}

public void init()
{

		resize(400, 200);
		setLayout(new FlowLayout());
    	bpar =  new Button("Parameter");
        add(bpar);



	   System.out.println("Finished init");

}


public  void addChoice(){
       if (firstPanel != null) return;
        firstPanel = new ParaDialog(this);
        resize(200, 200);
		setLayout(new FlowLayout());
         bch =  new Button("Select Choice");
         firstPanel.add(bch);
         makeMenu();
		 firstPanel.add(menu);
}

public  void getInstance(){

 if (ie != null) firstPanel.remove(ie.getPanel());
   String s = menu.getSelectedItem();
    entity e = myUserFace.withName(s);
   ie = (infoEnt)e;
   firstPanel.add(ie.getPanel());
 //  ie.refresh();
  firstPanel.resize(600,200);
   firstPanel.validate();
   }

public void getInfo(){
        ie.getInfo();
}

public void removeSelf(){
    remove(firstPanel);
      firstPanel.remove(ie.getPanel());
}


}
