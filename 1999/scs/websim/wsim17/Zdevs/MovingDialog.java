/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  MovingDialog.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */
 /*
    This class is a basic extension of the Dialog class.  It can be used
    by subclasses of Frame.  To use it, create a reference to the class,
    then instantiate an object of the class (pass 'this' in the constructor),
    and call the show() method.

    example:

    MovingDialog theMovingDialog;
    theMovingDialog = new MovingDialog(this);
    theMovingDialog.show();

    You can add controls to AboutBox with Cafe Studio.
    (Menus can be added only to subclasses of Frame.)
 */

package Zdevs;
import java.awt.*;

public class MovingDialog extends Dialog{
 int myx,myy;
 static int num_of_instances =0;
 private devs mySource;
 private content myContent;

 public MovingDialog(Frame parent,String title, boolean modal,devs d,content c){
  super(parent, title, modal);
  mySource = d;
  myContent = c;
  //{{INIT_CONTROLS
  setLayout(null);
  addNotify();
  resize(insets().left + insets().right + 83, insets().top + insets().bottom + 53);
  Output=new Button("Output");
  add(Output);
  Output.reshape(insets().left + 7,insets().top + 6,63,20);
  //}}
  num_of_instances++;
  setResizable(false);
 }

 public synchronized void show() {
  Rectangle bounds = getParent().bounds();
  Rectangle abounds = bounds();
  move(bounds.x + (bounds.width - abounds.width)/ 2,
       bounds.y + (bounds.height - abounds.height)/2);
  super.show();
 }

 public synchronized void wakeUp() {
  notify();
 }

 public boolean handleEvent(Event event) {
  if(event.id == Event.ACTION_EVENT && event.target == Output) {
    clickedOutput();
    return true;
  }

  /*else if(event.id == Event.WINDOW_DESTROY){
    hide();
    return true;
    }*/

  return super.handleEvent(event);
 }

  //{{DECLARE_CONTROLS
  Button Output;
   //}}

 public void clickedOutput(){
  // to do: put event handler code here.
  Frame f = new Frame();
  OutDisplay od = new OutDisplay(f,"Output Info",false,mySource,myContent);
  od.move((myx + num_of_instances*5)%600,(500+num_of_instances*5)%800);
  od.show();
 }

 public void set_info_location(int x, int y){
  myx = x;
  myy = y;
 }
}


