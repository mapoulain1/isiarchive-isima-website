/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   :  schedul.java
 *  Version    :  1.0
 *  Date       :  12-11-96
 */

package Zdevs;
import java.lang.*;
import Zdevs.Zcontainer.*;

 class intEvent extends devs{
  public String phase;
  public int sigma;
  public String outPort = null;
  public content Content;

 public intEvent(String Phase, int Sigma, String out){
  super("intEnt");
  phase = Phase;
  sigma = Sigma;
  outPort = out;
  Content = make_content(out,new entity("notify"));
 }

 public intEvent(String Phase, int Sigma, content cont){
  super("intEnt");
  phase = Phase;
  sigma = Sigma;
  Content = cont;
 }
}

public class schedul extends atomic{

 public void addIntEvent(String Phase, int Sigma, String out){
  q.add(new intEvent(Phase, Sigma,out));
 }

 protected intEvent event;
 protected queue q;

 public  schedul (){
  super(" schedul ");
  inports.add("stop");
  inports.add("start");
  outports.add("out");
  phases.add("active");
  q = new queue();
  addTestPortValue("start",new entity("val"));
  addTestPortValue("stop",new entity("val"));
  initialize();
 }

 public schedul(String name){
  super(name);
  inports.add("stop");
  inports.add("start");
  outports.add("out");
  phases.add("active");
  q = new queue();
  initialize();
 }

 public void initialize(){
  phase = "active";
  sigma = 100;
  super.initialize();
 }

 public void addIntEvent(String Phase, int Sigma, content cont){
  q.add(new intEvent(Phase, Sigma,cont));
 }

 protected void addIntEvents(){
  addIntEvent("p1",1000,"o1");
  content c = make_content("p2", new entity("special"));
  addIntEvent("p2",2000,c);
 }

 /*
  public void showState(){
    super.showState();
 }

 public void myRepaint(){
  super.myRepaint();
 }
 */

 public void  deltext(int e,message x){
  Continue(e);
  for(int i=0; i< x.get_length();i++)
     if(message_on_port(x,"start",i))
       hold_in("active",0);
     for(int i=0; i< x.get_length();i++)
      if(message_on_port(x,"stop",i))
        passivate();
 }

 public void  deltint( ){
  if(phase_is("active")){
    entity ent = q.front();
    q.remove();
    event = (intEvent)ent;
    hold_in(event.phase,event.sigma);
  }
  else if(phase_is(event.phase) && q.get_length() > 0){
    entity ent = q.front();
    q.remove();
    event = (intEvent)ent;
    hold_in(event.phase,event.sigma);
  }
  else passivate();
 }

 public message  out( ){
  message  m = new message();
  content con;
  if(!phase_is("active") && phase_is(event.phase)){
    if(event.outPort != null)
      con = make_content(event.outPort, new entity("notice"));
    else  con = event.Content;
    m.add(con);
  }
  return m;
 }
}

