/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : pipeCoord.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import Zdevs.*;
import Zdevs.Zcontainer.*;

public class pipeCoord extends Coord{

 //protected queue  procs;
 protected queue   jobs;
 protected proc   pcur,pfirst;
 protected function next;

 public pipeCoord(String   name){
  super(name);
  next = new function();
  jobs = new queue();
  //procs = new queue();
  phases.add("send_first");
 }

 public pipeCoord(){
  super("pipeCoord");
  next = new function();
  phases.add("send_first");
  jobs = new queue();
  //procs = new queue();
  //CAUTION: start with port "setup" to test
  addTestPortValue("setup",new entity(""));
  addTestPortValue("in",new entity("val"));
  addTestPortValue("x",new pair(new proc("p0",1000),
                       new entity("val")));
  addTestPortValue("x",new pair(new proc("p1",1000),
                       new entity("val")));
  message m = new message();
  m.add(make_content("in", new entity("val1")));
  m.add(make_content("in",new entity("val2")));
  addTest("multiple inputs", m);
  initialize();
 }

 public void initialize(){
  phase = "passive";
  sigma = INFINITY;
  job = null;
  super.initialize();;
 }

 protected void add_procs(proc  p){
  if(next.empty())
    pfirst = p;
  else
   next.replace(pcur,p);
  next.add(p,new proc("null",100));
  pcur = p;
 }

 public void showState(){
  super.showState();
  addString("number of procs: " + next.get_length());
  addString("number of jobs: " + jobs.get_length());
 }

 nameGen n = new nameGen();

 public void  deltext(int e,message  x){
  Continue(e);
  if(phase_is("passive")){
    for(int i=0; i< x.get_length();i++)
       if(message_on_port(x,"setup",i))
         add_procs(new proc(n.getName("p"),1000));
  }
  if(phase_is("passive")){
    for(int i=0; i< x.get_length();i++)
       if(message_on_port(x,"in",i)){
         job = x.get_val_on_port("in",i);
         pcur = pfirst;
         hold_in("send_first",100);
       }

  }
  // (proc,job) pairs returned on port x
  //always accept so that no processor is lost
  for(int i=0; i< x.get_length();i++)
  if(message_on_port(x,"x",i)){
    entity  val = x.get_val_on_port("x",i);
    jobs.add(val);
  }
  //output completed jobs at earliest opportunity
  if(phase_is("passive") && !jobs.empty()){
    hold_in("send_y",100);
  }
  // show_state();
 }

 public void  deltint( ){
  if(phase_is("send_y")){
    jobs = new queue();
    passivate();
  }
  //output completed jobs at earliest opportunity
  else if(phase_is("send_first") && !jobs.empty())
    hold_in("send_y",100);
  else passivate();
  //show_state();
 }

 public message    out( ){
  message m = new message();
  if(phase_is("send_first"))
    m.add(make_content("y",new pair(pcur,job)));
  else if(phase_is("send_y"))
  for(int i= 0; i< jobs.get_length();i++){
     entity   val = jobs.list_ref(i);
     pair pr = (pair)val;
     entity  p_return = pr.get_key();
     proc pnext = ((proc)(next.assoc(p_return.get_name())));
     entity jb = pr.get_value();
     if(!pnext.eq("null"))
        m.add( make_content("y",new pair(pnext,jb)));
     else
      m.add( make_content("out",jb));
  }
  return m;
 }

 public void show_state(){
  System.out.println(   "state of  " +  name +  ": " );
  System.out.println(  "phase, sigma : "+ phase +  " " +  sigma );
  System.out.println( );
 }
}


