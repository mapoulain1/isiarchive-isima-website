/***************************************************************
	ECE 575
	Project:  Distributed Object Computing
	Daryl Hild


File:  MAUrcvr.java

              +---MAUrcvr----------+
         in   |      sigma         |  out
         ---->|      phase         |----->
              |      buffer        |
              |      media_state   |
              |  rcv_service_time  |
              +--------------------+

ATOMIC-MODEL:  MAUrcvr
   assumptions:
      1.  Only input and output ports declared below are valid.
      2.  Each frame for this destination takes rcv_service_time
          units to process before deliverying to node.
      3.  Only three types of frames appear on ethernet:
          Preamble, Frame, and NoiseBurst.

   state variables:
      sigma         INFINITY
      phase         passive     (phases: passive, busy)
      buffer        nil
      media_state   idle

   parameters:
      rcv_service_units  5
      ZERO_TIME          100
      rcv_sim_time       if ZERO_TIME==0
                            =rcv_service_units
                         else
                            =rcv_service_units*ZERO_TIME

   input-ports:
      in

   output-ports:
      out

   external transition function:
      for each message on input-port in
         set frame to content-value of message
         case media_state
              idle            if frame==Preamble
                                 media_state = single_carrier
                              else
                                 media_state = idle
              single_carrier  if frame==Preamble
                                 media_state = multi_carriers
              multi_carriers  if frame==NoiseBurst
                                 media_state = idle
      if frame!=Preamble && frame!=NoiseBurst && media_state=single_carrier
         media_state = idle
         parse pdu(dst, length, payload) from frame
         if (dst == this node)
            add pdu(dst, length, payload) to buffer
            if phase is passive
               hold-in busy rcv_sim_time
            else
               Continue
         else  // ignore message; not my address

   internal transition function:
      case phase
           passive    error: Passive phase invalid
           busy       remove pdu from buffer
                      if buffer.empty
                         passivate
                      else
                         hold-in busy rcv_sim_time
           else       error: Unknown phase
                      passivate

   output function:
      get pdu from buffer
      send pdu on output-port out

***************************************************************/


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;


public class MAUrcvr extends atomic {


	protected queue buffer;
	protected int rcv_sim_time, rcv_service_time, ZERO_TIME;
	protected String media_state;

	public MAUrcvr(String MAUname, int setRcvSvcTime, int setZeroTime) {
	   super(MAUname+"_rcvr");
	   phases.add("busy");
	   rcv_service_time = setRcvSvcTime;
	   ZERO_TIME = setZeroTime;
	   initialize();
	}


	public MAUrcvr() {
	   super("MAU_rcvr");
	   phases.add("busy");
	   rcv_service_time = 5;
	   ZERO_TIME = 100;
	   addTestPortValue("in",new entity("Preamble"));
	   addTestPortValue("in",new entity("NoiseBurst"));
	   addTestPortValue("in", new pair(
	      new pair(new entity("MAU"), new entity("src1")),
	      new pair(new intEnt(1200000),new entity("payload_1"))));
	   addTestPortValue("in", new pair(
	      new pair(new entity("dst2"), new entity("src3")),
	      new pair(new intEnt(2400000),new entity("payload_2"))));
	   initialize();
	}


	public void initialize() {
	    phase = "passive";
	    sigma = INFINITY;
	    buffer = new queue();
	    media_state="idle";
	    if (ZERO_TIME==0)  rcv_sim_time=rcv_service_time;
	    else               rcv_sim_time=rcv_service_time*ZERO_TIME;
	    super.initialize();
	}


	public void deltext(int et, message x) {
	    entity frame = new entity("Preamble");
	    for (int i=0; i< x.get_length();i++) {
	        if (message_on_port(x,"in",i)) {
	            frame = x.get_val_on_port("in",i);
	            if (media_state=="idle") {
	                if (frame.eq("Preamble"))
	                    media_state = "single_carrier";
	                else
	                    media_state = "idle";
	            }
	            else if (media_state=="single_carrier") {
	                if (frame.eq("Preamble"))
	                    media_state = "multi_carriers";
	            }
	            else { //media_state=="multi_carriers"
	                if (frame.eq("NoiseBurst"))
	                    media_state = "idle";
	            }
	        }
	    }
	    if (frame.eq("Preamble")) {
	        System.out.println(name + " EXTERNAL --> "
	          + "received: Preamble; media_state: " + media_state);
	    }
	    else if (frame.eq("NoiseBurst")) {
	        System.out.println(name + " EXTERNAL --> "
	          + "received: NoiseBurst; media_state: " + media_state);
	    }
	    else if (media_state=="single_carrier") {
	        media_state = "idle";
	        // parse pdu (dst, length, payload) from frame
	        pair   framePr      = (pair)frame;
	        entity   addressEnt = framePr.get_key();
	        pair     addressPr  = (pair)addressEnt;
	        entity     dst      = addressPr.get_key();
	        entity   pduEnt     = framePr.get_value();
	        if (this.eq(dst.get_name()+"_rcvr")) {
	            buffer.add(new pair(dst, pduEnt));
	            if (phase_is("passive"))
	                hold_in("busy", rcv_sim_time);
	            else
	                Continue(e);
	            System.out.println(name + " EXTERNAL --> "
	            + "received and processing: " + frame.get_name());
	        }
	        else {
	            System.out.println(name + " EXTERNAL --> "
	            + "not my address, ignoring: "
	            + frame.get_name());
	        }
	    }
	    else {
	        System.out.println(name + " EXTERNAL --> "
	          + "detected a collision; media_state: " + media_state);
	    }
//	    show_state(" EXTERNAL --> ");
	}


	public void  deltint() {
	   if (phase_is("passive"))
	      System.out.println(name + " INTERNAL --> "
	        + "Error: Passive phase is invalid.");
	   else if (phase_is("busy")) {
	      buffer.remove();
	      if (buffer.empty())   passivate();
	      else                  hold_in("busy", rcv_sim_time);
	   }
	   else {
	      System.out.println(name + " INTERNAL --> "
	        + "Error: Unknown phase.");
	      passivate();
	   }
//	   show_state(" INTERNAL --> ");
	}


	public message out() {
	   message m = new message();
	   entity pdu = buffer.front();
	   m.add(make_content("out", pdu));
//	   System.out.println(" OUTPUT FUNCTION --> "
//	     + "pdu on output-port out is: "
//	     + pdu.get_name());
	   return m;
	}


	public void show_state(String header) {
	    System.out.println(name + header
	      + " state is:    "
	      + "; phase: " + phase
	      + "; sigma: " + sigma
	      + "; media_state: " + media_state
	      + "; buffer length: " + buffer.get_length());
//	    buffer.print();
	}
}
