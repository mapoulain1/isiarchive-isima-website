/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atoms.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */

import Zdevs.Zcontainer.*;


public class atoms extends  list{
  int i = 0;

public atoms(){

		menuAddItem("Ethernet");
		menuAddItem("MAUxmtr");
		menuAddItem("MAUrcvr");
		menuAddItem("PKTgenr");
		menuAddItem("ETHERtransd");
		menuAddItem("LCNtransd");

		menuAddItem("atomGpt");
		menuAddItem("atomMult");
		menuAddItem("atomPipe");
		menuAddItem("basicProc");
		menuAddItem("divideCoord");
		menuAddItem("genr");
		menuAddItem("multiServerCoord");
		menuAddItem("pipeCoord");
		menuAddItem("priorityQ");
		menuAddItem("proc");
		menuAddItem("procQ");
		menuAddItem("randGenr");
		menuAddItem("transd");

}

protected void menuAddItem(String s){
		insert(new entity(s),i++);
}
}
