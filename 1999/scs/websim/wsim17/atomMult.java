/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atomMult.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */

import Zdevs.*;
import Zdevs.Zcontainer.*;

public class atomMult extends atomDigraph{


public atomMult(){
  super(new multiServer());
  inports.add("in");
  outports.add("out");
  addTestPortValue("in",new entity("job"));
  addTestPortValue("none",new entity("job"));
}

public atomMult(String name,int proc_time,int size){
  super(new multiServer(name,proc_time,size));
  inports.add("in");
  outports.add("out");
}
}