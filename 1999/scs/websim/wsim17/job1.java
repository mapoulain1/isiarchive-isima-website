/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : job1.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import Zdevs.*;
import Zdevs.Zcontainer.*;

public class job1 extends job{
 public int size;

 public job1(String name){
  super(name);
  size = 1;
 }

 public job1(String name,int Processing_time,int Size){
  super(name,Processing_time);
  size = Size;
 }

 public boolean greater_than( entity  m){
  job1 jm = (job1)m;
   if(super.equal(m))
     return size > jm.size;
    else return super.greater_than(m);
 }

 public void print(){
  System.out.println("job: "+name +" processing time: " + processing_time
         + " size: " + size
         );
 }
}