/***************************************************************
	ECE 575
	Homework #5
	Daryl Hild

File:  PKTgenr.java

               +---PKTgenr------+
               |                |  out1
     startStop |      sigma     |----->
     --------->|      phase     |  out2
               |      pktCount  |----->
               |   NodeName     |    :
               |   numNodes     |    :
     setLoad   |   load         |  outX
     --------->|   ZERO_TIME    |----->
               +----------------+

Assumptions:
     1. Only input and output ports declared below are legitimate.
     2. Values for phase are limited to those specified below;
        all others are disallowed.
     3. The distribution of packet generation times is random.


DEVS ATOMIC-MODEL:  PKTgenr
     state variables:
                   sigma       INFINITY
                   phase       passive     (phases: passive, busy)
                   pktCount    0
     parameters:   NodeName    NODE
                   numNodes    1
                   load        20
                   ZERO_TIME   100
     input-ports:  startStop
                   setLoad
     output-ports: out
     external transition function:
          for each message
               case input-port
                    setLoad     set load with content-value
                    startStop   case phase
                                     busy     passivate
                                     else     hold_in busy rand_time()
                                set pktCount with content-value

     internal transition function:
          decrement pktCount
          if pktCount<=0
             passivate
          else
             hold_in busy rand_time()

     output function:
          len = random payload length
          src = random source node selection
          dst = random destination node selection
          build packet with len bytes
          send packet to output-port out

***************************************************************/


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;
import java.util.*;


public class PKTgenr extends atomic {


	protected int pktCount, numNodes, load, ZERO_TIME;
	protected String NodeName;


	public PKTgenr() {
		super("PKTgenr");
		inports.add("startStop");
		inports.add("setLoad");
		outports.add("out1");
		phases.add("busy");
		NodeName = "NODE";
		numNodes = 1;
		load = 20;
		ZERO_TIME = 100;
	    addTestPortValue("startStop", new intEnt(5));
	    addTestPortValue("startStop", new intEnt(10));
	    addTestPortValue("startStop", new intEnt(20));
	    addTestPortValue("setLoad", new intEnt(10));
	    addTestPortValue("setLoad", new intEnt(20));
	    addTestPortValue("setLoad", new intEnt(40));
		initialize();
	}


	public PKTgenr(String Name, String nodeName, int NumNodes, int Load, int ZeroTime) {
		super(Name);
		inports.add("startStop");
		inports.add("setLoad");
		for (int i=1; i<=NumNodes; i++)
		   outports.add("out" + i);
		phases.add("busy");
		NodeName = nodeName;
		numNodes = NumNodes;
		load = Load ;
		ZERO_TIME = ZeroTime;
		initialize();
	}


	public void initialize() {
		phase = "passive";
		sigma = INFINITY;
		pktCount = 0;
		super.initialize();
	}


	public void  deltext(int et, message x) {
	   for (int i=0; i< x.get_length();i++) {
	      if (message_on_port(x,"setLoad",i)) {
	         entity en = x.get_val_on_port("setLoad",i);
	         intEnt setting = (intEnt)en;
	         load = setting.getv();
	         System.out.println(name + " EXTERNAL --> "
	           + "packet generator load reset to: " + load);
	      }
	      else if (message_on_port(x,"startStop",i)) {
	         if (phase_is("busy")) {
	            passivate();
	            System.out.println(name + " EXTERNAL --> "
	              + "packet generator stopped with packet count: " + pktCount);
	         }
	         else {
	            hold_in("busy", rand_time());
	            System.out.println(name + " EXTERNAL --> "
	              + "packet generator started.");
	              entity en = x.get_val_on_port("startStop",i);
	              intEnt setting = (intEnt)en;
	              pktCount = setting.getv();
	         }
	      }
	   }
//	   show_state(" EXTERNAL --> ");
	}


	public void  deltint() {
	    pktCount = pktCount - 1;
	    if (pktCount<=0) {
	        passivate();
	        System.out.println(name + " INTERNAL --> "
	          + "packet count is zero; packet generator stopped.");
	    }
	    else {
	        hold_in("busy", rand_time());
	    }
//	    show_state(" INTERNAL --> ");
	}


	public message out() {
	   message  m = new message();
	   int temp = randInt();
//	   int len = temp - 1500 * (int)(temp/1500); // 1500 is max 802.3 payload length
	   int len = temp - 50 * (int)(temp/50);     // use 50 for faster simulations
	   temp = randInt();
	   int src = 1 + temp - numNodes * (int)(temp/numNodes);
	   temp = randInt();
	   int dst = 1 + temp - numNodes * (int)(temp/numNodes);
	   pair packet = new pair(new entity(NodeName + dst),
	      new pair(new intEnt(len),new entity("Payload_" + pktCount)));
	   content con = make_content("out" + src, packet);
	   m.add(con);
//	   System.out.println(name + " OUTPUT --> "
//	     + "packet on output-port 'out" + src + "' is: "
//	     + packet.get_key().get_name() + packet.get_value().get_name());
//	   show_state(" OUTPUT --> ");
	   return m;
	}


	public void show_state(String header) {
		System.out.println(name + header
		  + "     state is"
		  + ":    phase: " + phase
		  + ",    sigma: " + sigma
		  + ",    load: " + load
		  + ",    packet count: " + pktCount);
	}


	public int rand_time() {
	    int temp = randInt();
	    int loadFactor = 1000/load;
	    int RandTime = temp - loadFactor * (int)(temp/loadFactor);
	    if (ZERO_TIME!=0)   RandTime = RandTime * ZERO_TIME + ZERO_TIME;
	    return RandTime;
	}


	private int randInt() {
	   Random rand = new Random();
	   int randInt = rand.nextInt();
	   if (randInt < 0)   randInt = randInt * (-1);
	   return randInt;
	}
}