/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : transd.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;


public class transd extends atomic{
 protected function  arrived, solved;
 protected int clock,total_ta,observation_time;

 public transd(String  name,int Observation_time){
  super(name);
  inports.add("ariv");
  inports.add("solved");
  outports.add("out");
  phases.add("active");
  arrived = new function();
  solved = new function();
  observation_time = Observation_time;
  initialize();
 }

 public transd(){
  super("transd");
  inports.add("ariv");
  inports.add("solved");
  outports.add("out");
  phases.add("active");
  arrived = new function();
  solved = new function();
  observation_time = 1000;//100000;
  addTestPortValue("ariv",new entity("val"));
  addTestPortValue("solved",new entity("val"));
  initialize();
 }

 public void initialize(){
  phase = "active";
  sigma = observation_time;
  clock = 0;
  total_ta = 0;
  super.initialize();
 }

 public void showState(){
  super.showState();
  addString("arrived: " + arrived.get_length());
  addString("solved: " + solved.get_length());
  addString("TA: "+compute_TA());
  addString("Thruput: "+compute_Thru());
 }

 public void  deltext(int e,message  x){
  clock = clock + e;
  Continue(e);
  entity  val;
  for(int i=0; i< x.get_length();i++){
    if(message_on_port(x,"ariv",i)){
       val = x.get_val_on_port("ariv",i);
           arrived.add(val,new intEnt(clock));
    }
    if(message_on_port(x,"solved",i)){
       val = x.get_val_on_port("solved",i);
       if(arrived.key_name_is_in(val.get_name())){
         entity  ent = arrived.assoc(val.get_name());
         intEnt  num = (intEnt)ent;
         int arrival_time = num.getv();
         int turn_around_time = clock - arrival_time;
         total_ta = total_ta + turn_around_time;
         solved.add(val, new intEnt(clock));
       }
    }
  }
  show_state();
  // showState();
 }

 public void  deltint(){
  clock = clock + sigma;
  passivate();
  show_state();
 }

 public  message    out( ){
  message  m = new message();
  content  con = make_content("out",new entity("TA: "+compute_TA()));
  m.add(con);
  return m;
 }

 public float compute_TA(){
  float avg_ta_time = 0;
  if(!solved.empty())
    avg_ta_time = total_ta/solved.get_length();
  return avg_ta_time;
 }

 public float compute_Thru(){
  float thruput = 0;
  if(clock > 0)
    thruput = solved.get_length()/(float)clock;
  return thruput;
 }

 public void  show_state(){
  System.out.println("state of  "  +  name  +  ": " );
  System.out.println("phase, sigma : "
          + phase  +  " "  +  sigma  +  " "   );
  System.out.println( " tL = " + tL + " tN = " + tN);
  System.out.println(" jobs arrived :" );
            arrived.print_all();;
  System.out.println("total :"  +  arrived.get_length()  );

  System.out.println("jobs solved :" );
            solved.print_all();
  System.out.println("total :"  +  solved.get_length()  );
  System.out.println("AVG TA = "  +  compute_TA()   );
  System.out.println("THRUPUT = "  +  compute_Thru()  );
 }
}
