/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : atomGpt.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */

import Zdevs.*;
import Zdevs.Zcontainer.*;

public class atomGpt extends atomDigraph{


public atomGpt(){
  super(new gpt());
  inports.add("start");
  outports.add("out");
  addTestPortValue("start",new entity("val"));
}


}
