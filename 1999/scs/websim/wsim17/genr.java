/*      Copyright 1996 Arizona Board of regents on behalf of
 *                  The University of Arizona
 *                     All Rights Reserved
 *         (USE & RESTRICTION - Please read COPYRIGHT file)
 *
 *  Filename   : genr.java
 *  Version    :  1.0
 *  Date       : 12-4-96
 */


import java.lang.*;
import Zdevs.*;
import Zdevs.Zcontainer.*;

public class genr extends atomic{
 protected int int_arr_time;
 protected int count;
 protected entity ent;

 public genr(){
  super("genr");
  inports.add("stop");
  inports.add("start");
  inports.add("setPeriod");
  outports.add("out");
  phases.add("busy");
  int_arr_time = 3000;
  addTestPortValue("start",new entity("val"));
  addTestPortValue("stop",new entity("val"));
  addTestPortValue("setPeriod",new intEnt(500));
  addTestPortValue("setPeriod",new intEnt(1000));
  addTestPortValue("setPeriod",new intEnt(2000));
  initialize();
 }

 public genr(String name,int Int_arr_time){
  super(name);
  inports.add("stop");
  inports.add("start");
  inports.add("setPeriod");
  outports.add("out");
  phases.add("busy");
  int_arr_time = Int_arr_time ;
  initialize();
 }

 public void initialize(){
  phase = "passive";
  sigma = INFINITY;
  count = 0;
  super.initialize();
  showState();
 }

 public void showState(){
  super.showState();
  addString("count: " + count);
  addString("Period: " + int_arr_time);
 }

 public void myRepaint(){
  super.myRepaint();
  if(myAtomicDrawPanel != null){
    myAtomicDrawPanel.graph.SetPhaseElapsed(count,e/100,phase);
   myAtomicDrawPanel.repaint();
  }
 }

 public void  deltext(int e,message x){
  Continue(e);
  for(int i=0; i< x.get_length();i++)
     if(message_on_port(x,"setPeriod",i)){
       entity en = x.get_val_on_port("setPeriod",i);
       intEnt in = (intEnt)en;
       int_arr_time = in.getv();
     }
  for(int i=0; i< x.get_length();i++)
     if(message_on_port(x,"start",i)){
       ent = x.get_val_on_port("start",i);
       hold_in("busy",int_arr_time);
     }
  for(int i=0; i< x.get_length();i++)
     if(message_on_port(x,"stop",i))
       passivate();
 }

 public void  deltint( ){
  if(phase_is("busy")){
    count = count +1;
    hold_in("busy",int_arr_time);
  }
 }

 public message  out( ){
  message  m = new message();
  content con = make_content("out",
  new entity("job" + count));
  m.add(con);
  return m;
 }

 public void show_state(){
  System.out.println();
  System.out.println("state of  " + name + ": " );
  System.out.println("phase, sigma,count : "
         + phase + " " + sigma + " " + count);
 }
}

