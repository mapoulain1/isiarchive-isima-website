<HTML>
<HEAD>
   <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso-8859-1">
   <META NAME="GENERATOR" CONTENT="Mozilla/4.01 [en] (Win95; I) [Netscape]">
   <META NAME="Author" CONTENT="Andrea D'Ambrogio">
   <META NAME="Description" CONTENT="Paper Submission to:1998 International Conference on Web-based Modeling and SimulationJanuary 11-14, 1998, San Diego, CA, USAPart of the 1998 SCS MultiConference on Computer Simulation">
   <TITLE>A Web-based Environment for the Reuse of Simulation Models</TITLE>
</HEAD>
<BODY TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#0000EE" VLINK="#551A8B" ALINK="#FF0000">

<CENTER><B><FONT SIZE=+1>A WEB-BASED ENVIRONMENT</FONT></B></CENTER>

<CENTER><B><FONT SIZE=+1>FOR</FONT></B> <B><FONT SIZE=+1>THE REUSE OF</FONT></B></CENTER>

<CENTER><B><FONT SIZE=+1>SIMULATION MODELS</FONT></B></CENTER>

<CENTER><BR>
<A HREF="mailto:iazeolla@info.utovrm.it">Giuseppe Iazeolla</A> and <A HREF="mailto:dambro@info.utovrm.it">Andrea
D'Ambrogio</A></CENTER>

<CENTER><BR>
Laboratory for Computer Science and <A HREF="http://www.info.utovrm.it">CERTIA
Research Center</A> (*)</CENTER>

<CENTER>University of Roma "Tor Vergata"</CENTER>

<CENTER>Roma (Italy)</CENTER>

<CENTER><BR>
<B><FONT SIZE=+1>Abstract</FONT></B></CENTER>


<P>The effort to develop a simulation application can be drastically reduced
if the user is enabled to access libraries of simulation models, retrieve
the model of interest, modify it for reuse at the local site in a local
application, and run the local application by accessing a simulation tool
available elsewhere.

<P>For example, a user residing at site A might wish to develop a large
simulation model SM and run it by the simulation tool ST not available
in A. By use of the Web, the user may find out that an interesting submodel
SBM has already been developed at site B and that at site C there exists
the simulation tool ST.

<P>The proposed Web-based environment helps the user residing in A to find
out which types of SBMs or STs exist elsewhere on the Internet, download
the interesting SBM from a remote site B to site A, integrate the SBM into
SM at site A and, finally, run SM by accessing ST at a remote site C.

<P>The paper introduces a Java and CORBA-based architecture for the Web-based
environment, and presents a prototype environment for queueing network
simulation models implemented in the language of the QNAP2 simulation tool.

<P><B><FONT SIZE=+1>1. Introduction</FONT></B>

<P>Modeling and simulation is used in an increasing number of real world
situations, to analyze behaviors, obtain improvements or eliminate problems.
Model building and evaluation is a not trivial task, since of the difficulty
involved in building a valid and simplified representation of complex systems
[Vangheluwe et al. 97].

<P>It is then fundamental to enable the simulation community to build models
with drastically reduced effort, by utilizing submodels available elsewhere.

<P>In software reuse, one of the most important activities to be performed
is the "acquisition, instantiation and/or modification of existing reusable
components", as detailed in the reuse activity model introduced in [Kang
et al. 92]. This paper focuses on the use of the Web technology to acquire,
instantiate and/or modify existing simulation models. The proposed environment
is called <I>WSE</I> (<I>Web-enabled Simulation Environment</I>) and combines
the Web technology to the use of Java and CORBA.

<P><I>Java</I> is an object-oriented, architecture-neutral programming
language that emphasizes the central ideas of simplicity, portability,
flexibility, dynamism and security [Sun 95]. A Java <I>applet</I> is a
simple Java program that can be embedded in a HTML document to provide
interactive, executable content on the Web. The applet code is obtained
by compiling the Java source code to a byte code interpretable by common
Web browsers.

<P>Simulation models implemented as Java applets can be posted to a Web
site so that any user with a Java-enabled Web browser can execute the model
[Buss et al. 96], according to the paradigm "write once, run everywhere".
In this paper Java is used to implement a uniform and easy-to-use graphical
user interface, which is run on a common Java-enabled Web browser, to enable
the user to perform retrieve, modify and save actions and to run the simulation
models of his/her interest.

<P><I>CORBA</I> specifies a standard for communication between objects
in a distributed client-server environment, that enables applications to
transparently access the services offered by local and remote objects,
by use of an object interface, independent of hardware and software implementations
[OMG 95]. The main feature of CORBA is that client objects may invoke services
of other objects on a distributed system without knowledge of their locations
and implementations. CORBA technology reduces development costs and design
complexity, since of the availability of simple mechanisms to access remote
services across multiple hardware and software platforms.

<P>Even though CORBA technology is mainly used to implement <I>distributed
simulation</I> [Schockle 97, Bachinsky et al. 94], in this paper CORBA
is used to transparently access model libraries, retrieve models and run
them by using the appropriate simulation tool.

<P>The integration of Java and CORBA combines the advantages of the Java
flexibility and architecture and of the CORBA communication and distribution
transparency. CORBA also provides the link between the Java applet running
on the Web browsers (the clients) and the necessary back-end services (the
WSE servers).

<P>The main benefits of basing the WSE on Java and CORBA can be summarized
as:
<UL>
<LI>
uniform and friendly user interface</LI>

<LI>
transparent access to simulation models and tools (location transparency,
distribution transparency and platform independence)</LI>

<LI>
dynamic acquisition, instantiation and/or modification of simulation models</LI>

<LI>
global availability (Internet-based interaction)</LI>

<LI>
plug-and-use architecture to easily embed simulation models and tools in
the WSE.</LI>
</UL>
The remainder of this paper is organized as follows. Section 2 introduces
the design concepts for the WSE, by illustrating the Java and CORBA-based
architecture. Section 3 presents the WSE prototype implemented at the University
of Roma "Tor Vergata", for reusing simulation models expressed in QNAP2,
a generic language for discrete-event simulation and evaluation of queueing
network models.

<P><B><FONT SIZE=+1>2. The WSE architecture</FONT></B>

<P>Figure 1 illustrates the architecture of the Web-enabled Simulation
Environment, composed of:
<UL>
<LI>
a Web Simulation Server</LI>

<LI>
the client application, implemented as a Java applet, that is downloaded
from the Web Simulation Server and run on a Java-enabled Web browser at
client sites</LI>

<LI>
a set of Simulation Tool (ST) servers, each running a specific simulation
tool (e.g. RESQ [Sauer et al. 82], QNAP2 [QNAP2 94], SIMPLE [Mohr 91],
SES/Workbench [SES 92], etc.)</LI>

<LI>
a set of Simulation Model (SM) servers, each server holding one or more
simulation models or submodels that require a specific ST to be run</LI>

<LI>
a CORBA-based infrastructure (IIOP Software Bus and Wrappers) to interface
the client application running on the Web browsers with ST and SM servers
over the Internet.</LI>
</UL>

<CENTER><IMG SRC="IMG00001.GIF" HEIGHT=450 WIDTH=760></CENTER>

<CENTER><BR>
Figure 1. The WSE architecture</CENTER>


<P>The Web Simulation Server is a conventional Web server, providing a
URL to download the Java applet that implements the client application.
The WSE users contact the Web Simulation Server, then download the client
application and finally run it at their site by use of a Java-enabled Web
browser.

<P>The following Sub-sections give details about the client application
and the modalities to interface the ST and SM servers into the WSE.

<P><B>2.1 The client application</B>

<P>The client application is downloaded from the Web Simulation Server,
and run by any Java-enabled Web browser (e.g. Netscape Navigator, Microsoft
Internet Explorer or Sun Hotjava).

<P>The client application provides a uniform and friendly user interface
to obtain services through the underlying CORBA infrastructure, in order
to:
<UL>
<LI>
find out which types of models or submodels SBMs exist elsewhere on the
Web</LI>

<LI>
retrieve the interesting SBM</LI>

<LI>
integrate the retrieved SBM into a SM at the local site</LI>

<LI>
save SM on the local filesystem (**)</LI>

<LI>
forward SM to the appropriate ST</LI>

<LI>
run SM on the ST server</LI>

<LI>
obtain and view the simulation results.</LI>
</UL>
Such services are partly performed at the client sites and partly at the
SM and ST server sites.

<P><B>2.2 The SM and ST servers</B>

<P>The SMs and STs software components can be seen as preexisting components
(legacy resources) to be encapsulated into the WSE.

<P>Encapsulation requires the use of techniques (called <I>wrapping techniques</I>)
to interface the legacy components with the access standards and conventions
established for the WSE. CORBA provides a standard object-oriented approach
for encapsulating legacy resources. When implemented according to the CORBA
standard, the wrappers are easily reusable to encapsulate new SMs and STs,
thus providing the WSE plug-and-use capability.

<P>The SM and ST wrappers are specified by use of the <I>CORBA</I> <I>IDL</I>
(<I>Interface Definition Language</I>), a declarative language which is
independent of the implementation language, the location, the hardware
and software architecture and the network technology. IDL provides both
basic and constructed data types and operations to define services offered
by the SM and ST wrappers. The interface declaration defines the construct
that holds data types and operations. IDL compilers are available to map
IDL into the most common programming languages (e.g. C, C++, Java).

<P>The IDL specified wrappers are then compiled into Java code, which is
split into the server segment (run at the SM/ST server site) and the client
segment (run at the client site).

<P>The <I>SM wrapper </I>consists of code to represent the SM as an object
encapsulating all data pertaining to the SM itself.

<P>The <I>ST wrapper</I> consists of code to represent the ST as an object
and to invoke the appropriate set of ST actions necessary to run the selected
SM.

<P>Transparent communication among the ST objects, the SM objects, and
the Web browser, is performed by the Software Bus illustrated in Figure
1. Such function is obtained by use of the CORBA <I>Object Request Broker
</I>(<I>ORB</I>), that operates control and data transfers between clients
and servers [OMG 95].

<P>In order to guarantee the interoperability between different ORB implementations
used at different sites, CORBA provides a <I>General Inter-ORB Protocol</I>
(<I>GIOP</I>), that specifies a set of message formats and common data
representations for communications between the ORBs. The <I>Internet Inter-ORB
Protocol</I> (<I>IIOP</I>), mentioned in Figure 1, specifies how GIOP messages
are exchanged over a TCP/IP network, thus making it possible to use the
Internet as a backbone through which various ORBs can communicate [Orfali
et al. 95].

<P>Next Section illustrates a WSE prototype for the integration into the
WSE of SM servers containing simulation models that require the QNAP2 simulation
tool [QNAP2 94], residing on a different ST site.

<P><B><FONT SIZE=+1>3. WSE prototype for the evaluation of Queueing Network
Systems</FONT></B>

<P>This Section illustrates the WSE prototype implemented at the University
of Roma "Tor Vergata", for the over-the-Internet simulation of queueing
network models expressed in the QNAP2 language. QNAP2 is a generic language
integrated in the Modline open environment for modeling and simulation
of discrete-events systems [Modline]. Details about queueing network systems
can be found in [Lavenberg 83] and [Lazowska et al. 84], while details
on QNAP2 can be found in [QNAP2 94].

<P>The QNAP2 models are text files containing QNAP2 commands for the description
and solution of queueing network models. An example QNAP2 model is illustrated
in Figure 2a, for a simple queueing network to study the CPU/DISK interaction
in a simple computer system as in Figure 2b.
<BR>&nbsp;
<CENTER><TABLE>
<TR>
<TD WIDTH="360"><TT><FONT SIZE=-1>/DECLARE/ QUEUE S, CPU, DISK;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>CLASS X, Y;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>/STATION/ NAME = S;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>TYPE = SOURCE;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>TRANSIT = CPU, X, 1, CPU, Y, 1;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>SERVICE = EXP(10.);</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>/STATION/ NAME = CPU;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>TRANSIT = DISK, X, 1, OUT, 3;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>SERVICE = EXP(1.);</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>/STATION/ NAME = DISK;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>TRANSIT = CPU;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>SERVICE = EXP(3.);</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>/CONTROL/ CLASS = CPU;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>MARGINAL = DISK, 3;</FONT></TT>&nbsp;
<BR><TT><FONT SIZE=-1>/EXEC/ SOLVE;</FONT></TT>&nbsp;
<BR><FONT SIZE=-1><TT>/END/</TT>&nbsp;</FONT>&nbsp;
<CENTER>(a)&nbsp;</CENTER>
</TD>

<TD WIDTH="427">
<CENTER><IMG SRC="IMG00002.GIF" HEIGHT=201 WIDTH=394></CENTER>

<CENTER>(b)&nbsp;</CENTER>
</TD>
</TR>
</TABLE></CENTER>

<CENTER><BR>
Figure 2. An example QNAP2 model</CENTER>


<P>It is assumed the QNAP2 model resides at site <I>i</I> and the QNAP2
tool at site <I>j</I> over the Internet, as in Figure 3 that also gives
details about components of the client application (<I>stubs</I>) and of
the server application (<I>skeletons</I>).<BR>
<BR>
<CENTER><IMG SRC="IMG00003.GIF" HEIGHT=290 WIDTH=512></CENTER>

<CENTER><BR>
Figure 3. CORBA infrastructure in the WSE</CENTER>


<P>Figure 4 illustrates the IDL specification of the SM and ST wrappers
to access QNAP2 services.

<P>The SM wrapper provides two operations:
<UL>
<LI>
<TT>getModelList()</TT>, to retrieve the list of SMs residing at the site
denoted as <TT>host_name</TT></LI>

<LI>
<TT>getModel()</TT>, to retrieve the specific SM denoted as <TT>model_name</TT>.</LI>
</UL>
The ST wrapper provides just one operation:
<UL>
<LI>
<TT>evaluate()</TT>, to run the SM denoted as <TT>ModelInfo</TT>, of type
<TT>infos</TT>, defined as a structure containing the SM name and its description.</LI>
</UL>

<CENTER><TABLE>
<TR>
<TD WIDTH="384">
<PRE><FONT SIZE=-1>// IDL specification for&nbsp;
<FONT FACE="Courier New">// QNAP2 SM and ST wrappers

module QNAP2 {
&nbsp;// constants
&nbsp;const long MAX_LENGTH_NAME = 20;
&nbsp;// types
&nbsp;struct infos {
&nbsp;&nbsp;&nbsp; string &lt; MAX_LENGTH_NAME > modelName;
&nbsp;&nbsp;&nbsp; string description;
&nbsp;&nbsp;&nbsp; };

&nbsp;interface SM {
&nbsp; //exceptions
&nbsp; exception No_Models {};
&nbsp; exception Model_NotFound {};
&nbsp; //operations
&nbsp; string getModelList(in string host_name,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; in string path)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; raises (No_Models)</FONT></FONT></PRE>
</TD>

<TD WIDTH="390">
<PRE><FONT SIZE=-1>&nbsp; infos getModel (
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; in string host_name, in string path,
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; in string model_name)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; raises&nbsp; (Model_NotFound);
&nbsp; }; // end of interface SM

&nbsp;interface ST {
&nbsp; // const
&nbsp; const long RESULTS_SIZE = 10000;
&nbsp; // types
&nbsp; typedef
&nbsp;&nbsp;&nbsp;&nbsp; string &lt; RESULTS_SIZE > results_report;
&nbsp; //exceptions
&nbsp; exception QNAP2_message {string mess};
&nbsp; //operations
&nbsp; results_report evaluate
&nbsp;&nbsp;&nbsp; (in infos modelInfo)
&nbsp;&nbsp;&nbsp; raises (QNAP2_message);
&nbsp; }; // end of interface ST
}; // end of QNAP2 module</FONT></PRE>
</TD>
</TR>
</TABLE></CENTER>

<CENTER><BR>
Figure 4. IDL specification of the SM and ST wrappers to access QNAP2 servers</CENTER>


<P>The IDL specification of Figure 4 is taken as input by an IDL-to-Java
compiler, that yields the client and server segments of the wrapper code,
respectively denoted as stub and skeleton. The final code is then obtained
by writing in Java the implementation of the operations provided by the
SM and ST wrappers. As illustrated in Figure 3, the stub is responsible
for collecting the requests sent by the Java applet implementing the client
application. The requests are then transferred from the client side to
the server side through the CORBA infrastructure, by using the IIOP. At
server side, the skeleton is responsible for gathering the requests and
activating the SM or ST server implementation which in turn is responsible
for elaborating the requests and give the results back to the client.

<P>Figure 5 illustrates an example user interface provided by the WSE prototype
for the integration of QNAP2 simulation tool, developed at the Laboratory
for Computer Science and the CERTIA Research Center of the University of
Roma "Tor Vergata" [D'Ambrogio et al. 97]. Figure 5a illustrates the set
of retrieved QNAP2 models and the description of the selected item, obtained
by pressing the <I>Description</I> button. By pressing the <I>Open</I>
button, the selected model is opened in a text area window, thus allowing
the user to modify the retrieved model, as illustrated in Figure 5b, where
the QNAP2 model of Figure 2a has been modified by introducing one more
DISK station. It is then possible to save the modified model on the local
filesystem (<I>Save</I> button) and finally run it (<I>Solve</I> button).
Figure 5c gives the solution obtained at the client site by evaluating
the modified QNAP2 model of Figure 5b.
<BR>&nbsp;
<CENTER><TABLE>
<TR>
<TD WIDTH="348">
<CENTER><IMG SRC="IMG00004.GIF" HEIGHT=199 WIDTH=332> (a)&nbsp;<IMG SRC="IMG00005.GIF" HEIGHT=219 WIDTH=328>
(b)&nbsp;</CENTER>
</TD>

<TD WIDTH="464">
<CENTER><IMG SRC="IMG00006.GIF" HEIGHT=453 WIDTH=445> (c)&nbsp;</CENTER>
</TD>
</TR>
</TABLE></CENTER>

<CENTER><BR>
Figure 5. Example user interface windows for the WSE prototype</CENTER>


<P><B><FONT SIZE=+1>4. Conclusions</FONT></B>

<P>A Web-based Environment for the reuse of simulation models has been
introduced. It provides uniform and transparent access to simulation models
and tools, by use of the Java programming language and the CORBA standard.

<P>A prototype for the over-the-Internet simulation of models expressed
in the QNAP language has been illustrated.

<P>The main benefits provided by the environment can be synthesized into:
uniform and friendly user interface, transparent access to simulation models
and tools (location transparency, distribution transparency and platform
independence), dynamic acquisition, instantiation and/or modification of
simulation models, global availability (Internet-based interaction) and
plug-and-use architecture to easily embed simulation models and tools in
the WSE.

<P><B><FONT SIZE=+1>5. References</FONT></B>

<P>[Bachinsky et al. 94] Bachinsky S.T., Bailey T.A., Gokhman B, Hodum
F.J., TACTICS: An Infrastructure for Integrating High Fidelity Simulations,
<I>Military, Government and Aerospace Simulation</I>, Vol. 26, N. 4, The
Society for Computer Simulation, San Diego, CA, April 10-15, 1994.

<P>[Buss et al. 96] Buss A.H., Stork K.A., Discrete Event Simulation on
the World Wide Web using Java, <I>Proceedings of the 1996 Winter Simulation
Conference</I>, Coronado, CA, December 1996.

<P>[D'Ambrogio et al. 97] D'Ambrogio A., Iazeolla G., Paoli S., A Prototype
for Software Reuse over the Internet, Technical Report RI.97.03, Laboratory
for Computer Science, University of Rome "Tor Vergata", May 1997.

<P>[Kang et al. 92] Kang K.C., Cohen S., Holibaugh R., Perry J., Peterson
A.S., A Reuse-based Software Development Methodology, Special Report, CMU/SEI-92-SR-4,
Software Engineering Institute, Carnegie Mellon University, Pittsburgh,
PA, January 1992.

<P>[Lavenberg 83] Lavenberg S.S., Computer Performance Modeling Handbook,
Academic Press, 1983.

<P>[Lazowska et al. 84] Lazowska E.D., Zahorjian J.L., Graham G.S., Sevcick
K.C., Quantitative System Performance: Computer Systems Analysis using
Queueing Network Models, Prentice Hall, 1984.

<P>[Modline] Simulog, Modline Software Package. Available at: &lt;http://www.simulog.fr/US/html/
prods/modline.html>.

<P>[Mohr 91] Mohr B., SIMPLE: a Performance Evaluation Tool Environment
for Parallel and Distributed Systems, in <I>Distributed Memory Computing,
2nd European Conference</I>, <I>EDMCC2,</I> (A. Bode ed.), Munich, Germany,
pp. 80-89, Springer, Berlin, LNCS 487, April 1991.

<P>[OMG 95] Object Management Group, The Common Object Request Broker:
Architecture and Specification (Revision 2.0), July 1995.

<P>[Orfali et al 95] Orfali R., Harkey D., Edwards J., The Essential Distributed
Objects Survival Guide, John Wiley &amp; Sons, September 1995.

<P>[QNAP2 94] Simulog. QNAP2 User's Guide, June 1994.

<P>[Sauer et al. 82] Sauer C.H., MacNair E., Kurose J.F., Computer/communication
systems modeling with research queueing package version 2, Research Report,
IBM Thomas J. Watson Research Center, Yorktown Heights, NY, (1982).

<P>[Schockle 97] Distributed Simulation Software for Complex Continuous
System, <I>Special Issue in Object-Oriented Programming</I>, Max Muhlhauser
ed., d.punkt Verlag, Heidelberg, Germany, March 1997.

<P>[SES 92] Scientific and Engineering Software Inc., SES/Workbench User
Manual, 1992.

<P>[Sun 95] Sun Microsystems, The Java Language Environment - A White Paper,
October 1995.

<P>[Vangheluwe et al. 97] Vangheluwe H., Vansteenkiste G., and Reddy R.,
Simformatics: Meaningful Model Storage, Re-use, Exchange, and Simulation,
<I>Proceedings of the Summer Computer Simulation Conference</I>, July 1997,
Arlington, Virginia, pp. 936-41, ed. Mohammad S. Obaidat and John Illgen.
San Diego, CA: Soc. for Computer Simulation International, 1997.

<P>
<HR WIDTH="100%"><FONT SIZE=-1>(*) Work partially supported by the University
of Roma at "Tor Vergata" CERTIA Research Center Project on Multimedia and
Collaborative Technology, the MURST Projects on Performability in Software
Engineering, and Performance of Client/Server Systems, and the CNR Project
on Performance and Reliability Engineering of Distributed Databases.</FONT>
<BR><FONT SIZE=-1>(**) This operation is only possible if allowed by the
Java-enabled Web Browser, due to the security restrictions applied to applets
downloaded over the Internet</FONT>
</BODY>
</HTML>
