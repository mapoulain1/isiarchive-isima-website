<HEAD>
<TITLE>Survivability Simulator for Multi-Agent Adaptive Coordination<A NAME=tex2html1 HREF=article.foot.html#65><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/foot_motif.gif"></A>
<A NAME=tex2html2 HREF=article.foot.html#15><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/foot_motif.gif"></A>
</TITLE>
</HEAD>
<BODY><P>
 <H1>Survivability Simulator for Multi-Agent Adaptive Coordination<A NAME=tex2html1 HREF=article.foot.html#65><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/foot_motif.gif"></A>
<A NAME=tex2html2 HREF=article.foot.html#15><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/foot_motif.gif"></A>
</H1>
<CENTER>
<P><b><A HREF="http://www.cs.umass.edu/~vincent">R&#233;gis Vincent</A>, <A
HREF="http://www.cs.umass.edu/~bhorling">Bryan Horling</A>, <A
HREF="http://www.cs.umass.edu/~wagner">Tom Wagner</A> and <A
HREF="http://dis.cs.umass.edu/">Victor Lesser</A> <BR>University of
Massachusetts <BR>Computer Sciences Department <BR>Multi-Agent Laboratory
<BR>Phone: 413-545-0675, Fax: 413-545-1249<BR>Email: {<A
HREF="mailto:vincent@cs.umass.edu">vincent</A>,<A
HREF="mailto:bhorling@cs.umass.edu">bhorling</A>,<A
HREF="mailto:wagner@cs.umass.edu">wagner</A>,<A
HREF="mailto:lesser@cs.umass.edu">lesser</A>}@cs.umass.edu<BR>Web: <A
HREF="http://dis.cs.umass.edu">http://dis.cs.umass.edu/</A></b><P>
</CENTER>
<P><b></b><P>
<H3>Abstract:</H3>
<i><b>Keywords: Multi-agent systems, discrete simulation, simulator </b>
<P>
The growth of a distributed processing system can increase both the
number and likelihood of attacks it may be subject to over its lifetime
<A HREF=article.html#internetworm88>[7]</A><A HREF=article.html#grids>[14]</A>.  This fact, in addition to the
complexity inherent in such an environment, makes the survivability of
large heterogeneous systems one of the most challenging research areas
currently being investigated<A HREF=article.html#darpaweb>[1]</A>.  Our goal is to create a
distributed simulation system to test various coordination mechanisms
allowing the elements of the system to detect, react and adapt in the
face of adverse working conditions.  Our assumption is that the system
is composed of a group of autonomous agents.  Each agent has its own
local view of the world and its own goals, but is capable of
coordinating these goals with respect to remote agents.  
To simulate these complex systems, an environment is needed which
permits the simulation of an agent's method execution.  To this end,
we are developing a distributed event-based simulator capable of
simulating the effects directed attacks or a capricious environment 
have on agent method execution and recovery.
</i><P>
<P>
<H1><A NAME=SECTION0001000000000000000> Challenge</A></H1>
<P>
<A NAME=challenge><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/invis_anchor.xbm"></A>
<P>
With the advent of open computing environments, widely networked 
information resources, and an equally widely networked client base,
applications are evolving from highly centralized structures to  
distributed decentralized structures.  Distribution is required to cope 
with the complexity of the tasks, to distribute the load, and to utilize the 
necessarily distributed resources or expertise used by these applications.  
In this setting, application interactions are also evolving and often a 
distributed application is actually a collection of separate applications, 
each possibly having other tasks outside of the given application and possibly
under the management or ownership of different controlling entities.  With
increasing frequency, an agent computing paradigm is used to control these 
application components where each contributing application is an agent, 
having its own goals, functionality, and perhaps special computing resources.  
The agents interact via higher-level interfaces, exchanging high-level 
information such as goals and partial plans, rather than simply invoking 
each other's low-level methods via remote-function-call style interfaces.  
The agent-style control methodology lends itself to this integration task
because it enables designers to explicitly represent agent goals, where
agents may have <i>multiple</i> goals and goals for <i>different</i> client 
applications, and to reason about the benefits or relative importance of 
these goals from a self-interested perspective 
<A HREF=article.html#rosenschein89>[12]</A><A HREF=article.html#perrow86>[9]</A><A HREF=article.html#moe84>[8]</A>, a wholly cooperative 
perspective <A HREF=article.html#decker95a>[5]</A><A HREF=article.html#durfee89b>[6]</A>, or a market-driven economic 
perspective <A HREF=article.html#sandholm>[13]</A>.
<P>
While this highly networked and distributed environment is 
giving rise to the next generation of powerful applications, 
the same decentralized networked characteristics that serve as the growth 
catalyst also create the greatest hazard to these applications, that is the 
vulnerability of these applications to attack from outside sources 
<A HREF=article.html#internetworm88>[7]</A><A HREF=article.html#grids>[14]</A>.  Applications that are open, or built from 
individual components that are themselves open systems, are susceptible to 
virus style attacks and to attacks that disable the network, block 
communication, or disable member applications.  Attacks may be deliberate, 
i.e., the action of an adversary, or they may simply be system failures that 
affect the application like a deliberate attack by adversely affecting the 
application.  Attacks may also be more covert in nature, involving 
misinformation or the confusion of communications or member applications.  
To address attacks, deliberate or otherwise, the evolving heterogeneous 
distributed applications must also be <i>flexible</i>; they must be 
able to cope with a component application going down or the loss of 
communications between various components.  The distributed system 
architecture must adapt dynamically, perhaps finding different 
routings for communications or other component applications to fulfill a 
particular set of needs.
<P>
Our research focus is on building such survivable, dynamic, heterogeneous 
distributed systems.  We use an agent coordination control scheme, called 
<i>generalized-partial-global-planning</i> (GPGP) <A HREF=article.html#decker94f>[2]</A>, to organize 
the agent members of a distributed application.  In GPGP, agents exchange
their local views of the group problem solving context and negotiate
to determine which agent is to perform which task(s) at what time(s).
Individual agents are comprised of three primary modules: 1) the GPGP 
coordination module, which is responsible for interacting with 
other agents and maintaining a cohesive distributed application; 2) the local 
real-time scheduler <A HREF=article.html#wagner97c>[15]</A><A HREF=article.html#wagner97e2>[16]</A> that performs trade-off 
analysis of different
possible courses of action and decides, in conjunction with the GPGP module, 
which actions to perform and at what times; 3) the domain problem solver
that performs the domain (application) actions.  Thus agents consist of two
main control problem solving components, the scheduler and the coordination
module, and one main domain problem solving component.  Agents represent and
reason about problem solving processes using the T&#198;MS task modeling
framework  <A HREF=article.html#decker93f>[4]</A><A HREF=article.html#decker94b>[3]</A>.  In T&#198;MStasks are hierarchically 
decomposed into
subtasks and finally into primitive actions.  T&#198;MS differs from most
traditional modeling frameworks in that it represents both hard and soft
interactions between tasks, e.g., enabling or facilitating relationships,
and alternative ways to perform tasks.  Part of the agent control scheme 
is to reason about inter-agent and intra-agent task interactions and to
reason about the different ways to perform tasks, and the different quality, 
cost, and duration trade-offs of each alternative way.  For example, for the 
task of gathering information on a competitor cooperation, the agents may be 
able to achieve the task in short order and little cost, but may sacrifice 
quality to do so.  However, given more time and money, the agents may be able 
to explore more information resources and produce a higher-quality report.
<P>
One major thrust of our current work is on studying and improving the 
flexibility and adaptability aspects of our agent coordination mechanisms.
Recent work in learning coordination mechanisms 
<A HREF=article.html#ijcai97nagui>[11]</A><A HREF=article.html#aaai97nagui>[10]</A> has shown that it is more effective to 
learn situation specific coordination strategies rather than using a single 
strategy for all situations.  The work on flexibility and adaptability is
related in that a situation specific learning component is needed, perhaps
in conjunction with approximate expert knowledge, to learn when and how to
adapt to attacks.  Accordingly, we must simulate these complex distributed 
agent-based 
heterogeneous systems so that we can study the mechanisms in a controlled
fashion.  We are developing a distributed event-based simulator to simulate
agent action execution in the face of system attacks and recovery.  The 
simulator is a single centralized artifact which handles the T&#198;MS 
action execution for all agents in the system.  The agents are decoupled, 
running as external processes, as in a real application; we will discuss
the complexities of this decoupling in the next section.  In addition
to action execution, all agent communication is also routed through the
simulator.  Thus, attacks can change or modify action performance, e.g., 
causing failure, poor results, or changing resource consumption
characteristics, or attacks may change communication characteristics, 
causing interference, blockages, delays, and lost or
garbled messages.  From an architectural perspective, the simulator
replaces the component of the agent responsible for actual method execution
and the coupling is transparent to the other agent control components.
Similarly, the communications component is modified so that the simulator
controlled routing is transparent.  This action/communication coupling
between the simulator and the agents enables the simulator to affect system
execution performance in a reactive sense.  However, to experiment with
distributed application prototypes, or to study distributed applications
for which the domain problem solving component has yet to be implemented,
the simulator must also be able to interact with the agents in a proactive
sense.  The distinction lies in where the agent plans originate.  If the
agent plans are generated by a fully implemented domain problem solver,
then the simulator's role is limited to modulation of action execution and 
agent communications.  However, if the plans are generated by the simulator
and seeded to the respective agents, the simulator can not only control
action execution and communications, but it can control the high-level
objectives of one or more agent's in the system.  This functionality is
implemented by replacing the agent's domain problem solving component with a
stub that receives T&#198;MS task structures from the simulator.  Thus the agent
/simulator coupling is at the action, communication, and, optionally,
plan-generation levels.
<P>
We will return to these issues in the subsequent sections.  
Section <A HREF=article.html#simulatordesign>2</A> describes the software design of our simulator
and the different attacks and the measures taken by the simulator.  
Section <A HREF=article.html#preliminaryimplementation>3</A> provides our current implementation
status  
and describes the features of Java that facilitate this work.  
Section <A HREF=article.html#futuresdirections>4</A> defines the next phase of our development and
our planned future work.
<P>
<H1><A NAME=SECTION0002000000000000000> Simulator Design</A></H1>
<P>
<A NAME=simulatordesign><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/invis_anchor.xbm"></A>
<P>
The simulator is designed as a central process; all agents involved
with the model are connected to the simulator using sockets.  The
agents themselves are independent processes, which could run on
physically different machines.  Time synchronization is controlled by
the simulator, which periodically sends a pulse to each of its remote
agents.  Each agent has a local <i>manager</i> which converts the
simulation pulse into real CPU time.  The <i>manager</i> controls
process and records the time spent scheduling, planning, or
executing methods.  Note, the simulator does not control the
agents' activities, it merely allocates time slices and records
the activities performed by the agent during the time slide via
an instrumentation interface.
Once the alloted CPU time has been used, the <i>manager</i> halts the agent and sends an acknowledgment message back to
the simulator.  The simulator waits for all acknowledgments to
arrive before sending a new pulse (so all process are synchronized). 
Using this method, any functions (even computationally long ones)
can be executed using whatever pulse-granularity is desired, i.e.,
if we wish to simulate an agent environment where all planning actions 
complete in a single time pulse, the pulse-granularity is defined by
the longest running planning action.  
For example, a planning phase may explore a very large
space search, but the simulated time required may be just one pulse. 
Since all remote processes are frozen until the planning is completed,
this gives the illusion that the planning was actually performed very
quickly.  Each agent therefore has its own task-specific time
transformation function, which it uses to convert each simulator pulse
into local CPU time.
However, the clock mechanism works at the other end of the spectrum
too.  If we wish to study actual planning times, where slower planners 
take longer, or agents executing on slower machines take longer, then the 
pulse-granularity is smaller and a tick may correspond to a second in 
real-time or cpu-time.
<P>
The simulator is also a message router, in that all agent
communications will pass through it.  This scheme permits explicit
control over network and communication delays.  In this way, if we want
to simulate a very fast communication path, the simulator may
immediately re-send a message to its destination, but if we want to
simulate a compromised network, the simulator may wait <i>n</i> pulses
before sending the message.  This method also allows an agent to
broadcast a message to all other agents without explicitly knowing the
number of agents that will receive the message.
<P>
The simulator behavior is directed by a queue containing a
time-ordered list of events.  Each message it receives either adds or
removes events from the queue.  At each pulse the simulator selects
the correct events and realizes their effects (for example, a network
may slow down).  Only after the effect of each event has been completely
determined is the timing pulse sent to each agent.
Primitive actions in T&#198;MScalled <i>methods</i> are characterized
statistically via discrete probably distributions in three dimensions,
quality, cost, and duration.  Agents reason about these characteristics
when deciding which actions to perform and at what time.
<P>
When a agent wants to execute a method, it sends a message to the simulator
with the method name. The simulator then retrieves the method from the <i>objective</i> T&#198;MS database. 
Agents schedule, plan, and interact using a <i>subjective</i> view of
the methods.  The subjective view differs from the objective view because
agent's may have an imperfect model of what will actually happen,
performance-wise, when the method is executed.  For example, for a method
that involves retrieving data from a remote site via the WWW, the remote
site's performance characteristics may have changed since the agent learned
them and thus the agent's view of the execution behavior of that method,
namely its duration, is imperfect.  In a simulation environment, both the
subjective and the objective method views are created by the simulator /
task generator and the objective, or true, views of the methods are stored
in the simulator's T&#198;MS database.  Thus when an execution message arrives,
the simulator must obtain the objective view of the method before any
other steps may be taken.
The first step of the simulator is to calculate the value of the cost,
duration and quality that will be ``produced'' by this execution.
The duration (in pulse time) is used to create an event that sends to the agent
the result in terms of cost and quality. 
Any event realized before the newly queued event is performed may
change the results of the newly queued event's ``execution.''  For example, 
a network breakdown event at the front of the queue may increase the time of 
all the simulated executions, that follow it in the queue, by 100%.  Thus
subsequent method completion events are delayed.  
<IMG ALIGN=BOTTOM ALT="" SRC="simulator-details.gif">
This interaction effect
is also possible because of the interactions between the methods themselves.
For example, if one method <i>enables</i> another method, and the first method
fails, then the other method may no longer be executed or executing the 
method will produce no result.  If both methods are already ``scheduled''
in the event queue, and the first method fails, then the event associated
with the second method's execution must be changed.
<P>
The random generator used to calculate the cost, duration and quality values
is seeded by a fixed parameter or by a random number (depending of the
current time). The solution of seeding by a fixed parameter is used to have a
deterministic simulation. Our random generator produces the same answer if 
the same seeding value is used. The goal is to compare different agent
coordination 
mechanisms on the same problem using the same simulation.
To test a particular coordination mechanism on several problems, we use a
seeding based on the current time which guarantees that two simulations do not
produce the same solution.
<P>
The next section will describe our preliminary implementation in Java, and
will describe in details the synchronization problem.
<P>
<H1><A NAME=SECTION0003000000000000000> Preliminary Implementation</A></H1>
<P>
<A NAME=preliminaryimplementation><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/invis_anchor.xbm"></A>
<P>
	As noted above, the simulation environment was designed to be
both distributed and platform independent, in order to provide the
most amount of ability and flexibility when modeling complex
interactions among heterogeneous agents.  Java was therefore the
language of choice for its implementation, as it offers robust
communication primitives and bytecode interpreters on a wide range of
platforms, among other benefits.  The 1.1 API was used to create the
environment, which consists of a single ``server'' process, called the
<em>simulator</em>, and one or more ``client'' processes, which serve as
<em>managers</em> to the agents executing within the system.
<P>
	Each manager has under its control an external agent process,
which at this time is a black box written in an arbitrary language. 
The manager serves as an interface between the agent and the
simulation environment; it is responsible for process control, message
passing and event delivery.  When a manager is started, it is given
both the address of the simulation server and the name of the agent
which is to be under its control.  The manager then spawns a child
process (which will become the agent process) and attempts to contact
the simulation server, after which it idles until the actual
simulation process is initiated.  During simulation, the manager waits
for a clock pulse to be delivered by the simulator, which indicates
the start of a time slot.  Upon receipt of the pulse, the manager
converts it into its own local time scale by waking the agent process
and permitting it to run for a locally specified amount of time.  In
this way, various processor or code optimizations can be modeled by
simply letting the agent run for a different lengths of time.  When
the manager has determined that the agent's time slot has been used
up, the agent process is halted and a pulse acknowledgment is returned
to the simulator.  Any events or messages generated by the agent are
sent to the simulator before acknowledging the pulse, so that they
will be recognized as originating during that particular time slot.
<P>
	For our implementation, native methods were needed to both
execute and control the agent, as the process paradigm offered in the
Java API does not offer the low level controls required to manage an
external process.  While this affects the portability of the code, the
relatively small number of native methods provide the important
ability to make use of previously written agents and also permit a
much more diverse agent pool.  A simple Java-based threaded agent stub
has also been written, which can be used on platforms where process
control is not easily available.  A second artifact of this approach
is that the process monitoring is performed in user space and thus is
dependent on the exact preemption time of the agent and manager
processes.  Because of this, fine grained timing control is not
possible, although for our purposes, this is an acceptable tradeoff of
complexity, since pulse conversions are typically measured in seconds.
<P>
	The simulator functions as the central hub of the simulation
environment, being responsible for agent synchronization, event
simulation and modification, message routing and time management. 
When the simulator starts up, it initializes an event queue, a local
clock, and a communications server which waits for manager initiated
connections.  As each agent joins, it is represented locally at the
simulator by both agent specification and communications objects.  It
is through these instances that the simulator communicates with and
controls the remote agents.  All communications at this time are done
with TCP based character streams.  During the simulation, agents can
join or leave at any time, but typically the server will wait for some
number of agents to connect and then begin the simulation process,
after which the agent pool remains constant.  The simulation time line
is discrete, in that all events and agent execution periods fall
completely within a specified time slot.  Activity during each of
these slots begins with the simulator first waiting for all remote
agents to complete their activity (as indicated by the pulse
acknowledgment), then incrementing the clock and sending a new pulse
to each agent.  This synchronization step is crucial to the validation
of the distributed nature of the simulation environment.  Without it,
there would be no clear notion of when events take place, leading to
race conditions amongst the agents and the simulated or actual events. 
Once the agents have been notified, all events scheduled to terminate
during that time period are retrieved and realized, after which the
time slot is considered completed.  At this time, events elsewhere in
the queue may be modified to simulate resource or behavior changes. 
For instance, a message delivery may be delayed to mimic a faulty
network, or the results of a simulated computation may be altered by a
theoretical adversary.  The simulation time line then progresses in this
manner, with the agent and simulator functioning essentially in
lockstep, until the simulator is paused or terminated.
<P>
<H1><A NAME=SECTION0004000000000000000> Future Directions</A></H1>
<P>
<A NAME=futuresdirections><IMG ALIGN=MIDDLE SRC="http://www.cs.umass.edu/nikos/figs/invis_anchor.xbm"></A>
<P>
As we have just completed a prototype of the simulation environment, 
much work remains to be done.  We anticipate the following features to be
integrated into the system in the near future:
<P>
<UL>
<P>
<LI> Problem and attack generators to create, direct and modify 
the agents' goals.  A related feature is the ability to quantify 
the survivability test-level and the degree of adaptation required of
the agent coordination mechanisms.
<P>
<LI> ``Real'' agents, capable of working on a given task structure and
communication with other peer agents.
<P>
<LI> Agent communication interception and routing.
<P>
<LI> Communication implementation using serialization, CORBA or Java RMI.
</UL>
<P>
The main advantage of our simulator will be:
<P>
<UL>
	<LI> Platform independent distributed implementation.
	<LI> Domain independent architecture.
	<LI> Deterministic behavior on the part of the simulation
environment.  If the agents themselves are deterministic, two
executions will produce the same event stream and results. 
	<LI> Attacks are clearly defined as events, permitting the
modeling of complex coordinated attacks.
</UL>
<P>
Our goal is to construct a multi-agent simulator based on an
object-oriented model in Java.  The simulator is currently in an early
stage of development.  Our design's main advantage in using Java are
robust network capabilities and platform independent implementation.
We expect that a preliminary version will be available by January 1998.
<P>
<P><H2>References</H2><P>
<DL COMPACT>
<DT><A NAME=darpaweb><B>1</B></A><DD>
DARPA.
http://www.ito.darpa.mil/ResearchAreas/Information_Survivability.html,
  1996.
<P>
<DT><A NAME=decker94f><B>2</B></A><DD>
Keith S. Decker.
<em>Environment Centered Analysis and Design of Coordination
  Mechanisms</em>.
PhD thesis, University of Massachusetts, 1995.
<P>
<DT><A NAME=decker94b><B>3</B></A><DD>
Keith S. Decker.
T&#198;MS: A framework for analysis and design of coordination
  mechanisms.
In G. O'Hare and N. Jennings, editors, <em>Foundations of
  Distributed Artificial Intelligence</em>, chapter 16. Wiley Inter-Science, 1995.
<P>
<DT><A NAME=decker93f><B>4</B></A><DD>
Keith S. Decker and Victor R. Lesser.
Quantitative modeling of complex environments.
<em>International Journal of Intelligent Systems in Accounting,
  Finance, and Management</em>, 2(4):215-234, December 1993.
Special issue on ``Mathematical and Computational Models of
  Organizations: Models and Characteristics of Agent Behavior''.
<P>
<DT><A NAME=decker95a><B>5</B></A><DD>
Keith S. Decker and Victor R. Lesser.
Designing a family of coordination algorithms.
In <em>Proceedings of the First International Conference on
  Multi-Agent Systems</em>, pages 73-80, San Francisco, June 1995. AAAI Press.
Longer version available as UMass CS-TR 94-14.
<P>
<DT><A NAME=durfee89b><B>6</B></A><DD>
E. H. Durfee, V. R. Lesser, and D. D. Corkill.
Cooperative distributed problem solving.
In A. B. Barr, P. Cohen, and E. Feigenbaum, editors, <em>The
  Handbook of Artificial Intelligence</em>, volume 4, pages 83-147. Addison
  Wesley, 1989.
<P>
<DT><A NAME=internetworm88><B>7</B></A><DD>
M. Eichin and J. Rochis.
With microscope and tweezers: An analysis of the internet worm of
  november 1988.
In <em>IEEE symposium on Research in Securiry and Privacy</em>, 1989.
<P>
<DT><A NAME=moe84><B>8</B></A><DD>
Terry M. Moe.
The new economics of organization.
<em>American Journal of Political Science</em>, 28(4):739-777, November
  1984.
<P>
<DT><A NAME=perrow86><B>9</B></A><DD>
Charles Perrow.
<em>Complex Organizations</em>.
Random House, New York, 1986.
<P>
<DT><A NAME=aaai97nagui><B>10</B></A><DD>
M. V. Nagendra Prasad and Victor R. Lesser.
Learning problem solving control in cooperative multi-agent systems.
In <em>Workshop on Multi-Agent Learning, AAAI-97</em>, Providence, Rhode
  Island, 1997.
<P>
<DT><A NAME=ijcai97nagui><B>11</B></A><DD>
M. V. Nagendra Prasad and Victor R. Lesser.
The use of meta-level information in learning situation_specific
  coordination.
In <em>IJCAI-97</em>, Nagoya (Japan), 1997.
<P>
<DT><A NAME=rosenschein89><B>12</B></A><DD>
J. S. Rosenschein and J. S. Breese.
Communication-free interactions among rational agents: A
  probabilistic approach.
In L. Gasser and M. N. Huhns, editors, <em>Distributed Artificial
  Intelligence, Vol. II</em>. Pitman Publishing Ltd., 1989.
<P>
<DT><A NAME=sandholm><B>13</B></A><DD>
Tuomas Sandholm and Victor Lesser.
Issues in automated negotiation and electronic commerce: Extending
  the contract net framework.
In <em>Proceedings of the First International Conference on
  Multi-Agent Systems (ICMAS-95)</em>, pages 328-335, 1995.
<P>
<DT><A NAME=grids><B>14</B></A><DD>
S. Staniford-Chen, S. Cheung, R. Crawford, M. Dilger, J. Frank, Hoagland J.,
  K. Levitt, C. Wee, R. Yip, and D. Zerkle.
Grids-a graph based intrusion detection system for large networks.
Technical report, NISSC, 1996.
http://seclab.cs.ucdavis.edu/arpa/grids/welcome.html.
<P>
<DT><A NAME=wagner97c><B>15</B></A><DD>
Thomas Wagner, Alan Garvey, and Victor Lesser.
Complex Goal Criteria and Its Application in Design-to-Criteria
  Scheduling.
In <em>Proceedings of the Fourteenth National Conference on
  Artificial Intelligence</em>, July 1997.
Also available as UMASS Department of Computer Science Technical
  Report TR-1997-10.
<P>
<DT><A NAME=wagner97e2><B>16</B></A><DD>
Thomas Wagner, Alan Garvey, and Victor Lesser.
Criteria-Directed Heuristic Task Scheduling.
<em>International Journal of Approximate Reasoning, Special Issue on
  Scheduling</em>, To appear 1997.
Also available as UMASS Department of Computer Science Technical
  Report TR-97-16.
</DL>
<P>
<H1><A NAME=SECTION0006000000000000000>  <em>About this document ...</em> </A></H1>
<P>
 For more information, please visit our Web site <A HREF="http://dis.cs.umass.edu"> http://dis.cs.umass.edu </A><HR>

</BODY>
<P><ADDRESS>
Regis Vincent
</ADDRESS>
