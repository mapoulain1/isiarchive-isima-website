<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<!--Converted with LaTeX2HTML 97.1 (release) (July 13th, 1997)
 by Nikos Drakos (nikos@cbl.leeds.ac.uk), CBLU, University of Leeds
* revised and updated by:  Marcus Hennecke, Ross Moore, Herb Swan
* with significant contributions from:
  Jens Lippman, Marek Rouchal, Martin Wilck and others -->
<HTML>
<HEAD>
<TITLE>FACTORY SIMULATION ACROSS THE WORLD WIDE WEB USING JAVA</TITLE>
<META NAME="description" CONTENT="FACTORY SIMULATION ACROSS THE WORLD WIDE WEB USING JAVA">
<META NAME="keywords" CONTENT="wilcoxpa">
<META NAME="resource-type" CONTENT="document">
<META NAME="distribution" CONTENT="global">
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=iso_8859_1">
</HEAD>
<BODY >
<!--Navigation Panel-->
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next" SRC="next_motif_gr.gif"> 
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up" SRC="up_motif_gr.gif"> 
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous" SRC="previous_motif_gr.gif">   
<BR>
<BR>
<BR>
<!--End of Navigation Panel-->
<P><H1 ALIGN="CENTER">FACTORY SIMULATION ACROSS THE WORLD WIDE WEB USING JAVA</H1>
<P ALIGN="CENTER"><STRONG>Pauline Wilcox and Rob Pooley
<BR>
Department of Computer
Science
<BR>
University of Edinburgh
<BR>
Kings Buildings, Edinburgh EH9
3JZ
<BR>
United Kingdom
<BR>
rjp or paw@dcs.ed.ac.uk</STRONG></P>
<P ALIGN="CENTER"><STRONG></STRONG></P>
<P ALIGN="LEFT"></P>
<P>
<H3 CLASS="ABSTRACT">Abstract:</H3>
<P CLASS="ABSTRACT">This project is concerned with the development of a distributed,
        multi-user Internet based simulation environment for automated
        factories, written in Java.  The motivating factor
        behind the work described is the rapid development in the field of
        computer controlled automated production lines, which has led to
        increasing complexity of scheduling of these systems. To avoid the
costs in
        moving expert modellers and Operational Research staff to factory
        sites, a means for working in a geographically distributed manner
is desirable.
        The environment described here provides that. Its benefits are shown
        through a case study and ideas for future work are explored.
</P>
<P><H1><A NAME="SECTION00010000000000000000">
MOTIVATION</A>
</H1>
<P>
Simulation has been shown to be a successful means of expressing and
investigating problems in a wide range of fields, and in light of the
developments noted above, the interest in simulations of automated
factories has grown. In particular the graphical presentation of
a simulation model has advantages over a more basic trace output,
since the results are immediately more expressive to those viewing the
simulation.
<P>
The construction of simulation models remains a task requiring a large
degree of skill and specialist knowledge.  It is therefore not
unreasonable to assume the involvement of a team of simulation and
modelling analysts.  Since few companies will have a team of analysts
available on every site where they have factories, a team will usually
need to be brought in either from the Operational Research Department,
in a large organisation, or from an outside consultancy, in the case
of most small firms. The time of such experts is very valuable and
wasting it in travelling to and from the site is a considerable
overhead.
<P>
There is no shortage of examples of simulation packages suitable for
modelling factory operations.  What distinguishes the work presented
in this paper from these is the incorporation of simulation within a
system that allows remote interaction of interested parties.  Internet
technology, through the Java language and the World Wide Web,
provides a simulation system where users and analysts explore
issues remotely, thereby avoiding the need for unnecessary travel and
reducing costs, and hopefully time taken to reach a solution.
<P><H1><A NAME="SECTION00020000000000000000">
PROJECT AIMS</A>
</H1>
<P>
The principal aim of the project can be summarised as
producing a system that would allow multiple users to interact remotely and
solve issues relating to automated factories. The system is
object-oriented in design, and implemented using the Java
language, leading to a system that can be accessed via the
Internet. The issues concerning the factory floor are expressed
using a graphical simulation system, supporting animation.
<P>
Work began by producing a
prototype, to illustrate the ideas and
functionality, though perhaps not in the final polished and robust
form that would be preferred in the final system. The use of object
oriented techniques allowed easy incorporation of existing work in
simulation using Java, notably the <EM>simjava</EM> package, produced by Fred
Howell at Edinburgh (Howell 1996).
<P><H1><A NAME="SECTION00030000000000000000">
APPROACH TO DESIGN AND IMPLEMENTATION</A>
</H1>
<P>
The approach to design and implementation was to consider the system
split into three components :
<DL>
<DT><STRONG>Architecture</STRONG>
<DD>which describes how the various modules would be
connected and configured; a client/server architecture was used,
since this is the one best supported by the Java view of interaction;
<DT><STRONG>Communication/Dialogue</STRONG>
<DD>which describes how users of the system
interact to cooperate in reaching a decision;
<DT><STRONG>Simulation</STRONG>
<DD>which defines how the simulation models in the
system are created and run.
</DL>
<P>
A survey of successful software on the Internet identified a common
`game' theme amongst the majority of Java programs there.  This
analogy of viewing the system as a multi-user game fitted the project
well.  An important part of the initial design focussed on
identification of the valid player moves and describing sample
scenarios to see how the game would react/unfold.
<P>
The desired functionality of the proposed system was identified as follows:
<P><DL COMPACT>
<DT>1.
<DD>allow users to load and view existing simulations, achieving a
consistent view at different locations;
<DT>2.
<DD>allow interaction with simulation through controlled execution
and editing facilities;
<P><DT>3.
<DD>allow users to interact with one another;
<P><DT>4.
<DD>allow users to evaluate simulation and problem scenarios.
<P></DL>
<P>
During early stages it was accepted that fully graphical online
editing of the model was out-with the scope of the prototype.  Earlier
work had shown that such editing could be achieved using tools written
in Java (McNab 1997) and that it could readily be incorporated into a final
product.  Instead, editing in the prototype is restricted to altering
system parameters rather than physical reconfiguration online.  This
was not considered a major setback, as it was still possible for one
of the users to edit the model on their local machine and place the
resulting executable model into the database of models visible to
those participating in using the environment.  This new model could
then be loaded in place of the original, achieving the overall goal of
experimenting with differing variations on the system being analysed.
<P>
The definition of interaction between players and the simulation was
given careful consideration.  The decision was to have a main
controlling player at any one time, acting as a kind of <EM>super user</EM>.
This <EM>super user</EM> would take part in dialogue with other players to
determine which simulation to load and execute (this is on the
assumption that variations of a given model may exist).  For
simplicity and consistency the <EM>super user</EM> would retain control over
the simulation component of the system, although some means of
transferring this control to another designated player was necessary.
In this way the system could ensure all players were seeing a
consistent view of the game state.  The identification of the <EM>super
user</EM> determines the flow of the game control i.e.  identifying whose
turn it is at any given time.
<P><H1><A NAME="SECTION00040000000000000000">
CONSTRUCTION</A>
</H1>
<P>
<BR>
<DIV ALIGN="CENTER"><A NAME="31">&#160;</A>
<TABLE>
<CAPTION><STRONG>Figure 1:</STRONG>
Commands passing between Client and Server</CAPTION>
<TR><TD><IMG WIDTH="361" HEIGHT="362"
 SRC="img1.gif"
 ALT="\begin{figure*}
\begin{center}

\includegraphics [width=8cm, height=8cm]{command.eps}

 \end{center}\end{figure*}"></TD></TR>
</TABLE>
</DIV>
<BR>
<P>
After investigating alternatives the Java Remote Method
Invocation (RMI) approach was chosen as the basis for the required
client-server architecture. The basis of the RMI framework was taken
from code already developed by Fred Howell in the Department of 
Computer Science at Edinburgh University. This was adapted by adding
the player dialogue and simulation interaction, and modifying the applet
layout accordingly.
<P>
The objects defined <B>remote</B> are those that need to communicate with
one another. There are two remote objects in the prototype architecture:
<P><DL>
<DT><STRONG>Server Side :</STRONG>
<DD><EM>SimChatServer</EM> is an instance of the <EM>
SimChatImpl</EM> class. Since the server is at the core of all communication it
is defined remote.
<DT><STRONG>Client Side :</STRONG>
<DD><EM>SimChatApplet</EM> has a remote object in the form of
a component panel, <EM>SimChatPanel</EM>, which passes commands to the server.
<P></DL>
<P><EM>SimChatPanel</EM> and <EM>SimChatServer</EM> are therefore responsible for
carrying out the necessary communication between client and server.
<P>
The unit of communication is the <EM>command</EM>. A command is triggered via
the applet and communicated by <EM>SimChatPanel</EM> to <EM>SimChatServer</EM>.
The server maintains a list of connected players, and each player applet
has an associated list of commands in the form of a <EM>CommandQueue</EM>
object. Commands received by the server are placed in the <EM>
CommandQueue</EM> object for each of the clients.
<P>
This stream-lines the client-server interface; reducing the server
responsibility to passing on commands.  It is the responsibility of
the client applet to pick up and process the command from the <EM>
CommandQueue</EM> object.
<P>
The full range of commands supported are as follows:
<DL>
<DT><STRONG>Add :</STRONG>
<DD>the client connecting to the server; carried out automatically
when the applet is started.
<DT><STRONG>Load : </STRONG>
<DD>the command to load a simulation; this loads the chosen
simulation into its own browser window.
<DT><STRONG>Xfer :</STRONG>
<DD>the command to transfer <EM>super user</EM> status between players.
<DT><STRONG>Chat :</STRONG>
<DD>the submission of a chat command to the server
<DT><STRONG>Quit :</STRONG>
<DD>a player leaving the game and terminating the server connection.
</DL>
<P>
The basis for the simulation was chosen to be the <EM>simanim</EM>
package, the applet extension to <EM>simjava</EM>.
<P>
<BR>
<DIV ALIGN="CENTER"><A NAME="57">&#160;</A>
<TABLE>
<CAPTION><STRONG>Figure 2:</STRONG>
The user dialogue window</CAPTION>
<TR><TD><IMG WIDTH="362" HEIGHT="389"
 SRC="img2.gif"
 ALT="\begin{figure*}
\begin{center}

\includegraphics [width=8cm]{screen6.eps}

 \end{center}\end{figure*}"></TD></TR>
</TABLE>
</DIV>
<BR>
<P>
<BR>
<DIV ALIGN="CENTER"><A NAME="63">&#160;</A>
<TABLE>
<CAPTION><STRONG>Figure 3:</STRONG>
The simulation run window</CAPTION>
<TR><TD><IMG WIDTH="362" HEIGHT="391"
 SRC="img3.gif"
 ALT="\begin{figure*}
\begin{center}

\includegraphics [width=8cm]{screen5.eps}

 \end{center}\end{figure*}"></TD></TR>
</TABLE>
</DIV>
<BR><H1><A NAME="SECTION00050000000000000000">
CASE STUDY</A>
</H1>
<P>
To validate both the working of the system and its usefulness in
solving realistic problems, a case study was carried out. This took a
published example of a real life problem which had been solved by
Kiteck (Kiteck 1992) using conventional simulation and animation techniques.
A trace through a dialogue, using the distributed simulation system to
solve the same problem as in Kiteck's report, was carried out,
assuming that the simulation consultant is physically located at a
different site from the production engineers.
<P>
The initial phase of exchanging information and ideas via the chat
system worked well for a small number of participants. It showed that
an effective way of structuring the problem solving was to agree a
decision tree, where nodes defined alternative approaches to solving
the problem. This allowed the participants to share a mental map of
the task.
<P>
This example showed that future enhancements could focus on allowing
more efficient exchange of ideas, reducing typing.  The building of
some sort of decision making map, displayed graphically, seems an
attractive enhancement to the system. The problem of keeping track of
contributions in a multi-way discussion is well known and also needs
further support.
<P>
Thus, the case study reveals the potential for rapid decision making,
but also opens up the need for a means of structuring and supporting
the dialogue to reach sensible strategies quickly.  In particular it
rapidly became clear that the co-operative approach, where all
participants remain fully involved in the discussion throughout, is
potentially very wasteful of time and resources.  Two simple
improvements are proposed and will be validated in further work.
<P><DL COMPACT>
<DT>1.
<DD>In many cases it would be possible to divide up alternatives to
be explored, either arbitrarily or on the basis of the skills of the
participants, so that experiments could be conducted concurrently on
different sites. This is easiest where only numerical parameters are
being varied.
<DT>2.
<DD>It is extremely wasteful for everyone to wait while models are
created or edited by the modelling experts. Some way of allowing users
to drop out of the dialogue or to continue with other useful work,
during such interludes, is needed.
</DL>
<P>
Further ideas worthy of consideration include the development of
competitive, rather than co-operative paradigms of interaction. These
might allow something like competitive tendering, with different
consultants trying to produce the most effective solution to a problem.
<P><H1><A NAME="SECTION00060000000000000000">
PROJECT ACHIEVEMENTS</A>
</H1>
<P>
At the conclusion of the project a number of achievements can be identified.
<P>
Firstly, a <EM>working prototype</EM> of the required system has been
realized. Although, as expected, the prototype has limited
functionality, sufficient of the core is present to demonstrate
achievement of the initial aims.
<P><DL>
<DT><STRONG>The system supports multiple players</STRONG>
<DD>connecting to a central server.
<DT><STRONG>Players can join a session</STRONG>
<DD>with any other players to resolve a
problem they share.
<P><DT><STRONG>Players can
communicate</STRONG>
<DD>with one another via a chat dialogue, and view the list of
available simulations.
<P><DT><STRONG>A simulation can be chosen and loaded</STRONG>
<DD>on behalf
of all players by the initial <EM>super user</EM> (the initial super user is
determined to be the first player  to connect to the server). Once
loaded, each player has a local copy of the simulation which is under
their own local  control. The facilities already present in simanim
allow a degree of interaction, for example starting, stopping and
pausing the simulation as well as modification of certain operating
parameters.
<P><DT><STRONG>A mechanism to transfer <EM>super user</EM> control</STRONG>
<DD>to other
players is also incorporated.
<P></DL>
<P>
Through a case study, both the potential for the use of such a system
was proved and a number of ideas for improving its effectiveness were
identified.
<P>
Overall the project succeeded in the production of a
prototype  multi-user Internet based simulation system written in
Java. The
project succeeded in constructing such a system to allow
users and analysts to interact and explore the problem domain avoiding
the need for all the parties involved to be in the same physical
location.
<P>
Although the initial focus of this project was based around a factory
simulation the ideas presented could be argued to apply to any problem
domain where simulation provides a suitable medium for problem
expression.
<P><H1><A NAME="SECTION00070000000000000000">
FUTURE WORK</A>
</H1>
<P>
Future developments of the system will centre on lessons learned from
the prototype and its use. The key questions centre on how best to
make use of the interaction of groups with different skills and
knowledge. This involves both how to co-operate effectively and how to
benefit from concurrent work on different sites.
<P>
Minor questions concern integration of better browsing of model
databases into the chat facility and full integration of online
editing.
<P>
It is imperative that better support be provided for structuring and
recording the dialogue in preliminary stages, in order to generate a
decision map. Ideally this could be updated and amended as the project
proceeded, rather in the manner of a branch and bound optimisation in
traditional operational research.
<P>
A major question concerns the use of the decision map produced during
preliminary discussion to control automatic exploration of
alternatives and parameter spaces. This would seem to be a more
effective means of exploring questions of optimisation of system
throughput, whereas the present graphical orientation is more suited
to understanding how to fix single breakdowns in systems.
<P><H1><A NAME="SECTION00080000000000000000">
WORLD WIDE WEB REFERENCES</A>
</H1>
<P><SMALL>
http://biz.onramp.net/javapages/games.htm, Java Game
Programming
<P>
http://www.dcs.ed.ac.uk/home/hase/simjava/simjava-1.0/, Simulations
in Java
<P>
http://www.javasoft.com/docs/books/tutorial/index.html, The Java
Tutorial
<P>
http://chatsubo.javasoft.com/current/, Distributed
Systems
<P>
http://java.sun.com/docs/index.html, JDK 1.1.3
Documentation
<P>
http://ms.ie.org/websim/survey/survey.html, A Survey of Web Based
Simulations
</SMALL>
<P><H1><A NAME="SECTION00090000000000000000">
REFERENCES</A>
</H1>
 <B>Dunham, N.R.; A.K. Kochhar.
1983.</B> ``Approaches to the Computer Simulation of Production
Systems''. In <EM>Simulation in Inventory and Production Control</EM>, ed Haluk
Bekiroglu
<BR>
<B>Howell, F.W.; and R. McNab.  1996.</B> ``Using Java for
Discrete Event Simulation''. In <EM>Proceedings UK Computer and
Telecommunications Performance Engineering Workshop</EM>, University of
Edinburgh, 219-228
<BR>
<B>Kiteck, P.J. 1992.</B> ``Analysis of Component Interaction in
a Distribution Facility Using Simulation''. In <EM>Proceedings EUROSIM 1992</EM>,
North Holland
<BR>
<B>McNab R. 1997.</B> <EM>A Graphical Construction Tool for Process
Based Simulation Models</EM>, Honours project report, Department of
Computer Science, University of Edinburgh.
<BR>
<B>Page, E.H.; R.L. Moose and S.P. Griffen.</B> ``Web-based Simulation
is SimJava using Remote Method Invocation''. Submitted to 1997 Winter Simulation Conference, Atlanta, GA, 7-10 December

<H1><A NAME="SECTION000100000000000000000">
About this document ... </A>
</H1> 
 <STRONG>FACTORY SIMULATION ACROSS THE WORLD WIDE WEB USING JAVA</STRONG><P>
This document was generated using the
<A HREF="http://www-dsed.llnl.gov/files/programs/unix/latex2html/manual/"><STRONG>LaTeX</STRONG>2<tt>HTML</tt></A> translator Version 97.1 (release) (July 13th, 1997)
<P>
Copyright &#169; 1993, 1994, 1995, 1996, 1997,
<A HREF="http://cbl.leeds.ac.uk/nikos/personal.html">Nikos Drakos</A>, 
Computer Based Learning Unit, University of Leeds.
<P>
The command line arguments were: <BR>
 <STRONG>latex2html</STRONG> <tt>-split 1 wilcoxpa.tex</tt>.
<P>
The translation was initiated by Pauline Wilcox on 9/18/1997<HR>
<!--Navigation Panel-->
<IMG WIDTH="37" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="next" SRC="next_motif_gr.gif"> 
<IMG WIDTH="26" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="up" SRC="up_motif_gr.gif"> 
<IMG WIDTH="63" HEIGHT="24" ALIGN="BOTTOM" BORDER="0" ALT="previous" SRC="previous_motif_gr.gif">   
<BR>
<!--End of Navigation Panel-->
<ADDRESS>
<I>Pauline Wilcox</I>
<BR><I>9/18/1997</I>
</ADDRESS>
</BODY>
</HTML>
