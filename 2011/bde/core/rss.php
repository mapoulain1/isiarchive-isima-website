<?xml version="1.0" encoding="ISO-8859-1" ?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns="http://purl.org/rss/1.0/">

<channel rdf:about="http://www.isima.fr/bde/">
	<title>Le site du BDE de l'ISIMA - </title>
	<link>http://www.isima.fr/bde/</link>
	<description></description>
	<dc:language>fr</dc:language>
	
	<items>
		<rdf:Seq>

<rdf:li rdf:resource="http://www.isima.fr/bde/?102-enfin--vendredi-9-soiree-fin-de-partiels"/><rdf:li rdf:resource="http://www.isima.fr/bde/?101-soiree-clubbin-jeudi-10-novembre"/><rdf:li rdf:resource="http://www.isima.fr/bde/?100-soiree-masquee-jeudi-20-octobre"/><rdf:li rdf:resource="http://www.isima.fr/bde/?99-concert-inter-ecoles-au-bde-de-l-isima"/><rdf:li rdf:resource="http://www.isima.fr/bde/?98-soiree-fin-de-partiels-zz0"/><rdf:li rdf:resource="http://www.isima.fr/bde/?97-isimalt"/><rdf:li rdf:resource="http://www.isima.fr/bde/?96-quizzy"/><rdf:li rdf:resource="http://www.isima.fr/bde/?95-la-pourravetheque"/>
	</rdf:Seq>
	</items>
</channel>


  <item rdf:about="http://www.isima.fr/bde/?102-enfin--vendredi-9-soiree-fin-de-partiels">
	<title>Enfin ! Vendredi 9 soir�e fin de partiels !</title> 
	<link>http://www.isima.fr/bde/?102-enfin--vendredi-9-soiree-fin-de-partiels</link> 
	<dc:date>2011-12-06T19:53:15+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		L'�v�nement que tous les ZZ attendent, la meilleure partie des exams, le point culminant de la semaine : la soir�e fin de partiels !!!
Vendredi ce sera la f�te ! Histoire de d�compresser entre ZZ (ou pas!) sur les playlist de Yal et dans une ambiance de folie venez tous au BDE !
	]]></description> 
  </item>
  <item rdf:about="http://www.isima.fr/bde/?101-soiree-clubbin-jeudi-10-novembre">
	<title>Soir�e clubbin' jeudi 10 novembre</title> 
	<link>http://www.isima.fr/bde/?101-soiree-clubbin-jeudi-10-novembre</link> 
	<dc:date>2011-11-06T17:47:11+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		Le BDE des ZZ se transforme en discoth�que rien que pour eux ! De la musique qui fait bouger ton corps toute la nuit. Une ambiance comme on aime pour f�ter un weekend de trois jours !
	]]></description> 
  </item>
  <item rdf:about="http://www.isima.fr/bde/?100-soiree-masquee-jeudi-20-octobre">
	<title> Soir�e masqu�e - Jeudi 20 octobre</title> 
	<link>http://www.isima.fr/bde/?100-soiree-masquee-jeudi-20-octobre</link> 
	<dc:date>2011-10-16T22:14:19+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		L'ann�e est d�ja bien entam�e et c'est bient�t les vacances !
Pour f�ter cela, nous vous proposons une soir�e masqu�e au BDE le jeudi 20
octobre (soit le jeudi avant les vacances donc). Vous pouvez donc si vous
le souhaitez venir par�(e) de votre plus beau masque afin de faire la f�te
dans un ambiance un peu plus ... myst�rieuse :)
	]]></description> 
  </item>
  <item rdf:about="http://www.isima.fr/bde/?99-concert-inter-ecoles-au-bde-de-l-isima">
	<title>Concert inter �coles au BDE de l'ISIMA</title> 
	<link>http://www.isima.fr/bde/?99-concert-inter-ecoles-au-bde-de-l-isima</link> 
	<dc:date>2011-10-04T12:06:38+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		
	]]></description> 
  </item>
  <item rdf:about="http://www.isima.fr/bde/?98-soiree-fin-de-partiels-zz0">
	<title>Soir�e fin de partiels ZZ0</title> 
	<link>http://www.isima.fr/bde/?98-soiree-fin-de-partiels-zz0</link> 
	<dc:date>2011-09-21T02:20:05+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		Vendredi 23 septembre une soir�e avec Polytech au BDE de l'ISIMA pour f�ter la fin des semaines bloqu�es des ZZ0 et des rattrapages des futurs ZZ3.
Venez tous passer une bonne soir�e cette fin de semaine !
	]]></description> 
  </item>
  <item rdf:about="http://www.isima.fr/bde/?97-isimalt">
	<title>Isimalt</title> 
	<link>http://www.isima.fr/bde/?97-isimalt</link> 
	<dc:date>2011-07-13T18:52:22+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		Oyez Oyez !

Si vous aimez la bi�re - breuvage datant d'au moins du IVe mill�naire av J-C -, alors ISIMALT est fait pour vous.
	]]></description> 
  </item>
  <item rdf:about="http://www.isima.fr/bde/?96-quizzy">
	<title>Quizzy</title> 
	<link>http://www.isima.fr/bde/?96-quizzy</link> 
	<dc:date>2011-07-12T21:42:59+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		Club de l'ISIMA fond� en 2011, compos� de roux, de bretons, de savoyards,
d'�cossais et de gauchers. Mes membres ont soif de connaissance et savent
que J�roboam, Rehoboam, Salmanazar, Balthazar, Melchior, Melchisedech et
Nabuchodonosor sont des souverains.
	]]></description> 
  </item>
  <item rdf:about="http://www.isima.fr/bde/?95-la-pourravetheque">
	<title>La Pourraveth�que</title> 
	<link>http://www.isima.fr/bde/?95-la-pourravetheque</link> 
	<dc:date>2011-07-12T21:38:18+00:00</dc:date>
	<dc:creator>bde</dc:creator>
	<description><![CDATA[
		Cin�philes, sinophiles,

si toi aussi tu penses qu'il n'y a rien de meilleur qu'un bon film, si toi non plus tu n'h�sites pas � aller au cin�ma pour mieux profiter d'un bon film, alors ce club N'EST PAS POUR TOI !!
	]]></description> 
  </item></rdf:RDF>
